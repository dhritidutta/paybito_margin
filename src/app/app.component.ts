import { Component } from '@angular/core';
import { CoreDataService } from './core-data.service';
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { TradesComponent } from './trades/trades.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private modalService: NgbModal, private data:CoreDataService, private http:HttpClient, private route:Router, private trade:TradesComponent){

    this.route.events.subscribe(()=>{
      if(this.route.url!='/dashboard'){
          // console.log('stop');
          clearInterval(this.trade.tradeInterval);
      }
  });

  this.data.idleLogout();

  }

  warn(warn) {
    this.modalService.open(warn);
  }

  /* oauthThroughRefreshToken(){

    var presentRefreshToken=localStorage.getItem('refresh_token');
    var fd = new FormData();
    fd.append('refresh_token',presentRefreshToken);
    fd.append('grant_type','refresh_token');
    this.http.post<any>(this.data.WEBSERVICE+'/oauth/token?grant_type=refresh_token&refresh_token='+presentRefreshToken,'',{ headers:{
      // 'Content-Type': 'application/x-www-form-urlencoded',
      'authorization': 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg=='
    } })
    .subscribe(response=>{
        var result= response;
            localStorage.setItem('access_token',result.access_token);
            localStorage.setItem('refresh_token',result.refresh_token);

            var expiresTime=result.expires_in;
            expiresTime=expiresTime*1000;
            var start_time=$.now();
            var expiresIn=start_time+expiresTime;
            localStorage.setItem('expires_in',expiresIn);

            //get user details
            var userObj={};
            userObj['user_id']=localStorage.getItem('user_id');
            var userJsonString=JSON.stringify(userObj);
            // wip(1);
            var accessToken=localStorage.getItem('access_token');
            this.http.post<any>(this.data.WEBSERVICE+'/user/GetUserDetails',userJsonString,{headers: {
                'Content-Type': 'application/json',
                'authorization': 'BEARER '+accessToken,
            }})
            .subscribe(response=>{
            // wip(0);
            var result=response;
            if(result.error.error_data!='0'){
                this.data.alert(result.error.error_msg,'danger');
                }else{
                localStorage.setItem('user_name',result.userResult.full_name);
                localStorage.setItem('user_id',result.userResult.user_id);
                localStorage.setItem('phone',result.userResult.phone);
                localStorage.setItem('email',result.userResult.email);
                localStorage.setItem('address',result.userResult.address);
                localStorage.setItem('profile_pic',result.userResult.profile_pic);

                clearInterval(this.data.interval);
                this.data.time = 10;
                this.data.idleLogout();
                // this.data.idleLogout();
                // location.reload();
               // window.location='main.html';
            //    $('#sessionExpireModal').modal('hide');
            //    clearInterval($scope.expireTimerInterval);
            }
            });

    },reason=>{
        //   wip(0);
      this.data.alert(reason.error.message,'danger');
      this.data.logout();
    });
} */

}
