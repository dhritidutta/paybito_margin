import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { AngularDraggableModule } from 'angular2-draggable';
import { UserIdleModule } from 'angular-user-idle';
import { QRCodeModule } from 'angularx-qrcode';
import { RecaptchaModule } from 'ng-recaptcha';
import { Ng2OdometerModule } from 'ng2-odometer';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import {NgxPaginationModule} from 'ngx-pagination';
import { AppComponent } from './app.component';
import { OrderBookComponent } from './order-book/order-book.component';
import { ChartComponent } from './chart/chart.component';
import { TradesComponent } from './trades/trades.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { StopLossComponent } from './stop-loss/stop-loss.component';
import { SafePipe } from './chart/safe.pipe';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CoreDataService } from "./core-data.service";
import { BodyService } from "./body.service";
import { LoginComponent } from './login/login.component';
import { CookieService } from "ngx-cookie-service";
import { MyWalletComponent } from './my-wallet/my-wallet.component';
import { DepositFundsComponent } from './deposit-funds/deposit-funds.component';

import { WithdrawFundsComponent } from './withdraw-funds/withdraw-funds.component';
import { HistoryComponent } from './history/history.component';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';
import { IdentityVerificationComponent } from './identity-verification/identity-verification.component';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { SettingsComponent } from './settings/settings.component';
import { SupportComponent } from './support/support.component';
import { SignupComponent } from './signup/signup.component';
import { SecureTokenComponent } from './secure-token/secure-token.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { OtpComponent } from './otp/otp.component';
import { ModalsComponent } from './modals/modals.component';
import { LoaderComponent } from './loader/loader.component';
import { ReportComponent } from './report/report.component';
import{NumericDirective} from '../app/core-data.directive';
import { NgbdModalContent, } from './deposit-funds/deposit-funds.component';
import { PortfolioComponent } from './portfolio/portfolio.component';

export * from './chart/chart.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'signup', component: SignupComponent},
  { path: 'otp', component: OtpComponent},
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'my-wallet', component: MyWalletComponent},
  { path: 'deposit-funds', component: DepositFundsComponent},
  { path: 'deposit-funds/:transaction_id/:msid', component: DepositFundsComponent},
  { path: 'withdraw-funds', component: WithdrawFundsComponent},
  { path: 'history', component: HistoryComponent},
  { path: 'profile-details',  component: ProfileDetailsComponent},
  { path: 'identity-verification', component: IdentityVerificationComponent},
  { path: 'bank-details', component: BankDetailsComponent},
  { path: 'promotion', component:PromotionsComponent},
  { path: 'settings', component: SettingsComponent},
  { path: 'support', component: SupportComponent},
  { path: 'secure-token', component: SecureTokenComponent},
  { path: 'forget-password', component: ForgetPasswordComponent},
  { path: 'report', component: ReportComponent},
  { path: 'modaltest', component:ModalsComponent},
  { path: 'Portfolio-details', component: PortfolioComponent}
  
];

@NgModule({
  declarations: [
    AppComponent,
    OrderBookComponent,
    ChartComponent,
    TradesComponent,
    StopLossComponent,
    SafePipe,
    NavbarComponent,
    DashboardComponent,
    LoginComponent,
    MyWalletComponent,
    DepositFundsComponent,
     NgbdModalContent,
    WithdrawFundsComponent,
    HistoryComponent,
    ProfileDetailsComponent,
    IdentityVerificationComponent,
    BankDetailsComponent,
    PromotionsComponent,
    SettingsComponent,
    SupportComponent,
    SignupComponent,
    SecureTokenComponent,
    ForgetPasswordComponent,
    OtpComponent,
    ModalsComponent,
    LoaderComponent,
    ReportComponent,
    NumericDirective,
    PortfolioComponent
  ],
  imports: [
    FormsModule,
    QRCodeModule,
    RecaptchaModule,
    AngularDraggableModule,
    LazyLoadImageModule,
    NgbModule.forRoot(),
    BrowserModule,
    Ng2OdometerModule.forRoot(),
    HttpClientModule,
    NgxPaginationModule,
    UserIdleModule.forRoot({idle: 600, timeout: 10, ping: 15}), //control idle timeout
    RouterModule.forRoot(
      appRoutes
      // { enableTracing: true } // <-- debugging purposes only
    )
  ],

  providers: [
    ChartComponent,
    OrderBookComponent,
    StopLossComponent,
    CoreDataService,
    CookieService,
    TradesComponent,
    NavbarComponent,
    BodyService,
    DashboardComponent,
    MyWalletComponent,
    NgbdModalContent,
    SignupComponent,
  {provide: LocationStrategy, useClass: HashLocationStrategy}
],
entryComponents: [NgbdModalContent],
  bootstrap: [AppComponent]
})
export class AppModule { }
