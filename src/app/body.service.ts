import {
  Injectable
} from '@angular/core';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  CoreDataService
} from './core-data.service';
import {
  TradesComponent
} from "./trades/trades.component";
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
import { OrderBookComponent } from './order-book/order-book.component';

@Injectable({
  providedIn: 'root'
})
export class BodyService {
  cryptoCurrency: any;
  lockOutgoingTransactionStatus: any;
  paybito_phone: string;
  paybito_amount: string;
  paybito_otp: string;
  other_address: string;
  other_amount: string;
  other_otp: string;
  recievingAddress: any = 0;

  selelectedBuyingAssetBalance: any;
  selelectedSellingAssetBalance: any;
  rcvCode: string;
  balance_list: {
    currency: string;
    balance: number;
    send: number;
    receive: number;
  }[];
  loader: boolean;
  pgn: any = [];
  trigxBalance: number;

  constructor(private http: HttpClient, private data: CoreDataService, private route: Router, private orderbk: OrderBookComponent) {
    this.orderbk.tradePageSetup();
  }
  //get user transaction
  totalFiatBalance;
  triggerslink: any = 'https://xchain.io/tx/';
  fiatBalanceLabel;
  public btcBalance;
  public bchBalance;
  public hcxBalance;
  public iecBalance;
  public ethBalance;
  public ltcBalance;
  public btccurrency;
  buyPrice;
  btcBalanceInUsd;
  usdBalance;
  sellPrice;
  buyPriceText;
  sellPriceText;
  fiatBalance;
  fiatBalanceText: string;
  selectedCryptoCurrency;
  selectedCryptoCurrencyBalance;
  triggersBalance;
  bchBalanceInUsd;
  hcxBalanceInUsd;
  iecBalanceInUsd;
  ethBalanceInUsd;
  btcBought;
  btcSold;
  bchBought;
  bchSold;
  hcxBought;
  hcxSold;
  iecBought;
  iecSold;
  ethBought;
  //new etc
  etcBought;
  etcSold;
  public etcBalance;
  etcBalanceInUsd;
  //diam
  diamBought;
  diamSold;
  public diamBalance;
  diamBalanceInUsd;
  //new bsv balance
  bsvBought;
  bsvSold;
  public bsvBalance;
  bsvBalanceInUsd;
  //new currency

  ethSold;
  ltcBought;
  ltcSold;
  noOfItemPerPage = '20';
  timeSpan = 'all';
  public filterCurrency: any;
  public currencylist: any;
  public mainArr = [];
  public bchcurrency;
  public ethcurrency;
  public bcccurrency;
  public diamcurrency;
  public ltccurrency;
  public hcxcurrency;
  public etccurrency;
  public bsvcurrency;
  ngOnInit() {
    this.GetcurrencyList();
  }
  getUserTransaction() {
    debugger;
    var userTransObj = {};
    userTransObj['user_id'] = localStorage.getItem('user_id');
    // userTransObj['crypto_currency']=localStorage.getItem('selected_currency');
    userTransObj['crypto_currency'] = localStorage.getItem('buying_crypto_asset');;
    var jsonString = JSON.stringify(userTransObj);
    this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/GetUserBalance', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token')
      }
    })
      .subscribe(response => {
        var result = response;

        if (result.error.error_data != '0') {
          this.data.alert('Cannot fetch user balance', 'danger');
        } else {
          this.totalFiatBalance = this.data.CURRENCYICON + (result.userBalanceResult.fiat_balance).toFixed(2);
          this.fiatBalanceLabel = 'Total ' + this.data.CURRENCYNAME + ' Balance';
          if (result.userBalanceResult.btc_balance == 'null' || result.userBalanceResult.btc_balance == null) {
            this.btcBalance = '0'
          } else {
            this.btcBalance = parseFloat(result.userBalanceResult.btc_balance);
          }
          if (result.userBalanceResult.bch_balance == 'null' || result.userBalanceResult.bch_balance == null) {
            this.bchBalance = '0'
          } else {
            this.bchBalance = parseFloat(result.userBalanceResult.bch_balance);
          }
          if (result.userBalanceResult.hcx_balance == 'null' || result.userBalanceResult.hcx_balance == null) {
            this.hcxBalance = '0'
          } else {

            this.hcxBalance = parseFloat(result.userBalanceResult.hcx_balance);
          }
          if (result.userBalanceResult.iec_balance == 'null' || result.userBalanceResult.iec_balance == null) {
            this.iecBalance = '0'
          } else {
            this.iecBalance = parseFloat(result.userBalanceResult.iec_balance);
          }
          if (result.userBalanceResult.eth_balance == 'null' || result.userBalanceResult.eth_balance == null) {
            this.ethBalance = '0'
          } else {
            this.ethBalance = parseFloat(result.userBalanceResult.eth_balance);
          }
          if (result.userBalanceResult.ltc_balance == 'null' || result.userBalanceResult.ltc_balance == null) {
            this.ltcBalance = '0'
          } else {
            this.ltcBalance = parseFloat(result.userBalanceResult.ltc_balance);
          }

          if (result.userBalanceResult.triggers_balance == 'null' || result.userBalanceResult.triggers_balance == null) {
            this.triggersBalance = '0'
          } else {
            this.triggersBalance = parseFloat(result.userBalanceResult.triggers_balance);
          }
          if (result.userBalanceResult.trigx_balance == 'null' || result.userBalanceResult.trigx_balance == null) {
            this.trigxBalance = 0;
          } else {
            this.trigxBalance = parseFloat(result.userBalanceResult.trigx_balance);
          }
          //etc
          if (result.userBalanceResult.etc_balance == 'null' || result.userBalanceResult.etc_balance == null) {
            this.etcBalance = 0;
          } else {
            this.etcBalance = parseFloat(result.userBalanceResult.etc_balance);
          }
          //bsv
          if (result.userBalanceResult.bsv_balance == 'null' || result.userBalanceResult.bsv_balance == null) {
            this.bsvBalance = 0;
          } else {
            this.bsvBalance = parseFloat(result.userBalanceResult.bsv_balance);
          }
          //diam
          if (result.userBalanceResult.diam_balance == 'null' || result.userBalanceResult.diam_balance == null) {
            this.diamBalance = '0'
          } else {
            this.diamBalance = parseFloat(result.userBalanceResult.diam_balance);
          }
          this.buyPrice = result.userBalanceResult.crypto_buy_price;
          this.btcBalanceInUsd = (parseFloat(this.btcBalance) * parseFloat(this.buyPrice)).toFixed(4);
          this.bchBalanceInUsd = (parseFloat(this.bchBalance) * parseFloat(this.buyPrice)).toFixed(4);
          this.hcxBalanceInUsd = (parseFloat(this.hcxBalance) * parseFloat(this.buyPrice)).toFixed(4);
          this.iecBalanceInUsd = (parseFloat(this.iecBalance) * parseFloat(this.buyPrice)).toFixed(4);
          this.ethBalanceInUsd = (parseFloat(this.ethBalance) * parseFloat(this.buyPrice)).toFixed(4);
          //diam
          this.diamBalanceInUsd = (parseFloat(this.diamBalance) * parseFloat(this.buyPrice)).toFixed(4);
          //new etc
          this.etcBalanceInUsd = (parseFloat(this.etcBalance) * parseFloat(this.buyPrice)).toFixed(4);
          //bsv
          this.bsvBalanceInUsd = (parseFloat(this.bsvBalance) * parseFloat(this.buyPrice)).toFixed(4);

          // this.buyPrice=(this.buyPrice).toFixed(2);
          this.sellPrice = result.userBalanceResult.crypto_sell_price;
          //this.sellPrice=(this.sellPrice).toFixed(2);
          this.buyPriceText = this.data.CURRENCYICON + this.buyPrice;
          this.sellPriceText = this.data.CURRENCYICON + this.sellPrice;
          this.fiatBalance = result.userBalanceResult.fiat_balance;
          this.fiatBalanceText = this.data.CURRENCYICON + result.userBalanceResult.fiat_balance;

          this.selectedCryptoCurrency = localStorage.getItem('selected_currency');
          if (result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'] == null || result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'] == 'null') {
            this.selectedCryptoCurrencyBalance = '0';
          } else {
            this.selectedCryptoCurrencyBalance = result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'];
          }
          this.btcBought = result.userBalanceResult.bitcoins_bought;
          this.btcSold = result.userBalanceResult.bitcoins_sold;
          this.bchBought = result.userBalanceResult.bitcoinCash_bought;
          this.bchSold = result.userBalanceResult.bitcoinCash_sold;
          this.hcxBought = result.userBalanceResult.hcx_bought;
          this.hcxSold = result.userBalanceResult.hcx_sold;
          this.iecBought = result.userBalanceResult.iec_bought;
          this.iecSold = result.userBalanceResult.iec_sold;
          this.ethBought = result.userBalanceResult.ethereum_bought;
          this.ethSold = result.userBalanceResult.ethereum_sold;
          this.ltcBought = result.userBalanceResult.ltc_bought;
          this.ltcSold = result.userBalanceResult.ltc_sold;
          //new etc
          this.etcBought = result.userBalanceResult.etc_bought;
          this.etcSold = result.userBalanceResult.etc_sold;
          //bsv
          this.bsvBought = result.userBalanceResult.bsv_bought;
          this.bsvSold = result.userBalanceResult.bsv_sold;
          //diam
          this.diamBought = result.userBalanceResult.diam_bought;
          this.diamSold = result.userBalanceResult.diam_sold;

          if (localStorage.getItem('buying_crypto_asset') != 'usd') {
            this.selelectedBuyingAssetBalance = parseFloat(result.userBalanceResult[localStorage.getItem('buying_crypto_asset') + '_balance']).toFixed(4);
          } else if (
            ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'btc')) ||
            ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'eth'))
          ) {
            this.selelectedBuyingAssetBalance = parseFloat(result.userBalanceResult[localStorage.getItem('buying_crypto_asset') + '_balance']).toFixed(8);
          } else {
            this.selelectedBuyingAssetBalance = (result.userBalanceResult.fiat_balance).toFixed(2);
          }
          if (localStorage.getItem('selling_crypto_asset') != 'usd') {
            this.selelectedSellingAssetBalance = parseFloat(result.userBalanceResult[localStorage.getItem('selling_crypto_asset') + '_balance']).toFixed(4);
          } else if (
            ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'btc')) ||
            ((localStorage.getItem('buying_crypto_asset') == 'hcx') && (localStorage.getItem('selling_crypto_asset') == 'eth'))
          ) {
            this.selelectedSellingAssetBalance = (result.userBalanceResult.fiat_balance).toFixed(8);
          } else {
            this.selelectedSellingAssetBalance = (result.userBalanceResult.fiat_balance).toFixed(2);
          }

        }

        // console.log(this.btcBalance);
        //this.main.
        //this.GetcurrencyList(); ethcurrencybtc btc eth bcc daim ltc hcx etc bsv 

        var arraylenth = this.mainArr.length;
        /// console.log('this.mainArr++++++++++',this.mainArr);

        for (var i = 0; i < arraylenth; i++) {
          ///  console.log('9999999',this.mainArr[i]['currencyname']);

          if (this.mainArr[i]['currencyname'] == 'BTC') {
            this.btccurrency = 'BTC';
          }
          else if (this.mainArr[i]['currencyname'] == 'ETH') {
            this.ethcurrency = 'ETH';
          }
          else if (this.mainArr[i]['currencyname'] == 'BCC') {
            this.bcccurrency = 'BCH';
          }
          else if (this.mainArr[i]['currencyname'] == 'DIAM') {
            this.diamcurrency = 'DIAM';
          }
          else if (this.mainArr[i]['currencyname'] == 'LTC') {
            this.ltccurrency = 'LTC';
          }
          else if (this.mainArr[i]['currencyname'] == 'HCX') {
            this.hcxcurrency = 'HCX';
          }
          else if (this.mainArr[i]['currencyname'] == 'ETC') {
            this.etccurrency = 'ETC';
          }
          else if (this.mainArr[i]['currencyname'] == 'BSV') {
            this.bsvcurrency = 'BSV';
          }
          else if (this.mainArr[i]['currencyname'] == 'BCH') {
            this.bsvcurrency = 'BCH';
          }
        }
        // console.log('000000', this.bchcurrency, this.hcxcurrency, this.ethcurrency, this.ltccurrency, this.diamcurrency, this.btccurrency, this.bsvcurrency);

        this.balance_list = [
          {
            currency: this.bcccurrency,
            balance: this.bchBalance,
            send: 1,
            receive: 1
          },
          {
            currency: this.hcxcurrency,
            balance: this.hcxBalance,
            send: 1,
            receive: 1
          },
          {
            currency: this.ethcurrency,
            balance: this.ethBalance,
            send: 1,
            receive: 1
          },
          {
            currency: this.ltccurrency,
            balance: this.ltcBalance,
            send: 1,
            receive: 1
          },
          {
            currency: this.diamcurrency,
            balance: this.diamBalance,
            send: 1,
            receive: 1
          },
          {
            currency: this.btccurrency,
            balance: this.btcBalance,
            send: 1,
            receive: 1
          },
          {
            currency: this.etccurrency,
            balance: this.etcBalance,
            send: 1,
            receive: 1
          },

          {
            currency: this.bsvcurrency,
            balance: this.bsvBalance,
            send: 1,
            receive: 1
          }


        ];
      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {

          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
    this.GetcurrencyList();
    //   // var test = this.currencylist;
    // // console.log('testDDDDDDD',this.bchBalance,this.etcBalance);

    // this.mainArr.push({'btcbalance':this.btcBalance},{'bchbalance':this.bchBalance},{'hcxbalance':this.hcxBalance},{'ethbalance':this.ethBalance},{'ltcbalance':this.ltcBalance},{'diambalance':this.diamBalance},{'trigarbalance':this.triggersBalance},{'etcbalance':this.etcBalance},{'bsvbalance':this.bsvBalance});
    // console.log('testDDDDDDD', this.mainArr);
    // var x = this.currencylist.length;
    // for (var i = 0; i <= x; i++) {


    // }


  }

  environmentSettingListObj;
  selectedCurrency;
  buyDisclaimer;
  buyTxnDisclaimer;
  sellDisclaimer;
  sellTxnDisclaimer;
  sendDisclaimer;
  sendMiningDisclaimer;
  indentificationStatus;
  bankDetailStatus;

  getDashBoardInfo() {
    var infoObj = {};
    infoObj['user_id'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(infoObj);
    this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        var result = response;
        if (result.error.error_data != '0') {
          //    alert(result.error.error_msg);
        } else {
          var storeDashboardInfo = JSON.stringify(result);
          var environmentSettingsListObj: any = {};
          localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
          for (var i = 0; i < result.settingsList.length; i++) {
            environmentSettingsListObj['' + result.settingsList[i].name + ''] = result.settingsList[i];
          }
          environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
          localStorage.setItem('environment_settings_list', environmentSettingsListObj);
          //showing disclaimer for sell
          this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
          this.buyDisclaimer = this.environmentSettingListObj['buy_max_value'][this.selectedCurrency + '_description'];
          this.buyTxnDisclaimer = this.environmentSettingListObj['buy_txn_charges'][this.selectedCurrency + '_description'];
          this.sellDisclaimer = this.environmentSettingListObj['sell_max_value'][this.selectedCurrency + '_description'];
          this.sellTxnDisclaimer = this.environmentSettingListObj['sell_txn_charges'][this.selectedCurrency + '_description'];
          this.sendDisclaimer = this.environmentSettingListObj['send_other_max_value'][this.selectedCurrency + '_description'];
          this.sendMiningDisclaimer = this.environmentSettingListObj['send_other_m_charges'][this.selectedCurrency + '_description'];

          if (result.userAppSettingsResult.user_docs_status == '') {
            this.indentificationStatus = 'Identity verification documents not submitted';
            this.data.alert('PLEASE ENSURE THAT ALL DOCUMENTS ARE IN JPG OR JPEG FORMAT. PNG, GIF, and other formats are not permitted.', 'info');
          }
          if (result.userAppSettingsResult.user_docs_status == '1') {
            this.indentificationStatus = 'Identity verification documents verified';
          }
          if (result.userAppSettingsResult.user_docs_status == '0') {
            this.indentificationStatus = ' Identity verification documents submitted for Verification';
          }
          if (result.userAppSettingsResult.user_docs_status == '2') {
            this.indentificationStatus = ' Identity verification documents declined, please submit again';
          }
          if (result.userAppSettingsResult.bank_details_status == '') {
            this.bankDetailStatus = 'Bank details not submitted';
          }
          if (result.userAppSettingsResult.bank_details_status == '0') {
            this.bankDetailStatus = 'Bank details  submitted for Verification';
          }
          if (result.userAppSettingsResult.bank_details_status == '2') {
            this.bankDetailStatus = ' Bank details verified';
          }
          if (result.userAppSettingsResult.bank_details_status == '3') {
            this.bankDetailStatus = ' Bank documents declined, please submit again';
          }
          if (
            localStorage.getItem('check_id_verification_status') &&
            result.userAppSettingsResult.user_docs_status == ''
          ) {
            // this.nav.alert('Please submit Identity verification documents to access all Paybito features','danger');
          }
          localStorage.setItem('check_id_verification_status', 'false');
        }
      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {

          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }
  verificationTitle;
  verificationText

  userDocVerificationStatus() {
    var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
    var userDocStatus = userAppSettingsObj.user_docs_status;
    if (userDocStatus == '') {
      this.verificationTitle = 'Submit ID Verification';
      this.verificationText = 'Please submit Identity verification documents to access all Paybito features';
      this.data.alert(this.verificationText, 'danger');
      this.route.navigateByUrl('/identity-verification');
      return false;
    } else if (userDocStatus == '2') {
      //todo: show Modal;
      this.verificationTitle = 'Submit ID Verification';
      this.verificationText = 'Your Identity verification documents has been declined in the verification process. Please submit again.';
      this.data.alert(this.verificationText, 'warning');
      this.route.navigateByUrl('/identity-verification');
      return false;
    } else if (userDocStatus == '0') {
      //todo: show Modal;
      this.verificationTitle = 'Document Verification Pending';
      this.verificationText = 'Your Id proof documents have not yet been verified by us. You will have restricted access to the features of the app until they are approved.';
      this.data.alert(this.verificationText, 'info');
      return false;
    } else {
      return true;
    }
  }

  userBankVerificationStatus() {
    var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
    var userBankStatus = userAppSettingsObj.bank_details_status;
    if (userBankStatus == '') {
      this.verificationTitle = 'Submit Payment Method';
      this.verificationText = 'Your Payment details are not yet submitted. Please submit your Payment Details to proceed';

      return false;
    } else if (userBankStatus == '0') {
      //todo: show Modal;
      this.verificationTitle = 'Payment Being Verified';
      this.verificationText = 'Your Payment details are being verified. This step will be available after verification.';
      return false;
    } else if (userBankStatus == '3') {
      this.verificationTitle = 'Submit Payment Method';
      this.verificationText = 'Your Payment details have been disapproved. Kindly submit the details again to proceed.';
      return false;
    } else {
      return true;
    }
  }
  paginationBtn;
  pagination(totalCount, noOfItemPerPage, functionName) {
    var paginationButtonCount = parseInt(totalCount) / parseInt(noOfItemPerPage);
    this.paginationBtn = '';
    // $('.pagination_div').html('');
    if (Math.ceil(paginationButtonCount) >= 1) {
      for (var i = 1; i <= paginationButtonCount + 1; i++) {
        this.paginationBtn += '<button type="button" class="btn btn-dark font-xs filter-button" onclick="angular.element(this).scope().' + functionName + '(' + i + ')" >' + i + '</button>';
      }
    } else {
      this.paginationBtn += '';
    }

    // $('.pagination_div').html(this.paginationBtn);
  }

  totalCount;
  historyDetails;
  historyTableTr;
  selectedCurrencyText;

  transactionHistory(pageNo) {
    this.historyTableTr = `<tr>
    <td colspan="5" class="text-center py-3">
    <img src="./assets/svg-loaders/puff.svg" width="50" alt="">
    </td>
  </tr>`;
    $('.historyTableBody').html(this.historyTableTr);

    var historyObj = {};
    historyObj['customerID'] = localStorage.getItem('user_id');
    historyObj['page_no'] = pageNo;
    historyObj['no_of_items_per_page'] = this.noOfItemPerPage;
    // historyObj['no_of_items_per_page']=20;
    historyObj['time_span'] = this.timeSpan;

    var jsonString = JSON.stringify(historyObj);
    //   wip(1);
    this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/GetUserAllTransaction', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        //   wip(0);
        this.historyTableTr = '';
        var result = response;

        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.historyDetails = result.userTransactionsResult;
          console.log('+++++++++++++dhriti',this.historyDetails);
          
          this.totalCount = result.totalCount;
          if (this.historyDetails != null) {
            for (var i = 0; i < this.historyDetails.length; i++) {
              var timestamp = this.historyDetails[i].transaction_timestamp;
              var dt = new Date(this.historyDetails[i].transaction_timestamp);
              var timestampArr = timestamp.split('.');
              timestamp = this.data.readable_timestamp(timestampArr[0]);
              var action = this.historyDetails[i].action;
             
              
              this.selectedCurrency = localStorage.getItem('selected_currency');
              this.selectedCurrencyText = this.selectedCurrency.toUpperCase();
              if (this.historyDetails[i].base_currency != 'usd') {
                var baseCurrency = this.historyDetails[i].base_currency;
              } else {
                var baseCurrency: any = 'fiat';
              }
              if (this.historyDetails[i].currency != 'usd') {
                var counterCurrency = this.historyDetails[i].currency;
              } else {
                var counterCurrency: any = 'fiat';
              }
              if (action == 'Margin From Wallet') {
                var counterCurrency=this.historyDetails[i].currency.toUpperCase();
                 var currencyvalue='debit_'+counterCurrency.toLocaleLowerCase()+'_amount';
                 
                   amount =counterCurrency+ this.historyDetails[i][currencyvalue]+'Dr' ;
                  }
                  if (action == 'Funding From Wallet') {
                    var counterCurrency=this.historyDetails[i].currency.toUpperCase();
                  
                       var currencyvalue='debit_'+counterCurrency.toLocaleLowerCase()+'_amount';
                   
                       amount =counterCurrency+ this.historyDetails[i][currencyvalue]+'Dr' ;
                      }
              // if (action == null) {
               
              //  var counterCurrency=this.historyDetails[i].currency;
              //     var currencyvalue='debit_'+counterCurrency.toLocaleLowerCase()+'_amount';
              //     //alert('fdgdfg'+currencyvalue);
              //     amount =counterCurrency+ this.historyDetails[i][currencyvalue]+'Dr' ;
              //    }
              if (action == 'Buy') {
                if (this.historyDetails[i]['debit_' + baseCurrency + '_amount'] != 0
                  && this.historyDetails[i]['credit_' + counterCurrency + '_amount'] != 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' ' + this.historyDetails[i]['debit_' + baseCurrency + '_amount'] + ' Dr. </span> / <span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['credit_' + counterCurrency + '_amount'] + ' Cr.</span>';
                } else {
                  if (this.historyDetails[i]['debit_' + baseCurrency + '_amount'] != 0) {
                    var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' ' + this.historyDetails[i]['debit_' + baseCurrency + '_amount'] + ' Dr. </span> ';
                  }
                  if (this.historyDetails[i]['credit_' + counterCurrency + '_amount'] != 0) {
                    var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['credit_' + counterCurrency + '_amount'] + ' Cr. </span> ';
                  }
                }
              }
              if (action == 'Buyoffer') {

                var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' ' + this.historyDetails[i]['debit_' + baseCurrency + '_amount'] + ' Dr. </span> ';
              }
              if (action == 'Sell') {

                if (this.historyDetails[i]['credit_' + baseCurrency + '_amount'] != 0 && this.historyDetails[i]['debit_' + counterCurrency + '_amount'] != 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' ' + this.historyDetails[i]['credit_' + baseCurrency + '_amount'] + ' Cr. </span> / <span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['debit_' + counterCurrency + '_amount'] + ' Dr.</span>';
                } else {
                  if (this.historyDetails[i]['credit_' + baseCurrency + '_amount'] != 0) {
                    var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' ' + this.historyDetails[i]['credit_' + baseCurrency + '_amount'] + ' Cr. </span> ';
                  }
                  if (this.historyDetails[i]['debit_' + counterCurrency + '_amount'] != 0) {
                    var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['debit_' + counterCurrency + '_amount'] + ' Dr. </span> ';
                  }
                }
              }

              //Buydel
              if (action == 'Buydel') {
                if (this.historyDetails[i].base_currency == 'usd' && this.historyDetails[i]['credit_fiat_amount'] == 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                    + this.historyDetails[i]['debit_fiat_amount'] + ' Dr. </span> ';
                }
                else if (this.historyDetails[i].base_currency == 'usd' && this.historyDetails[i]['debit_fiat_amount'] == 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                    + this.historyDetails[i]['credit_fiat_amount'] + ' Cr. </span> ';
                }
                else if (this.historyDetails[i].base_currency != 'usd' && this.historyDetails[i]['credit_' + this.historyDetails[i].base_currency + '_amount'] == 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                    + this.historyDetails[i]['debit_' + this.historyDetails[i].base_currency + '_amount'] + ' Dr. </span> ';
                }
                else {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                    + this.historyDetails[i]['credit_' + this.historyDetails[i].base_currency + '_amount'] + ' Cr. </span> ';
                }
              }
              if (action == 'Selldel') {
                if (this.historyDetails[i]['credit_' + this.historyDetails[i].currency + '_amount'] == 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                    + this.historyDetails[i]['debit_' + this.historyDetails[i].currency + '_amount'] + ' Dr. </span> ';
                }
                else {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                    + this.historyDetails[i]['credit_' + this.historyDetails[i].currency + '_amount'] + ' Cr. </span> ';
                }
              }
              //margin transaction
             
              //end Buydel
              //RollBack Code
              if (action == 'Rollback' || action == 'rollback' || action == 'RollBack') {
                if (this.historyDetails[i].base_currency != 'usd' && this.historyDetails[i].currency != 'usd') {
                  if ((this.historyDetails[i]['credit_' + this.historyDetails[i].base_currency + '_amount']) != 0) {
                    if (this.historyDetails[i].base_currency == '' || this.historyDetails[i].base_currency == '-') {
                      var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                        + this.historyDetails[i]['credit_' + this.historyDetails[i].currency + '_amount'] + ' Cr. </span> ';
                    }
                    else {
                      var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                        + this.historyDetails[i]['credit_' + this.historyDetails[i].base_currency + '_amount'] + ' Cr. </span> ';
                    }
                  }
                  else {
                    var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                      + this.historyDetails[i]['debit_' + this.historyDetails[i].base_currency + '_amount'] + ' Dr. </span> ';
                  }
                }
                else {
                  if ((this.historyDetails[i]['credit_fiat_amount']) != 0) {
                    var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                      + this.historyDetails[i]['credit_fiat_amount'] + ' Cr. </span> ';
                  }
                  else {
                    var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                      + this.historyDetails[i]['debit_fiat_amount'] + ' Dr. </span> ';
                  }
                }
              }
              //Rollback Code Ends
              //BuyOffer Modify
              if (action == 'Buymodify') {
                if (this.historyDetails[i].base_currency == 'usd' && this.historyDetails[i]['credit_fiat_amount'] == 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                    + this.historyDetails[i]['debit_fiat_amount'] + ' Dr. </span> ';
                }
                else if (this.historyDetails[i].base_currency == 'usd' && this.historyDetails[i]['debit_fiat_amount'] == 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                    + this.historyDetails[i]['credit_fiat_amount'] + ' Cr. </span> ';
                }
                else if (this.historyDetails[i].base_currency != 'usd' && this.historyDetails[i]['credit_' + this.historyDetails[i].base_currency + '_amount'] == 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                    + this.historyDetails[i]['debit_' + this.historyDetails[i].base_currency + '_amount'] + ' Dr. </span> ';
                }
                else {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].base_currency).toUpperCase() + ' '
                    + this.historyDetails[i]['credit_' + this.historyDetails[i].base_currency + '_amount'] + ' Cr. </span> ';
                }
              }

              //BuyOffer Modify End

              //SellOffer Modify
              if (action == 'Sellmodify') {
                if (this.historyDetails[i].currency != 'usd' || this.historyDetails[i]['credit_' + this.historyDetails[i].currency + '_amount'] == 0) {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                    + this.historyDetails[i]['debit_' + this.historyDetails[i].currency + '_amount'] + ' Dr. </span> ';
                }
                else {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                    + this.historyDetails[i]['credit_' + this.historyDetails[i].currency + '_amount'] + ' Cr. </span> ';
                }
                /* if (this.historyDetails[i].currency == 'usd' && this.historyDetails[i]['credit_'+this.historyDetails[i].currency+'_amount'] == 0)
                {
                var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                + this.historyDetails[i]['debit_'+this.historyDetails[i].currency+'_amount'] + ' Dr. </span> ';
                }
                else {
                  var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                + this.historyDetails[i]['credit_'+this.historyDetails[i].currency+'_amount'] + ' Cr. </span> ';
                } */
              }
              //SellOffer Modify End
              if (action == 'Selloffer') {
                var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['debit_' + counterCurrency + '_amount'] + ' Dr. </span> ';
              }
              var hrefForTxn: any;
              if (action == 'Send' || action == 'Sent') {
                if (this.historyDetails[i]['debit_btc_amount'] != 0) {
                  var amount = '<span class="text-white">BTC ' + this.historyDetails[i]['debit_btc_amount'] + ' Dr.';
                  if (this.historyDetails[i].btc_txn_id != null && this.historyDetails[i].btc_txn_id != 'null') {
                    if ((this.historyDetails[i].btc_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://blockexplorer.com/tx/' + this.historyDetails[i].btc_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                if (this.historyDetails[i]['debit_bch_amount'] != 0) {
                  var amount = '<span class="text-white">BCH ' + this.historyDetails[i]['debit_bch_amount'] + ' Dr.';
                  if (this.historyDetails[i].btc_txn_id != null && this.historyDetails[i].btc_txn_id != 'null') {
                    if ((this.historyDetails[i].btc_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://explorer.bitcoin.com/bch/search/' + this.historyDetails[i].btc_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                //new etc
                if (this.historyDetails[i]['debit_etc_amount'] != 0) {
                  var amount = '<span class="text-white">ETC ' + this.historyDetails[i]['debit_etc_amount'] + ' Dr.';
                  if (this.historyDetails[i].etc_txn_id != null && this.historyDetails[i].etc_txn_id != 'null') {
                    if ((this.historyDetails[i].etc_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://explorer.bitcoin.com/etc/search/' + this.historyDetails[i].etc_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                //bsv
                if (this.historyDetails[i]['debit_bsv_amount'] != 0) {
                  var amount = '<span class="text-white">BSV ' + this.historyDetails[i]['debit_bsv_amount'] + ' Dr.';
                  if (this.historyDetails[i].bsv_txn_id != null && this.historyDetails[i].bsv_txn_id != 'null') {
                    if ((this.historyDetails[i].bsv_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://explorer.bitcoin.com/bsv/search/' + this.historyDetails[i].bsv_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                //diam
                if (this.historyDetails[i]['debit_diam_amount'] != 0) {
                  var amount = '<span class="text-white">DIAM ' + this.historyDetails[i]['debit_diam_amount'] + ' Dr.';
                  if (this.historyDetails[i].diam_txn_id != null && this.historyDetails[i].diam_txn_id != 'null') {
                    if ((this.historyDetails[i].diam_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://explorer.bitcoin.com/diam/search/' + this.historyDetails[i].diam_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }

                if (this.historyDetails[i]['debit_hcx_amount'] != 0) {
                  var amount = '<span class="text-white">HCX ' + this.historyDetails[i]['debit_hcx_amount'] + ' Dr.';
                }
                if (this.historyDetails[i]['debit_iec_amount'] != 0) {
                  var amount = '<span class="text-white">IEC ' + this.historyDetails[i]['debit_iec_amount'] + ' Dr.';
                }
                if (this.historyDetails[i]['debit_eth_amount'] != 0) {
                  var amount = '<span class="text-white">ETH ' + this.historyDetails[i]['debit_eth_amount'] + ' Dr.';
                }
                if (this.historyDetails[i]['debit_ltc_amount'] != 0) {
                  var amount = '<span class="text-white">LTC ' + this.historyDetails[i]['debit_ltc_amount'] + ' Dr.';
                }
                if (this.historyDetails[i]['debit_triggers_amount'] != '0') {
                  var amount = '<span class="text-white">TRIGGERS ' + this.historyDetails[i]['debit_triggers_amount'] + ' Dr.';
                  if (this.historyDetails[i].triggers_txn_id != null && this.historyDetails[i].triggers_txn_id != 'null') {
                    if ((this.historyDetails[i].triggers_txn_id).length >= 10) {
                      hrefForTxn = '<br><a target="_blank" href="' + this.triggerslink + this.historyDetails[i].triggers_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
              }
              if (action == 'Received' || action == 'Receive') {
                if (this.historyDetails[i]['credit_btc_amount'] != 0) {
                  var amount = '<span class="text-white">BTC ' + this.historyDetails[i]['credit_btc_amount'] + ' Cr.';
                  if (this.historyDetails[i].btc_txn_id != null && this.historyDetails[i].btc_txn_id != 'null') {
                    if ((this.historyDetails[i].btc_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://blockexplorer.com/tx/' + this.historyDetails[i].btc_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                if (this.historyDetails[i]['credit_bch_amount'] != 0) {
                  var amount = '<span class="text-white">BCH ' + this.historyDetails[i]['credit_bch_amount'] + ' Cr.';
                  if (this.historyDetails[i].bch_txn_id != null && this.historyDetails[i].bch_txn_id != 'null') {
                    if ((this.historyDetails[i].bch_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://explorer.bitcoin.com/bch/search/' + this.historyDetails[i].bch_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                //new etc
                if (this.historyDetails[i]['credit_etc_amount'] != 0) {
                  var amount = '<span class="text-white">ETC ' + this.historyDetails[i]['credit_etc_amount'] + ' Cr.';
                  if (this.historyDetails[i].etc_txn_id != null && this.historyDetails[i].etc_txn_id != 'null') {
                    if ((this.historyDetails[i].etc_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://explorer.bitcoin.com/etc/search/' + this.historyDetails[i].etc_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                //bsv
                if (this.historyDetails[i]['credit_bsv_amount'] != 0) {
                  var amount = '<span class="text-white">BSV ' + this.historyDetails[i]['credit_bsv_amount'] + ' Cr.';
                  if (this.historyDetails[i].bsv_txn_id != null && this.historyDetails[i].bsv_txn_id != 'null') {
                    if ((this.historyDetails[i].bsv_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://explorer.bitcoin.com/bsv/search/' + this.historyDetails[i].bsv_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                //diam
                if (this.historyDetails[i]['credit_diam_amount'] != 0) {
                  var amount = '<span class="text-white">DIAM ' + this.historyDetails[i]['credit_diam_amount'] + ' Cr.';
                  if (this.historyDetails[i].diam_txn_id != null && this.historyDetails[i].diam_txn_id != 'null') {
                    if ((this.historyDetails[i].diam_txn_id).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://explorer.bitcoin.com/diam/search/' + this.historyDetails[i].diam_txn_id + '">(Check Transaction Block)</a>';
                    }
                  }
                }
                if (this.historyDetails[i]['credit_hcx_amount'] != 0) {
                  var amount = '<span class="text-white">HCX ' + this.historyDetails[i]['credit_hcx_amount'] + ' Cr.';
                }
                if (this.historyDetails[i]['credit_iec_amount'] != 0) {
                  var amount = '<span class="text-white">IEC ' + this.historyDetails[i]['credit_iec_amount'] + ' Cr.';
                }
                if (this.historyDetails[i]['credit_eth_amount'] != 0) {
                  var amount = '<span class="text-white">ETH ' + this.historyDetails[i]['credit_eth_amount'] + ' Cr.';
                }
                if (this.historyDetails[i]['credit_ltc_amount'] != 0) {
                  var amount = '<span class="text-white">LTC ' + this.historyDetails[i]['credit_ltc_amount'] + ' Cr.';
                }
                if (this.historyDetails[i]['credit_triggers_amount'] != 0) {
                  var amount = '<span class="text-white">TRIGGERS ' + this.historyDetails[i]['credit_triggers_amount'] + ' Cr.';
                }
                if (this.historyDetails[i]['credit_trigx_amount'] != 0) {
                  var amount = '<span class="text-white">TRIGX ' + this.historyDetails[i]['credit_trigx_amount'] + ' Cr.';
                }

                //diam
                // if (this.historyDetails[i]['credit_diam_amount'] != 0) {
                //   var amount = '<span class="text-white">DIAM ' + this.historyDetails[i]['credit_diam_amount'] + ' Cr.';
                // }
              }
              if (action == 'Load') {

                var amount = ' <span class="text-white">' + this.data.CURRENCYICON + ' ' + this.historyDetails[i].credit_fiat_amount + ' Cr.</span>';
              }
              if (action == 'Withdraw') {

                var amount = ' <span class="text-white">' + this.data.CURRENCYICON + ' ' + this.historyDetails[i].debit_fiat_amount + ' Dr.</span>';
              }
              if (action == 'Fundwithdraw') {

                var amount = ' <span class="text-white">' + this.data.CURRENCYNAME +' ' + this.historyDetails[i].credit_fiat_amount + ' Cr.</span>';
              }
              if (this.historyDetails[i].status == 'confirm') {
                var status = 'Confirmed';
                var statusClass = 'text-green';
              }
              if (this.historyDetails[i].status == 'pending') {
                var status = 'Pending';
                var statusClass = 'text-orange';
              }
              if (this.historyDetails[i].status == 'cancel') {
                var status = 'Cancelled';
                var statusClass = 'text-red';
              }
              if (this.historyDetails[i].orderid != null) {
                var transactionId = this.historyDetails[i].orderid;
              } else {
                var transactionId = this.historyDetails[i].transaction_id;
              }

              if (action == 'Buyoffer') {
                var action: any = 'Buy Offer';
              }
              if (action == 'Selloffer') {
                var action: any = 'Sell Offer';
              }

              this.historyTableTr += '<tr>';
              this.historyTableTr += '<td>' + timestamp + '</td>';
              this.historyTableTr += '<td>' + transactionId + '</td>';
              this.historyTableTr += '<td class="text-white">' + action + '</td>';
              this.historyTableTr += '<td class="text-white">' + amount + '</td>';
              this.historyTableTr += '<td class="' + statusClass + '">' + status + '</td>';
              this.historyTableTr += '</tr>';
            }
            //   this.pagination(this.totalCount,this.noOfItemPerPage,'transactionHistory');
          } else {
            this.historyTableTr += '<tr><td colspan="5" class="text-center">No Data Exist</td></tr>';
          }
          $('.historyTableBody').html(this.historyTableTr);
          //   console.log(this.historyTableTr);

          //all
          this.pgn = [];
          for (i = 1; i <= Math.ceil(this.totalCount / 20); i++) {
            this.pgn.push(i);
          }
          // console.log(this.pgn);

        }
      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {

          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });

  }

  fullName;
  editName;
  userName;
  address;
  country;
  email;
  editEmail;
  phone;
  profilePic;
  joinDate;

  getUserDetails() {
    this.loader = true;
    var userObj = {};
    userObj['user_id'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(userObj);
    // wip(1);
    this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserDetails', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        //   wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          // alert(result.error.error_data);
        } else {
          this.loader = false;
          this.fullName = (result.userResult.full_name).toUpperCase();
          this.editName = result.userResult.full_name;
          this.userName = result.userResult.full_name;
          this.address = result.userResult.address;
          this.country = result.userResult.country;
          this.email = result.userResult.email;
          this.editEmail = result.userResult.email;
          this.phone = result.userResult.phone;
          if (localStorage.getItem('profile_pic') != '') {
            this.profilePic = this.data.WEBSERVICE + '/user/' + localStorage.getItem('user_id') + '/file/' + localStorage.getItem('profile_pic') + '?access_token=' + localStorage.getItem('access_token');
          } else {
            this.profilePic = './assets/img/default.png';
          }
          this.joinDate = this.data.readable_timestamp(result.userResult.created);
          this.btcBought = result.userBalanceResult.bitcoins_bought;
          this.btcSold = result.userBalanceResult.bitcoins_sold;
          this.bchBought = result.userBalanceResult.bitcoinCash_bought;
          this.bchSold = result.userBalanceResult.bitcoinCash_sold;
          this.hcxBought = result.userBalanceResult.hcx_bought;
          this.hcxSold = result.userBalanceResult.hcx_sold;
          this.iecBought = result.userBalanceResult.iec_bought;
          this.iecSold = result.userBalanceResult.iec_sold;
          //new etc
          this.etcBought = result.userBalanceResult.etc_bought;
          this.etcSold = result.userBalanceResult.etc_sold;
          //bsv
          this.bsvBought = result.userBalanceResult.bsv_bought;
          this.bsvSold = result.userBalanceResult.bsv_sold;
          //diam
          this.diamBought = result.userBalanceResult.diam_bought;
          this.diamSold = result.userBalanceResult.diam_sold;
        }

      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {

          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }

  getCurrencyForSend(elem) {
    // console.log('elem:' + elem);
    this.cryptoCurrency = elem;
    this.selectedCurrency = elem;
    if (this.userDocVerificationStatus() == true) {
      var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
      this.lockOutgoingTransactionStatus = userAppSettingsObj.lock_outgoing_transactions;
      if (this.lockOutgoingTransactionStatus == 1) {
        $('.sendOtpSection').show();
        // $('.get_Otp_btn').show();
        $('.send_btn').show();

      } else {
        $('.sendOtpSection').hide();
        // $('.get_Otp_btn').hide();
        $('.send_btn').show();
      }

      this.paybito_phone = '';
      this.paybito_amount = '';
      this.paybito_otp = '';
      this.other_address = '';
      this.other_amount = '';
      this.other_otp = '';

      $('#sendModal').modal('show');
      this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
      this.sendDisclaimer = this.environmentSettingListObj['send_other_min_value'][this.cryptoCurrency + '_description'];
      this.sendMiningDisclaimer = this.environmentSettingListObj['send_other_m_charges'][this.cryptoCurrency + '_description'];
    }

  }
  getCurrencyForRecieve(currency) {
    // this.modalService.open(elem, { centered: true });
    // console.log('elem:' + currency);
    this.cryptoCurrency = currency;
    $('.receive_address_label, .receive_address, .recieve_qr_code').hide();
    $('.generate_address_btn').hide();
    $('#qr_code').html('');
    if (this.userDocVerificationStatus() == true) {
      this.generateAddress();
    }

  }

  generateAddress() {
    var rcvObj = {};
    rcvObj['customerID'] = localStorage.getItem('user_id');
    rcvObj['crypto_currency'] = this.cryptoCurrency;
    var jsonString = JSON.stringify(rcvObj);
    // wip(1);
    if (this.cryptoCurrency != 'trigger') {
      this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/ReceiveBTC', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            this.rcvCode = result.customerkeysResult.fromadd;
          }

        });
    } else {
      this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/GetCounterPartyNewAddress', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            this.rcvCode = result.customerkeysResult.fromadd;
          }

        });

    }

  }

  sessionExpiredLogout() {
    localStorage.clear();
    // window.location="login.html?reason=Session expired, please login again.";.
    this.route.navigateByUrl('/login');
  }

  setInterval() {
    var userTransObj = {};
    userTransObj['user_id'] = localStorage.getItem('user_id');
    userTransObj['crypto_currency'] = localStorage.getItem('selected_currency');
    var jsonString = JSON.stringify(userTransObj);
    // wip(1);
    this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/GetUserBalance', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        // wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          if (result.error.error_msg == '' || result.error.error_msg == null) {

          } else {
            this.data.alert(result.error.error_msg, 'danger');
          }

          if (result.error.error_msg == 'invalid_token') {
            this.sessionExpiredLogout();
          }
        } else {
          this.totalFiatBalance = this.data.CURRENCYICON + (result.userBalanceResult.fiat_balance).toFixed(2);
          this.fiatBalanceLabel = 'Total ' + this.data.CURRENCYNAME + ' Balance';
          if (result.userBalanceResult.btc_balance == 'null' || result.userBalanceResult.btc_balance == null) {
            this.btcBalance = '0'
          } else {
            this.btcBalance = result.userBalanceResult.btc_balance;
          }
          if (result.userBalanceResult.bch_balance == 'null' || result.userBalanceResult.bch_balance == null) {
            this.bchBalance = '0'
          } else {
            this.bchBalance = result.userBalanceResult.bch_balance;
          }
          if (result.userBalanceResult.hcx_balance == 'null' || result.userBalanceResult.hcx_balance == null) {
            this.hcxBalance = '0'
          } else {

            this.hcxBalance = result.userBalanceResult.hcx_balance;
          }
          if (result.userBalanceResult.iec_balance == 'null' || result.userBalanceResult.iec_balance == null) {
            this.iecBalance = '0'
          } else {
            this.iecBalance = result.userBalanceResult.iec_balance;
          }
          // etc
          if (result.userBalanceResult.etc_balance == 'null' || result.userBalanceResult.etc_balance == null) {
            this.etcBalance = '0'
          } else {
            this.etcBalance = result.userBalanceResult.etc_balance;
          }
          //bsv
          if (result.userBalanceResult.bsv_balance == 'null' || result.userBalanceResult.bsv_balance == null) {
            this.bsvBalance = '0'
          } else {
            this.bsvBalance = result.userBalanceResult.bsv_balance;
          }
          //
          if (result.userBalanceResult.diam_balance == 'null' || result.userBalanceResult.diam_balance == null) {
            this.diamBalance = '0'
          } else {
            this.diamBalance = result.userBalanceResult.diam_balance;
          }
          if (result.userBalanceResult.triggers_balance == 'null' || result.userBalanceResult.triggers_balance == null) {
            this.triggersBalance = '0'
          } else {
            this.triggersBalance = result.userBalanceResult.triggers_balance;
          }
          this.buyPrice = result.userBalanceResult.crypto_buy_price;
          this.btcBalanceInUsd = (parseFloat(this.btcBalance) * parseFloat(this.buyPrice)).toFixed(8);
          this.bchBalanceInUsd = (parseFloat(this.bchBalance) * parseFloat(this.buyPrice)).toFixed(3);
          this.hcxBalanceInUsd = (parseFloat(this.hcxBalance) * parseFloat(this.buyPrice)).toFixed(3);
          this.iecBalanceInUsd = (parseFloat(this.iecBalance) * parseFloat(this.buyPrice)).toFixed(3);
          //new etc
          this.etcBalanceInUsd = (parseFloat(this.etcBalance) * parseFloat(this.buyPrice)).toFixed(3);
          //bsv
          this.bsvBalanceInUsd = (parseFloat(this.bsvBalance) * parseFloat(this.buyPrice)).toFixed(3);
          //diam
          this.diamBalanceInUsd = (parseFloat(this.diamBalance) * parseFloat(this.buyPrice)).toFixed(3);

          // this.buyPrice=(this.buyPrice).toFixed(2);
          this.sellPrice = result.userBalanceResult.crypto_sell_price;
          //this.sellPrice=(this.sellPrice).toFixed(2);
          this.buyPriceText = this.data.CURRENCYICON + ' ' + this.buyPrice;
          this.sellPriceText = this.data.CURRENCYICON + ' ' + this.sellPrice;
          this.fiatBalance = result.userBalanceResult.fiat_balance;
          this.fiatBalanceText = this.data.CURRENCYICON + ' ' + result.userBalanceResult.fiat_balance;
          this.selectedCryptoCurrency = localStorage.getItem('selected_currency');
          if (result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'] == null || result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'] == 'null') {
            this.selectedCryptoCurrencyBalance = '0';
          } else {
            this.selectedCryptoCurrencyBalance = result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'];
          }
          this.btcBought = result.userBalanceResult.bitcoins_bought;
          this.btcSold = result.userBalanceResult.bitcoins_sold;
          this.bchBought = result.userBalanceResult.bitcoinCash_bought;
          this.bchSold = result.userBalanceResult.bitcoinCash_sold;
          this.hcxBought = result.userBalanceResult.hcx_bought;
          this.hcxSold = result.userBalanceResult.hcx_sold;
          this.iecBought = result.userBalanceResult.iec_bought;
          this.iecSold = result.userBalanceResult.iec_sold;
          this.ethSold = result.userBalanceResult.ethereum_sold;
          this.ethBought = result.userBalanceResult.ethereum_bought;
          //new etcBought
          this.etcSold = result.userBalanceResult.etc_sold;
          this.etcBought = result.userBalanceResult.etc_Bought;

          //new bsv
          this.bsvSold = result.userBalanceResult.bsv_sold;
          this.bsvBought = result.userBalanceResult.bsv_Bought;

          this.ltcBought = result.userBalanceResult.ltc_bought;
          this.ltcSold = result.userBalanceResult.ltc_sold;
          //diam
          this.diamBought = result.userBalanceResult.diam_bought;
          this.diamSold = result.userBalanceResult.diam_sold;

        }
      }, reason => {
        //   wip(0);
        if (reason.error.error == 'invalid_token') {
          this.data.logout();
          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }
  GetcurrencyList() {

    this.mainArr = [];
    this.http.get<any>(this.data.WEBSERVICE + '/userTrade/getCurrencyDetails')
      .subscribe(responseCurrency => {
        this.filterCurrency = responseCurrency;

        //  console.log('currencyarray+++++++++++++', this.filterCurrency);
        for (var i = 0; i <= 0; i++) {
          var currencyarray = [];
          // console.log("bata===========" + this.filterCurrency[i].currencyMasterList);
          // currencyarray = this.filterCurrency[i].currencyMasterList;
          // var logcurrency = this.filterCurrency[i].isBaseCurrency;
          if (this.filterCurrency[0].baseCurrency == 'USD') {
            this.currencylist = this.filterCurrency[0].currencyMasterList;
            var usdarraylength = this.currencylist.length;
            for (i = 0; i <= usdarraylength - 1; i++) {
              var currencyname = this.currencylist[i].currency;
              // var mainArr = [];
              this.mainArr.push({ 'currencyname': currencyname });
            }
            // console.log('UUUUUUUUUUUU++++++++++++', this.mainArr);
          }


          // if (logcurrency == 1) {
          //   this.logic = true;
          // }
          // if (currencyarray.length == 0) {
          //   //alert('jfgfg');
          //   this.flag = false;
          //   // console.log(currencyarray);
          //   //this.flag=true;
          // }
          // this.Cname = this.filterCurrency[i].currencyMasterList;
          // mainArr = this.Cname;
          // this.testval = mainArr;
        }
      }
      )
  }

}
