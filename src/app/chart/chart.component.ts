import { Component, OnInit, DoCheck } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { OrderBookComponent } from "../order-book/order-book.component";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { TradesComponent } from "../trades/trades.component";
import * as $ from "jquery";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { Action } from "rxjs/internal/scheduler/Action";

@Component({
  selector: "app-chart",
  templateUrl: "./chart.component.html",
  styleUrls: ["./chart.component.css"]
})
export class ChartComponent implements OnInit {
  filePath: string;
  fileDir: string;
  url: any = null;
  chartDataInit: any;
  chartData: any;
  selected: any;
  indicatorStatus: any = 0;
  indicatorName: any = "";
  apiUrl: string;
  apiData: any;
  loader: boolean;
  errorText: string;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  selectedCryptoCurrencySymbol: string;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  currencyListEur: any = 0;
  currencyListBtc: any = 0;
  currencyListEth: any = 0;
  currencyListUsd: any = 0;
  buyPriceText: any;
  chartD: any = "d";
  dateD: any = "1m";
  indicatorGroup1: any;
  indicatorGroup2: any;
  highValue;
  lowValue;
  cur: any;
  tools: boolean;
  chartlist: any;
  ctpdata: any = 0;
  lowprice: any = 0;
  highprice: any = 0;
  rocdata: number = 0;
  action: any;
  volumndata: any = 0;
  act: any;
  react: boolean;
  buyingAssetIssuer: string;
  sellingAssetIssuer: string;
  //rocact:any;
  rocreact: boolean;
  droc: any;
  negetive: boolean;
  Tonight: boolean;
  Tomorning: boolean;
  filterCurrency: any;
  logic: boolean;
  flag: boolean;
  Cname: any;
  testval = [];
  currency_code: any;
  base_currency: any;
  assetpairbuy: string;
  assetpairsell: string;
  responseBuySell: any;
  currencylist:any;
  constructor(
    private http: HttpClient,
    public data: CoreDataService,
    private orderBook: OrderBookComponent,
    private stoploss: StopLossComponent,
    private trade: TradesComponent,
    public dash: DashboardComponent
  ) { }

  ngOnInit() {

    // this.currencyListEur = this.data.currencyListEur;
    this.currency_code = 'BTC';
    this.base_currency = 'USD';
    // this.currencyListBtc = this.data.currencyListBtc;
    // this.currencyListUsd = this.data.currencyListUsd;
    $(".selected").removeClass("active");
    $("#" + "1m").addClass("active");

    $(".bg_new_class")
      .removeClass("bg-dark")
      .css("background-color", "#16181a");
    // $(".resizable-widget").css("border", "#000");
    //this.currencyListEth = this.data.currencyListEth;
    this.indicatorGroup1 = this.data.indicatorGroup1;
    this.indicatorGroup2 = this.data.indicatorGroup2;
    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName = this.base_currency + this.currency_code;

    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText = this.currency_code;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText = this.base_currency;
    this.selectedCryptoCurrencySymbol = this.data.selectedCryptoCurrencySymbol =
      "./assets/img/currency/" +
      this.data.selectedSellingAssetText.toUpperCase() +
      ".svg";
      this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer = "GCA6WIWOJC5CQSH5EC3F3S2UTEM62CWR64FDODH4O723AVWB4EWA7QPU";
      this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer = "GACQ5Y4W63CJANB35L3P2ZPDB7TSF3FG2QAKGBM3RPV4DBIARXRBY5Z3";

    var d = new Date();
    var y = d.getFullYear();
    var m = d.getMonth();
    var dt = d.getDate();
    var prdt = y + "-" + m + "-" + dt;
    var nxdt = y + "-" + (m + 1) + "-" + dt;
    this.apiData = {
      from_date: prdt.toString(),
      to_date: nxdt.toString(),
      chart_duration: "m",
      currency: this.data.selectedBuyingAssetText.toLocaleLowerCase(),
      base_currency: this.data.selectedSellingAssetText.toLocaleLowerCase()
    };
    this.fileDir = "./assets/data/";
    this.filePath =
      this.fileDir +
      this.data.selectedBuyingAssetText.toLocaleLowerCase() +
      this.data.selectedSellingAssetText.toLocaleLowerCase() +
      ".json";
    this.graphData(0, "", this.chartD, this.dateD);
    this.orderBook.tradePageSetup();
    $("#" + this.dateD).addClass("active");
    this.stoploss.getUserTransaction();
    $(".reset").click();

    // this.url = './assets/tradingview/index.html?cur=BTCUSD';
    this.tools = true;

    $("#dropHolder").css("overflow", "scroll");
    $(window).resize(function () {
      var wd = $("#chartHolder").width();
      $("#dropHolder").width(wd);
      $("#dropHolder").css("overflow", "scroll");
    });
    this.http
      .get<any>(
        this.data.CHARTSERVISE +
        "trendsTradeGraphFor24Hours/" +
        this.currency_code +
        "/" +
        this.base_currency
      )
      .subscribe(value => {
        if (value != "") {
          this.chartlist = value[0];
          this.ctpdata = this.chartlist.CTP;
          this.lowprice = this.chartlist.LOW_PRICE;
          this.highprice = this.chartlist.HIGH_PRICE;
          this.act = this.chartlist.ACTION;
          this.rocdata = this.chartlist.ROC.toFixed(2);
          if (this.rocdata > 0) {
            //  this.negetive =
            this.rocreact = true;
            //alert ("Hi");//this.rocreact = true}
          } else {
            //console.log(this.rocreact);
            this.rocreact = false;
          }
          if (this.rocdata < 0) {
            this.negetive = true;
          } else {
            this.negetive = false;
          }

          //this.droc = this.rocdata.
          this.volumndata = this.chartlist.VOLUME.toFixed(2);

          //  if (this.rocdata >= 0){this.rocreact = true}
          //this.droc = this.rocdata.

          if (this.rocdata >= 0) {
            this.rocreact = true;
          }

          if (this.act == "sell") {
            this.react = true;
          }
        } else {
          this.ctpdata = 0;
          this.lowprice = 0;
          this.highprice = 0;
          this.rocdata = 0;
          this.volumndata = 0;
          // alert(123);
          // console.log(123);
        }
      });
    //this.changemode();
  }
  changemode() {
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".btn")
        .removeClass("bg-black")
        .css("background-color", "#dedede");
      $(".btn").css("border-color", "#b3b4b4");
      $(".btn").css("color", "#000");
      $(".charts-tab.active").css("background-color", "#fefefe");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".btn")
        .removeClass("bg-black")
        .css("background-color", "rgb(44, 65, 66)");
      $(".btn").css("border-color", "transparent");
      $(".btn").css("color", "#fff");
      $(".charts-tab.active").css("background-color", "#242e3e");
    }
  }
  ngDoCheck() {
    this.changemode();
  }
  buySellCode(item) {
    //(item.currency=="BCC")?this.currency_code ="BCH":this.currency_code = item.currency; //changed by sanu
    // this.currency_code = item.currency;
    if(item.currency=='BCC'){
      this.currency_code='BCH';
    }
    else{
      this.currency_code = item.currency;
    }
    this.base_currency = item.baseCurrency;
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
    this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.data.selectedBuyingCryptoCurrencyName = this.base_currency + this.currency_code;
    this.selectedBuyingAssetText = item.currency;
    this.selectedSellingAssetText = item.baseCurrency;
    localStorage.setItem("buying_crypto_asset", this.currency_code.toLocaleLowerCase());
    localStorage.setItem("selling_crypto_asset", this.base_currency.toLocaleLowerCase());
    this.assetpairsell = this.currency_code + item.baseCurrency;
    this.assetpairbuy = item.baseCurrency + this.currency_code;
    this.data.selectedSellingCryptoCurrencyissuer = "";
    this.data.selectedBuyingCryptoCurrencyissuer = "";
    this.http.get<any>(this.data.WEBSERVICE + '/userTrade/getAssetIssuer')
      .subscribe(responseBuySell => {
        //   console.log('responseBuySell+++++++++++', responseBuySell);
        this.responseBuySell = responseBuySell;
        var x = this.responseBuySell.length;

        for (var i = 0; i <= x-1; i++) {
          if (this.assetpairsell == this.responseBuySell[i].assetPair) {
            this.data.selectedSellingCryptoCurrencyissuer = this.responseBuySell[i].issuer;

          }
          else if (this.assetpairbuy == this.responseBuySell[i].assetPair) {
            this.data.selectedBuyingCryptoCurrencyissuer = this.responseBuySell[i].issuer;
          }
          else if (this.data.selectedSellingCryptoCurrencyissuer != null && this.data.selectedBuyingCryptoCurrencyissuer) {
            break;
          }
          else if (this.data.selectedSellingCryptoCurrencyissuer != null && this.data.selectedBuyingCryptoCurrencyissuer) {
            alert('Not found');
          }
        }
        //  console.log('++++++++++++dhriti', this.data.selectedSellingCryptoCurrencyissuer);
        //  console.log('++++++++++++dhriti', this.data.selectedBuyingCryptoCurrencyissuer);

        //  this.currency_code = item.currencyCode;
        // this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName = this.responseBuySell[0].asset;
        //this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName = this.responseBuySell[1].asset;
        //this.data.selectedBuyingCryptoCurrencyissuer = this.responseBuySell[0].sourceacc;
        //this.data.selectedSellingCryptoCurrencyissuer = this.responseBuySell[1].sourceacc;
        //sourceacc
        //  alert("11111"+this.responseBuySell.currency_code);
        //today
        var d = new Date;
        var y = d.getFullYear();
        var m = d.getMonth();
        var dt = d.getDate();
        var prdt = y + '-' + m + '-' + dt;
        var nxdt = y + '-' + (m + 1) + '-' + dt;
        this.apiData = {
          "from_date": prdt.toString(),
          "to_date": nxdt.toString(),
          "chart_duration": "m",
          "currency": this.currency_code.toLocaleLowerCase(),
          "base_currency": this.base_currency.toLocaleLowerCase()
        };

        this.fileDir = './assets/data/';
        this.filePath = this.fileDir + this.currency_code.toLocaleLowerCase() + this.base_currency.toLocaleLowerCase() + '.json';
        // console.log('base_currency+++++++++', this.base_currency);

        this.graphData(0, '', this.chartD, this.dateD);
        this.orderBook.tradePageSetup();
        this.trade.reload();
        this.trade.getMarketTradeBuy();
        $('#' + this.dateD).addClass('active');
        this.stoploss.getUserTransaction();
        $('.reset').click();
        // this.url = './assets/tradingview/index.html?cur=BTCUSD';
        this.tools = true;
        $('#dropHolder').css('overflow', 'scroll');
        $(window).resize(function () {
          var wd = $('#chartHolder').width();
          $('#dropHolder').width(wd);
          $('#dropHolder').css('overflow', 'scroll');
        });
        this.http.get<any>(this.data.CHARTSERVISE + '/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency)
          .subscribe(value => {
            if (value != '') {
              this.chartlist = value[0];
              this.ctpdata = this.data.ctpdata = this.chartlist.CTP;
              this.lowprice = this.data.lowprice = this.chartlist.LOW_PRICE;
              this.highprice = this.data.highprice = this.chartlist.HIGH_PRICE;
              this.act = this.chartlist.ACTION;
              // console.log('ctpdata_______________', this.act, this.ctpdata);
              this.rocdata = this.chartlist.ROC.toFixed(2);
              if (this.rocdata > 0) {
                //  this.negetive =
                this.rocreact = true;
                //alert ("Hi");//this.rocreact = true}
              }
              else {
                this.rocreact = false;
              }
              if (this.rocdata < 0) {
                this.negetive = true;
              }
              else {
                this.negetive = false;
              }
              //this.droc = this.rocdata.
              this.volumndata = this.chartlist.VOLUME.toFixed(2);
              //  if (this.rocdata >= 0){this.rocreact = true}
              //this.droc = this.rocdata.

              if (this.rocdata >= 0) { this.rocreact = true }

              if (this.act == 'sell') {

                this.react = true;
              }
              else {
                this.react = false;
              }
            } else {
              this.ctpdata = 0;
              this.lowprice = 0;
              this.highprice = 0;
              this.rocdata = 0;
              this.volumndata = 0;
              // alert(123);
              // console.log(123);

            }
          })
        //new mm
      })
  }
  //selected crypto currency for buy sell
  // buySellAssetCode(buyingAsset: string = "", sellingAsset: string = ""): void {
  //   this.selectedCryptoCurrencySymbol = null;
  //   this.selectedBuyingCryptoCurrencyName = buyingAsset;
  //   this.selectedSellingCryptoCurrencyName = sellingAsset;
  //   this.selectedCryptoCurrencySymbol =
  //     "./assets/img/currency/" + buyingAsset.toUpperCase() + ".svg";
  //   this.selectedBuyingAssetText = buyingAsset.toUpperCase();
  //   this.selectedSellingAssetText = sellingAsset.toUpperCase();
  //   localStorage.setItem("buying_crypto_asset", buyingAsset);
  //   localStorage.setItem("selling_crypto_asset", sellingAsset);
  //   // asign data to service
  //   this.data.selectedBuyingCryptoCurrencyName = this.selectedBuyingCryptoCurrencyName;
  //   this.data.selectedSellingCryptoCurrencyName = this.selectedSellingCryptoCurrencyName;
  //   this.data.selectedBuyingAssetText = this.selectedBuyingAssetText;
  //   this.data.selectedSellingAssetText = this.selectedSellingAssetText;

  //   //Fetch other data
  //   this.http
  //     .get<any>(
  //       this.data.CHARTSERVISE +
  //         "trendsTradeGraphFor24Hours/" +
  //         this.selectedBuyingCryptoCurrencyName +
  //         "/" +
  //         this.selectedSellingCryptoCurrencyName
  //     )
  //     .subscribe(value => {
  //       if (value != "") {
  //         this.chartlist = value[0];
  //         this.ctpdata = this.data.ctpdata = this.chartlist.CTP;
  //         this.lowprice = this.data.lowprice = this.chartlist.LOW_PRICE;
  //         this.highprice = this.data.highprice = this.chartlist.HIGH_PRICE;
  //         this.volumndata = this.chartlist.VOLUME.toFixed(2);
  //         this.act = this.chartlist.ACTION;
  //         this.rocdata = this.chartlist.ROC.toFixed(2);

  //         if (this.rocdata > 0) {
  //           this.rocreact = true;
  //         } else {
  //           this.rocreact = false;
  //         }
  //         if (this.rocdata < 0) {
  //           this.negetive = true;
  //         } else {
  //           this.negetive = false;
  //         }
  //         if (this.act == "sell") {
  //           this.react = true;
  //         }
  //       } else {
  //         // alert(123);
  //         // console.log(123);
  //         this.ctpdata = 0;
  //         this.lowprice = 0;
  //         this.highprice = 0;
  //         this.rocdata = 0;
  //         this.volumndata = 0;
  //       }
  //     });

  //   //conditional
  //   var fileName =
  //     this.selectedBuyingCryptoCurrencyName +
  //     this.selectedSellingCryptoCurrencyName +
  //     ".json";
  //   this.filePath = this.fileDir + fileName;
  //   //conditional
  //   this.stoploss.update();
  //   this.stoploss.market = false;

  //   //update graph
  //   this.graphData(0, "", this.chartD, this.dateD);
  //   //trade
  //   this.trade.reload();
  //   this.stoploss.onlyBuyAmount = this.stoploss.onlyBuyPrice = this.stoploss.onlyBuyTotalPrice = 0;
  //   this.stoploss.onlySellAmount = this.stoploss.onlySellPrice = this.stoploss.onlySellTotalPrice = 0;
  //   $(".reset").click();
  //   $("#trade").click();

  //   //       if(this.orderBook.source){
  //   // this.orderBook.source.close();
  //   // this.orderBook.source2.close();
  //   // }
  // }
  getNewCurrency() {

    this.http.get<any>(this.data.WEBSERVICE + '/userTrade/getCurrencyDetails')
      .subscribe(responseCurrency => {
        this.filterCurrency = responseCurrency;
        var mainArr = [];
      //  console.log('currencyarray+++++++++++++', this.filterCurrency);
        for (var i = 0; i <= 2; i++) {
          var currencyarray = [];
          // console.log("bata===========" + this.filterCurrency[i].currencyMasterList);
          currencyarray = this.filterCurrency[i].currencyMasterList;
          var logcurrency = this.filterCurrency[i].isBaseCurrency;
          if (this.filterCurrency[0].baseCurrency == 'USD') {
            this.currencylist = this.filterCurrency[0].currencyMasterList;
            // console.log('this.currencylist++++++++++++', this.currencylist);
          }
          if (logcurrency == 1) {
            this.logic = true;
          }
          if (currencyarray.length == 0) {
            //alert('jfgfg');
            this.flag = false;
            // console.log(currencyarray);
            //this.flag=true;
          }
          // this.Cname = this.filterCurrency[i].currencyMasterList;
          // mainArr = this.Cname;
          // this.testval = mainArr;
        }
      }
      )
  }
  // graph data
  graphData(

    indicatorStatus: any,
    indicatorName: any,
    chartDuration: any,
    dateDuration: any
  ): void {

    this.indicatorStatus = indicatorStatus;
    this.indicatorName = indicatorName;
    this.selected = indicatorName;
    this.chartD = chartDuration;
    this.dateD = dateDuration;
    this.loader = true;
    this.errorText = null;
    $(".selected").removeClass("active");
    $("#" + dateDuration).addClass("active");
    //time
    //Current Date
    var currentDates = new Date();
    var currentYear = currentDates.getFullYear();
    var currentMonth = currentDates.getMonth() + 1;
    // alert(currentMonth);
    var currentDate = currentDates.getDate();
    var currentDateInput = currentYear + "-" + currentMonth + "-" + currentDate;

    //Three Months Previous Date
    var threeMonthsPreviousDateUnix = currentDates.setFullYear(
      currentDates.getFullYear(),
      currentMonth - 3
    );
    var threeMonthsPreviousDate = new Date(threeMonthsPreviousDateUnix);
    if (threeMonthsPreviousDate.getMonth() == 0) {
      var threeMonthsPreviousDateInput =
        threeMonthsPreviousDate.getFullYear() +
        "-12-" +
        threeMonthsPreviousDate.getDate();
    } else {
      var threeMonthsPreviousDateInput =
        threeMonthsPreviousDate.getFullYear() +
        "-" +
        threeMonthsPreviousDate.getMonth() +
        "-" +
        threeMonthsPreviousDate.getDate();
    }

    //Six Months Previous Date
    var sixMonthsPreviousDateUnix = currentDates.setFullYear(
      currentYear,
      currentMonth - 6
    );
    var sixMonthsPreviousDate = new Date(sixMonthsPreviousDateUnix);
    if (sixMonthsPreviousDate.getMonth() == 0) {
      var sixMonthsPreviousDateInput =
        sixMonthsPreviousDate.getFullYear() -
        1 +
        "-12-" +
        sixMonthsPreviousDate.getDate();
    } else {
      var sixMonthsPreviousDateInput =
        sixMonthsPreviousDate.getFullYear() +
        "-" +
        sixMonthsPreviousDate.getMonth() +
        "-" +
        sixMonthsPreviousDate.getDate();
    }

    //Year Previous Date
    var twlMonthsPreviousDateUnix = currentDates.setFullYear(
      currentYear - 1,
      currentMonth
    );
    var twlMonthsPreviousDate = new Date(twlMonthsPreviousDateUnix);
    var twlMonthsPreviousDateInput =
      twlMonthsPreviousDate.getFullYear() +
      "-" +
      twlMonthsPreviousDate.getMonth() +
      "-" +
      twlMonthsPreviousDate.getDate();

    //last Week Date
    var myDate = new Date();
    var newDate = new Date(myDate.getTime() - 60 * 60 * 24 * 7 * 1000);
    var lastWeekDate =
      newDate.getFullYear() +
      "-" +
      (newDate.getMonth() + 1) +
      "-" +
      newDate.getDate();

    //last week date
    var yesterdayDateUnix = new Date(Date.now() - 864e5);
    var yesterdayDate =
      yesterdayDateUnix.getFullYear() +
      "-" +
      (yesterdayDateUnix.getMonth() + 1) +
      "-" +
      yesterdayDateUnix.getDate();

    //1 month date
    var lastMonthDate = new Date(currentYear, currentMonth - 2, currentDate);
    var lastMonthDateForInput =
      lastMonthDate.getFullYear() +
      "-" +
      (lastMonthDate.getMonth() + 1) +
      "-" +
      lastMonthDate.getDate();

    var inputObj = {};
    if (dateDuration != undefined) {
      if (dateDuration == "3m") {
        var fromDate = threeMonthsPreviousDateInput;
      }
      if (dateDuration == "6m") {
        var fromDate = sixMonthsPreviousDateInput;
      }
      if (dateDuration == "12m") {
        var fromDate = twlMonthsPreviousDateInput;
      }
      if (dateDuration == "1w") {
        var fromDate = lastWeekDate;
      }
      if (dateDuration == "1d") {
        var fromDate = yesterdayDate;
      }
      if (dateDuration == "1m") {
        var fromDate = lastMonthDateForInput;
      }
    } else {
      var fromDate = lastMonthDateForInput;
    }

    inputObj["from_date"] = fromDate;
    inputObj["to_date"] = currentDateInput;
    if (chartDuration == undefined) {
      inputObj["chart_duration"] = "d";
    } else {
      inputObj["chart_duration"] = chartDuration;
    }
    inputObj["currency"] = this.currency_code;
    inputObj["base_currency"] = this.base_currency;
    var jsonString = JSON.stringify(inputObj);
    // console.log(jsonString);
    //time
    localStorage.setItem("dateDuration", dateDuration);
    // api call//////////////////////////////////////
    this.apiUrl = this.data.WEBSERVICE + "/userTrade/GetTradeChart";
    this.apiData = jsonString;
    this.http
      .post<any>(this.apiUrl, this.apiData, {
        headers: { "content-Type": "application/json" }
      })
      .subscribe(
        data => {
          this.chartDataInit = data.api_response;
          var apiResponse = JSON.parse(this.chartDataInit);
          var graphDataArr = [];
          if (apiResponse.length) {
            for (var i = 0; i < apiResponse.length; i++) {
              var resultArrM = [];
              // console.log(apiResponse.length);

              if (parseFloat(apiResponse[i].volume) != 0.0) {
                //  resultArrM=[];
                var dateArr = [];
                dateArr = apiResponse[i].trendTimeStamp;

                resultArrM.push(dateArr);
                resultArrM.push(parseFloat(apiResponse[i].openPrice));
                resultArrM.push(parseFloat(apiResponse[i].highPrice));
                resultArrM.push(parseFloat(apiResponse[i].lowPrice));
                resultArrM.push(parseFloat(apiResponse[i].closePrice));
                resultArrM.push(parseFloat(apiResponse[i].volume));
                graphDataArr.push(resultArrM);
              }
            }
            var buyPrice: any = parseFloat(
              apiResponse[apiResponse.length - 1].openPrice
            );
          }
          if (buyPrice) buyPrice = buyPrice > 0 ? buyPrice : "0" + buyPrice;
          else buyPrice = 0;
          this.cur =
            this.data.selectedBuyingAssetText === "USD"
              ? "$"
              : this.data.selectedSellingAssetText + " ";
          this.buyPriceText = buyPrice;

          this.http
            .get(this.filePath, {
              headers: {
                "content-Type": "application/json"
              }
            })
            .subscribe(
              subdata => {
                if (subdata) {
                  var apiResponse: any = subdata;

                  for (var i = 0; i < apiResponse.length; i++) {
                    if (parseFloat(apiResponse[i].volume) != 0.0) {
                      var resultArr = [];
                      var dateArr = [];
                      dateArr = apiResponse[i].Date;
                      resultArr.push(dateArr);
                      resultArr.push(parseFloat(apiResponse[i].Open));
                      resultArr.push(parseFloat(apiResponse[i].High));
                      resultArr.push(parseFloat(apiResponse[i].Low));
                      resultArr.push(parseFloat(apiResponse[i].Price));
                      resultArr.push(parseFloat(apiResponse[i].Vol));

                      graphDataArr.push(resultArr);
                    }
                    ``;
                  }
                }

                //  data call
                this.chartData = JSON.stringify(graphDataArr);
                localStorage.setItem("chartData", this.chartData);
                this.url =
                  "./assets/chart.html?name=" +
                  this.currency_code +
                  "&indicatorStatus=" +
                  this.indicatorStatus +
                  "&indicatorName=" +
                  this.indicatorName +
                  "&buy=" +
                  this.selectedBuyingCryptoCurrencyName +
                  "&sell=" +
                  this.selectedSellingCryptoCurrencyName +
                  "&fromDate=" +
                  fromDate;
                // this.url = './assets/tradingview/index.html?cur='+this.selectedBuyingCryptoCurrencyName.toUpperCase()+this.selectedSellingCryptoCurrencyName.toUpperCase();
                this.loader = false;
                if (this.orderBook.source12) {
                  this.orderBook.source12.close();
                  this.orderBook.source2.close();
                }
                // this.orderBook.tradePageSetup(); //changed by sanu
                this.stoploss.getUserTransaction();
              },
              error => {
                this.loader = false;
                this.errorText = error.statusText;
                this.url = null;
              }
            ); //get subscribe
        },
        error => { }
      ); //post subscribe
  }

  //clear indicators
  destroy(): void {
    this.selected = null;
    this.graphData(0, "", this.chartD, this.dateD);
  }
}
