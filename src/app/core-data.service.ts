import {
  Injectable
} from '@angular/core';
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
import {
  UserIdleService
} from 'angular-user-idle';
import { HttpClient } from '@angular/common/http';
import { ViewChild, ElementRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoreDataService {

  /* Change here */
  CURRENCYICON: any = '$ ';
  CURRENCYNAME: any = 'USD';
  exchange: any = 'Paybito Pro';
  logo: any = './assets/img/logo-white-paybito.png';
  /* environment: any = 'dev';
   ADDR = "http://52.53.237.92:8080/main.html#!/";*/

  environment: any = 'demo';
  ADDR = "http://52.53.237.92:8080/main.html#!/";
  /*Change here*/
  loader: boolean = false;
  reason: any;
  // count: any = 0;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  selectedCryptoCurrencySymbol: string;
  triggersBalance: any = 0;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  environment_settings_list: string;
  //new
  selectedBuyingCryptoCurrencyissuer: string;
  selectedSellingCryptoCurrencyissuer: string;
  ctpdata: string;
  lowprice: string;
  highprice: string;
  changescreencolor:boolean=false;
  //   currencyListUsd: any = [{
  //       'buyCode': 'btc',
  //       'buyName': 'BITCOIN',
  //       'sellCode': 'usd',
  //       'sellName': 'US DOLLAR'
  //     },
  //     {
  //       'buyCode': 'eth',
  //       'buyName': 'ETHEREUM',
  //       'sellCode': 'usd',
  //       'sellName': 'US DOLLAR'
  //     },
  //     {
  //       'buyCode': 'bsv',
  //       'buyName': 'BITCOIN SV',
  //       'sellCode': 'usd',
  //       'sellName': 'US DOLLAR'
  //     },
  //     {
  //       'buyCode': 'diam',
  //       'buyName': 'DIAM',
  //       'sellCode': 'usd',
  //       'sellName': 'US DOLLAR'
  //     },
  //     {
  //       'buyCode': 'etc',
  //       'buyName': 'ETHEREUM CLASSIC',
  //       'sellCode': 'usd',
  //       'sellName': 'US DOLLAR'
  //     },
  //     {
  //       'buyCode': 'hcx',
  //       'buyName': 'HCXCOIN',
  //       'sellCode': 'usd',
  //       'sellName': 'US DOLLAR'
  //     },
  //     {
  //       'buyCode': 'bch',
  //       'buyName': 'BITCOIN-CASH',
  //       'sellCode': 'usd',
  //       'sellName': 'US DOLLAR'
  //     },
  //     {
  //       'buyCode': 'ltc',
  //       'buyName': 'LITECOIN',
  //       'sellCode': 'usd',
  //       'sellName': 'US DOLLAR'
  //     }
  //   ];
  //   currencyListHCX: any = [{
  //     'buyCode': 'bsv',
  //     'buyName': 'BITCOIN SV',
  //     'sellCode': 'hcxb',
  //     'sellName': 'HASHCASH COIN'
  //   },
  //   {
  //     'buyCode': 'etc',
  //     'buyName': 'ETHEREUM CLASSIC',
  //     'sellCode': 'hcxb',
  //     'sellName': 'HASHCASH COIN'
  //   },
  //   {
  //     'buyCode': 'bch',
  //     'buyName': 'BITCOIN-CASH',
  //     'sellCode': 'hcxb',
  //     'sellName': 'HASHCASH COIN'
  //   },
  //   {
  //     'buyCode': 'diam',
  //     'buyName': 'DIAM',
  //     'sellCode': 'hcxb',
  //     'sellName': 'HASHCASH COIN'
  //   },
  //   {
  //     'buyCode': 'ltc',
  //     'buyName': 'LITECOIN',
  //     'sellCode': 'hcxb',
  //     'sellName': 'HASHCASH COIN'
  //   },
  //   {
  //     'buyCode': 'eth',
  //     'buyName': 'ETHEREUM',
  //     'sellCode': 'hcxb',
  //     'sellName': 'HASHCASH COIN'
  //   },
  //   {
  //     'buyCode': 'btc',
  //     'buyName': 'BITCOIN',
  //     'sellCode': 'hcxb',
  //     'sellName': 'HASHCASH COIN'
  //   }

  // ];
  //   currencyListBtc: any = [{
  //       'buyCode': 'bch',
  //       'buyName': 'BITCOIN-CASH',
  //       'sellCode': 'btc',
  //       'sellName': 'BITCOIN'
  //     },
  //     // {
  //     //   'buyCode': 'iec',
  //     //   'buyName': 'IECCOIN',
  //     //   'sellCode': 'btc',
  //     //   'sellName': 'BITCOIN'
  //     // },
  //     {
  //       'buyCode': 'bsv',
  //       'buyName': 'BITCOIN SV',
  //       'sellCode': 'btc',
  //       'sellName': 'BITCOIN'
  //     },
  //     {
  //       'buyCode': 'etc',
  //       'buyName': 'ETHEREUM CLASSIC',
  //       'sellCode': 'btc',
  //       'sellName': 'BITCOIN'
  //     },
  //     {
  //       'buyCode': 'eth',
  //       'buyName': 'ETHEREUM',
  //       'sellCode': 'btc',
  //       'sellName': 'BITCOIN'
  //     },
  //     {
  //       'buyCode': 'hcx',
  //       'buyName': 'HCXCOIN',
  //       'sellCode': 'btc',
  //       'sellName': 'BITCOIN'
  //     },
  //     {
  //       'buyCode': 'ltc',
  //       'buyName': 'LITECOIN',
  //       'sellCode': 'btc',
  //       'sellName': 'BITCOIN'
  //     },
  //     {
  //       'buyCode': 'diam',
  //       'buyName': 'DIAM',
  //       'sellCode': 'btc',
  //       'sellName': 'BITCOIN'
  //     }
  //   ];
  //   currencyListEth: any = [{
  //       'buyCode': 'ltc',
  //       'buyName': 'LITECOIN',
  //       'sellCode': 'eth',
  //       'sellName': 'ETHEREUM'
  //     },
  // {
  //   'buyCode': 'iec',
  //   'buyName': 'IECCOIN',
  //   'sellCode': 'eth',
  //   'sellName': 'ETHEREUM'
  // },
  //   {
  //     'buyCode': 'bsv',
  //     'buyName': 'BITCOIN SV',
  //     'sellCode': 'eth',
  //     'sellName': 'ETHEREUM'
  //   },
  //   {
  //     'buyCode': 'etc',
  //     'buyName': 'ETHEREUM CLASSIC',
  //     'sellCode': 'eth',
  //     'sellName': 'ETHEREUM'
  //   },
  //   {
  //     'buyCode': 'hcx',
  //     'buyName': 'HCXCOIN',
  //     'sellCode': 'eth',
  //     'sellName': 'ETHEREUM'
  //   },
  //   {
  //     'buyCode': 'diam',
  //     'buyName': 'DIAM',
  //     'sellCode': 'eth',
  //     'sellName': 'ETHEREUM'
  //   }
  // ];

  indicatorGroup1: any = ['ATR', 'BBAND', 'MACD', 'EMA', 'ROC', 'KDJ', 'MFI', 'CMF'];
  indicatorGroup2: any = ['ARN', 'CHO', 'HA', 'KCH', 'SSMA', 'SOSC', 'Willams %r', 'TRIX'];

  interval: any;
  time: any;
  url: boolean = false;
  resize: boolean = false;
  icon: any;
  WEBSERVICE: string;
  REPORTSERVISE: string;
  TRADESERVICE: string;
  CHARTSERVISE: string;
  //malini CURRENCYSERVISE
  CURRENCYSERVISE: string;
  //end
  settings: any;
  count: any;
  // MARGINURL = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/Margin/";
  // LENDINGURL = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/Margin/trade/";
  BUYURL = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/TradeAmount/rest/MyAmount/getAmountBuy/";
  SELLURL = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/TradeAmount/rest/MyAmount/getAmountSell/";
  MARGINURL = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/Margin/";
  LENDINGURL = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/Margin/trade/";
  // BUYURL = "http://13.52.130.206:7080/TradeAmount/rest/MyAmount/getAmountSell/";
  // SELLURL = "http://13.52.130.206:7080/TradeAmount/rest/MyAmount/getAmountBuy/";
  constructor(private route: Router, private userIdle: UserIdleService, private http: HttpClient) {

    var alertPl = `<div class="alertPlace"></div>`;
    $('html').append(alertPl).fadeIn();

    switch (this.environment) {
      case 'live':
        // this.WEBSERVICE = "https://pbnew.hashcashconsultants.com/paybito_web_trade"; //live
        // this.TRADESERVICE = "https://pbnew.hashcashconsultants.com/TradeAmount/rest/MyAmount"; //live
        // this.CHARTSERVISE = "https://pbnew.hashcashconsultants.com/TrendSpriceVolume/paybito/"; //live
        // this.REPORTSERVISE = "https://pbnew.hashcashconsultants.com/PaybitoReportModule/report/"; //live
        this.WEBSERVICE = "https://pbnew.hashcashconsultants.com/paybito_web_trade"; //live
        this.TRADESERVICE = "https://pbnew.hashcashconsultants.com/TradeAmount/rest/MyAmount"; //live

        break;

      case 'dev':
        //malini CURRENCYSERVISE
        // this.CURRENCYSERVISE = "http://ec2-52-53-80-167.us-west-1.compute.amazonaws.com:7020/"
        // this.WEBSERVICE = "http://ec2-52-53-80-167.us-west-1.compute.amazonaws.com:7080/paybito_web_trade"; // dev
        // this.TRADESERVICE = "http://ec2-52-53-80-167.us-west-1.compute.amazonaws.com:9080/TradeAmount/rest/MyAmount"; // dev
        // this.CHARTSERVISE = "http://ec2-52-53-80-167.us-west-1.compute.amazonaws.com:7080/TrendSpriceVolume/paybito/"; //dev
        // this.REPORTSERVISE = "http://ec2-52-53-80-167.us-west-1.compute.amazonaws.com:7080/PaybitoReportModule/report/"; //dev
        // break;
           this.WEBSERVICE = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/paybito_web_trade"; // dev
          this.TRADESERVICE = "http://13.52.130.206:7080/TradeAmount/rest/MyAmount"; // dev
          this.CHARTSERVISE="http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/TrendSpriceVolume/paybito/"
          break;
      case 'uat':
        this.WEBSERVICE = "http://ec2-13-52-15-148.us-west-1.compute.amazonaws.com:7080/paybito_web_trade"; // uat
        this.TRADESERVICE = "http://ec2-13-52-15-148.us-west-1.compute.amazonaws.com:9080/TradeAmount/rest/MyAmount"; // uat
        // this.TRADESERVICE = "http://localhost:9080/TradeAmount/rest/MyAmount"; // uat
        this.CHARTSERVISE = "http://ec2-13-52-15-148.us-west-1.compute.amazonaws.com:13080/TrendSpriceVolume/paybito/"; //dev
        break;

      case 'demo':
        // this.WEBSERVICE = "http://ec2-52-8-64-240.us-west-1.compute.amazonaws.com:7080/paybito_web_trade"; // demo
        // this.TRADESERVICE = "http://ec2-52-8-64-240.us-west-1.compute.amazonaws.com:9080/TradeAmount/rest/MyAmount"; // demo
       
        this.WEBSERVICE = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/paybito_web_trade"; // dev
        this.TRADESERVICE = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/TradeAmount/rest/MyAmount"; // dev
        this.CHARTSERVISE="http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/TrendSpriceVolume/paybito/";//demo
        this.REPORTSERVISE = "http://ec2-13-52-20-196.us-west-1.compute.amazonaws.com:8080/PaybitoReportModule/report/";
        break;

    }

    this.getSettings();
  }

  getSettings() {
    this.http.get<any>('./assets/settings.json')
      .subscribe(data => {
        this.settings = data;
      })
  }


  idleLogout() {
    var us = localStorage.getItem('access_token');
    if (us != null) {
      //this.userIdle.onTimerStart();
      this.userIdle.startWatching();
      this.userIdle.resetTimer();
      // this.userIdle.onTimerStart().subscribe(count => 
      //   console.log(count));
      this.userIdle.onTimerStart().subscribe(count => {
        //  console.log(this.time);
        // console.log('count', count);
        if (count == 1) {
          $('#logoutWarn').click();
        }
        else if (count == 10) {

          $('#logoutWarncl').click();

          this.logout();
        }
        this.count = count;
      });
      this.userIdle.ping$.subscribe(() => console.log("PING"));
      this.userIdle.onTimeout().subscribe(() => {
      //  console.log('Time is up!');
        $('#logoutWarncl').click();
      }
      );

    }

  }
  continue() {
    this.Stop();
    this.stopWatching();
  }
  startWatching() {
    this.userIdle.startWatching();
  }
  stopWatching() {
    this.userIdle.stopWatching();

  }

  Stop() {
    $('#logoutWarncl').click();
    this.userIdle.stopTimer();
    this.userIdle.stopWatching();
    $('#logoutWarncl').click();
    // this.userIdle.resetTimer();
    // this.restart();
    var presentRefreshToken = localStorage.getItem('refresh_token');
    var fd = new FormData();
    fd.append('refresh_token', presentRefreshToken);
    fd.append('grant_type', 'refresh_token');
    //this.data.Stop();
    this.http.post<any>(this.WEBSERVICE + '/oauth/token?grant_type=refresh_token&refresh_token=' + presentRefreshToken, '', {
      headers: {
        // 'Content-Type': 'application/x-www-form-urlencoded',
        'authorization': 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg=='
      }
    })
      .subscribe(response => {
        var result = response;
        localStorage.setItem('access_token', result.access_token);
        localStorage.setItem('refresh_token', result.refresh_token);

        // var expiresTime=result.expires_in;
        // expiresTime=expiresTime*1000;
        // var start_time=$.now();
        // var expiresIn=start_time+expiresTime;
        // localStorage.setItem('expires_in',expiresIn);

        //get user details
        var userObj = {};
        userObj['user_id'] = localStorage.getItem('user_id');
        var userJsonString = JSON.stringify(userObj);
        // wip(1);
        var accessToken = localStorage.getItem('access_token');
        this.http.post<any>(this.WEBSERVICE + '/user/GetUserDetails', userJsonString, {
          headers: {
            'Content-Type': 'application/json',
            'authorization': 'BEARER ' + accessToken,
          }
        })
          .subscribe(response => {
            // wip(0);
            var result = response;
            if (result.error.error_data != '0') {
              this.alert(result.error.error_msg, 'danger');
            } else {
              localStorage.setItem('user_name', result.userResult.full_name);
              localStorage.setItem('user_id', result.userResult.user_id);
              localStorage.setItem('phone', result.userResult.phone);
              localStorage.setItem('email', result.userResult.email);
              localStorage.setItem('address', result.userResult.address);
              localStorage.setItem('profile_pic', result.userResult.profile_pic);
              //  clearInterval(this.data.interval);
              //console.log('this.data.interval++++++++++++',this.interval);
              //  this.data.time = 10;

              this.idleLogout();
              //this.data.restart();
              //  location.reload();
              // window.location='main.html';
              //    $('#sessionExpireModal').modal('hide');
              //    clearInterval($scope.expireTimerInterval);
            }
          });

      }, reason => {
        //   wip(0);
        //this.alert(reason.error.message,'danger');
        //this.data.idleLogout();
        location.reload();
        this.logout();
      });
  }
  restart() {
    this.userIdle.resetTimer();
  }
  onTimeout() {
    this.userIdle.onTimeout();
  }
  readable_timestamp(t) {
    // Split timestamp into [ Y, M, D, h, m, s ]
    var t: any = t.split(/[- :]/);

    // Apply each element to the Date function
    var d = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));

    //handle parsing GMT as UTC
    d.setMinutes(d.getMinutes() - 330);

    t = d.toTimeString();
    t = t.split(" ");
    return d.toDateString() + " " + t[0];
  }

  logout() {
    localStorage.clear();
    this.Stop();
    //  location.reload();
    caches.delete;
    this.route.navigateByUrl('/login');
  }

  alert(msg, type, time = 3000) {
    //console.log(msg, type);
    this.reason = msg;
    this.icon = 'puff';

    if (msg == 'Loading...') {
      this.loader = true;
      setTimeout(() => {
        this.loader = false;
      }, 10000);
    } else {
      $('.alert:first').fadeOut();
      var htx = `<div class="alert alert-` + type + ` my-2" role="alert">` + msg + `</div>`;
      $('.alertPlace').append(htx).fadeIn();
      setTimeout(() => {
        $('.alert:last').remove().fadeOut();
      }, time);
    }
  }

}
