import { Component, OnInit } from "@angular/core";
import { CoreDataService } from "../core-data.service";
import { Router } from "@angular/router";
import * as $ from "jquery";
import { OrderBookComponent } from "../order-book/order-book.component";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  alertType: any;
  alertMsg: any;
  chart: any = true;
  orderbook: any = true;
  trade: any = true;
  stoploss: any = true;
  drag: any = false;
  mode: any = false;
  chartlist: any;
  lowprice: any;
  highprice;
  any = 0;
  ctpdata: any = 0;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  screencolor: boolean;
  constructor(
    public data: CoreDataService,
    private route: Router,
    private http: HttpClient
  ) {}
  ngOnInit() {
    this.selectedSellingCryptoCurrencyName = "usd";
    this.selectedBuyingCryptoCurrencyName = "btc";
    this.data.changescreencolor = false;
    this.http
      .get<any>(
        this.data.CHARTSERVISE +
          "trendsTradeGraphFor24Hours/" +
          this.selectedBuyingCryptoCurrencyName +
          "/" +
          this.selectedSellingCryptoCurrencyName
      )
      .subscribe(value => {
        if (value != "") {
          this.chartlist = value[0];
          this.ctpdata = this.data.ctpdata = this.chartlist.CTP;
          this.lowprice = this.data.lowprice = this.chartlist.LOW_PRICE;
          this.highprice = this.data.highprice = this.chartlist.HIGH_PRICE;

          // alert(123);
          // console.log(123);
          // this.ctpdata= 0;
          // this.lowprice= 0;
          // this.highprice= 0;
        }
      });
    $(document).ready(function() {
      $(this).scrollTop(0);
      var i = 1;

      $(".drg").click(function() {
        i++;
        $(this).css("z-index", i);
      });

      $(':input[type="number"]').keyup(function() {
        //  console.log('edde');
      });
    });
  }
  changebg(val) {
    this.screencolor = val;
    this.data.changescreencolor = val;
    if (this.data.changescreencolor == true) {
      $(".content-wrapper").css("background-color", "#ececec").addClass("intro");
      document.getElementById("night").style.display = "block";
      document.getElementById("light").style.display = "none";
    } else {
      $(".content-wrapper").css("background-color", "#060707").removeClass("intro");
      //$("body").css("color", "white");
      document.getElementById("light").style.display = "block";
      document.getElementById("night").style.display = "none";
    }
  }
  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);

    var maxVal1: number = parseInt(maxVal);

    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }
}
