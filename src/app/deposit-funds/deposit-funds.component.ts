import {
  Component,
  OnInit,Input
} from '@angular/core';
import {
  BodyService
} from '../body.service';
import {
  CoreDataService
} from '../core-data.service';
import {
  HttpClient
} from '@angular/common/http';
import * as $ from 'jquery';
import {
  NgbModal,NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi there!</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Hello, {{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name;

  constructor(public activeModal: NgbActiveModal) {}
}
@Component({
  selector: 'app-deposit-funds',
  templateUrl: './deposit-funds.component.html',
  styleUrls: ['./deposit-funds.component.css']
})
export class DepositFundsComponent implements OnInit {

  invoiceList: any;
  public transactionid: any;
  public MSID: any;
  constructor(private modalService: NgbModal,private router: Router, private route: ActivatedRoute, public main: BodyService, public data: CoreDataService, private http: HttpClient) {
    this.main.getUserTransaction();
  }
  fiatBalanceText: string;
  CURRENCYNAME;
  depositDisclaimer;
  loadMaxValue;
  loadMinValue;
  invoiceOrderNo;
  invoiceAmount;

  ngOnInit() {
    this.paymentOrderList(1);
    this.getAdminBankDetails();
    this.fiatBalanceText = this.main.fiatBalanceText;
    this.CURRENCYNAME = this.data.CURRENCYNAME;

    //Extracting deposit disclaimer, min and max value
    var environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
    this.depositDisclaimer = environmentSettingListObj.load_max_value.description;
    this.loadMaxValue = environmentSettingListObj['load_max_value']['value'];
    this.loadMinValue = environmentSettingListObj['load_min_value']['value'];

  }
  deposit_amount;
  orderNoToShow;

  depositAmount(content) {
    if (this.deposit_amount != undefined || this.deposit_amount == 0) {
      if (this.main.userDocVerificationStatus() && this.main.userBankVerificationStatus()) {
        if (parseFloat(this.deposit_amount) <= parseFloat(this.loadMaxValue) && parseFloat(this.deposit_amount) >= parseFloat(this.loadMinValue)) {
          var depositObj = {};
          depositObj['amount'] = this.deposit_amount;
          depositObj['payment_method'] = 1;
          depositObj['user_id'] = localStorage.getItem('user_id');
          var jsonString = JSON.stringify(depositObj);
          this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/AddPaymentOrder', jsonString, {
            headers: {
              'Content-Type': 'application/json',
              'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
          })
            .subscribe(response => {
              var result = response;
              if (result.error.error_data != '0') {
                this.deposit_amount = '';
                this.data.alert(result.error.error_msg, 'warning');
              } else {
                this.modalService.open(content, {
                  centered: true
                });
                this.deposit_amount = '';
                this.paymentOrderList(1);
                this.main.getUserTransaction();
                this.getAdminBankDetails();
                this.orderNoToShow = result.paymentOrdersListResult[0].order_no;
                // $('.order_no').html(this.orderNo);
                // $('#order_id_modal').modal('show');
              }
            }, function (reason) {
              if (reason.data.error == 'invalid_token') {
                this.data.logout();
              } else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'danger');
              }
            });

        } else {
          this.data.alert(this.depositDisclaimer, 'warning');
        }
      } else {
        $('#payn').attr('disabled', true);
        this.data.alert('Your Bank Details are being verified', 'warning');
      }
    } else {
      this.data.alert('Please Provide Deposit Amount', 'info');
    }

  }

  totalCount;
  orderNo;
  noOfItemPerPage = '20';

  paymentOrderList(pageNo) {
    var paymentOrderObj = {};
    paymentOrderObj['user_id'] = localStorage.getItem('user_id');
    paymentOrderObj['page_no'] = pageNo.toString();
    paymentOrderObj['no_of_items_per_page'] = this.noOfItemPerPage;
    var jsonString = JSON.stringify(paymentOrderObj);
    this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/GetAllInvoiceList', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        var result = response;
        if (result.error.error_data != '0') {
          alert(result.error.error_msg);
        } else {
          var paymentOrdersListResult = result.invoicesListResult;
          var paymentOrderListHtml = '';
          this.totalCount = result.totalCount;
          $('.payment_order_list_body').html('');
          if (paymentOrdersListResult != null) {
            this.invoiceList = paymentOrdersListResult;

            for (var i = 0; i < paymentOrdersListResult.length; i++) {
              var timestamp = paymentOrdersListResult[i].created;
              var timestampArr = timestamp.split('.');
              timestamp = timestampArr[0];
              if (paymentOrdersListResult[i].paymentOrders.status == '15') {
                var status = 'Order Received';
                var actionText = 'View Invoice';
                var actionTextColor = 'text-blue';
              }
              if (paymentOrdersListResult[i].paymentOrders.status == '16') {
                var status = 'Reference Updated';
                var actionText = 'Update Reference Number';
                var actionTextColor = 'text-yellow';
              }
              if (paymentOrdersListResult[i].paymentOrders.status == '17') {
                var status = 'Order Confirm';
                var actionText = 'Order Confirm';
                var actionTextColor = 'text-green';
              }
              this.orderNo = paymentOrdersListResult[i].paymentOrders.order_no;
              paymentOrderListHtml += '<tr>';
              paymentOrderListHtml += '<td>' + timestamp + '</td>';
              paymentOrderListHtml += '<td>' + this.orderNo + '</td>';
              paymentOrderListHtml += '<td class="text-white">' + ' ' + (paymentOrdersListResult[i].fiat_amount).toFixed(4) + '</td>';
              paymentOrderListHtml += '<td><span class="text-blue" data-order-id="' + this.orderNo + '" data-invoice-id="' + paymentOrdersListResult[i].invoice_id + '" onclick="angular.element(this).scope().getInvoiceDetails(this)" style="cursor:pointer;">View Invoice</span></td>';
              paymentOrderListHtml += '</tr>';
            }
            this.main.pagination(this.totalCount, this.noOfItemPerPage, 'paymentOrderList');
          } else {
            paymentOrderListHtml += '<tr colspan="5">No Data Exist</tr>';
          }
          //   $('.payment_order_list_body').html(paymentOrderListHtml);
        }

      }, function (reason) {
        //   wip(0);
        if (reason.data.error == 'invalid_token') {
          this.data.logout();
        } else {
          this.data.logout();
          this.data.alert('Could Not Connect To Server', 'danger');
        }
      });
  }

  bankName;
  routingNo;
  accountNo;
  bankUserName;
  bankAddress;
  currentbalence;
  currencyname;
  dep_amount;
  disclaim: boolean;
  password;
  TransactionId;
  fname;
  lname;
  Name;
  email;
  getAdminBankDetails() {
    var bankDetailsObj = {};
    bankDetailsObj['user_id'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(bankDetailsObj);
    this.http.post<any>(this.data.WEBSERVICE + '/user/GetAdminBankDetails', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        var result = response;
        if (result.error.error_data != '0') {
          //    alert(result.error.error_msg);
        } else {
          this.bankName = result.bankDetails.bank_name;
          this.routingNo = result.bankDetails.routing_no;
          this.accountNo = result.bankDetails.account_no;
          this.routingNo = result.bankDetails.routing_no;
          this.bankUserName = result.bankDetails.bank_user_name;
          this.bankAddress = result.bankDetails.bank_address;

        }
      }, function (reason) {
        //   wip(0);
        if (reason.data.error == 'invalid_token') {
          this.data.logout();
        } else {
          this.data.logout();
          this.data.alert('Could Not Connect To Server', 'danger');
        }
      });

  }

  invoice(content, id, amount) {
    this.modalService.open(content, {
      centered: true
    });
    this.invoiceOrderNo = id;
    this.invoiceAmount = amount;
  }
  myFunction() {
    // Get the checkbox
    var checkBox = document.getElementById("disclaim");
    // Get the output text
    var text = document.getElementById("text");
    this.TransactionId = localStorage.getItem('user_id') + Date.now();
  //  console.log("+++++++++", this.TransactionId);
    //this.password =Md5.hashStr('VIP2019@!$') ;
    //  alert( this.password);
    // If the checkbox is checked, display the output text
    var checked = document.forms["uc-cart-checkout-form"]["disclaim"].checked;
    //alert(checked);
    if (checked == true) {
      this.disclaim = true;
    } else {
      this.disclaim = false;
    }
  }
  Opendisclaimer(depodisc, CURRENCYNAME, balence) {
    this.modalService.open(depodisc, {
      centered: true
    });
    this.currencyname = CURRENCYNAME;
    this.currentbalence = balence;
    this.dep_amount = ((<HTMLInputElement>document.getElementById("deposit_amount")).value);
    this.Name = localStorage.getItem('user_name');
    //this.Name = "Amit Bhunia";
   // console.log('++++++++++++email', this.Name);
    var temp=this.Name.split(" ")
    this.fname=temp[0];
    this.lname=temp[1];
    if(this.lname=="undefined" || this.lname=="")
    this.lname = '';
    //console.log('++++++++++++',this.fname,this.lname);
    this.email = localStorage.getItem('email');
   // console.log('++++++++++++email', this.email);
  }

}
