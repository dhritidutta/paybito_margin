import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  email: any;
  password: any;
  secure_token: any;
  repassword: any;

  constructor(private http:HttpClient, public data:CoreDataService, private route:Router) { }

  ngOnInit() {
    
  }

confirmPassword(){
    if(this.password==this.repassword){
      $('.confirm_password_text').html('Password Matched');
      $('.confirm_password_text').css('color','green');
      $('.submit_btn').removeAttr('disabled');
    }else{
      $('.confirm_password_text').html('Password  Mismatched');
      $('.confirm_password_text').css('color','#bf260c');
      $('.submit_btn').attr('disabled','disabled');
    }
  }
 //resert function
forgotPassword(isValid){
   if(isValid){
    // $('.submit_btn').removeAttr('disabled');
     var forgotPasswordObj={};
     forgotPasswordObj['email']=this.email;
     forgotPasswordObj['password']=this.password;
     forgotPasswordObj['secure_token']=this.secure_token;
     var jsonString=JSON.stringify(forgotPasswordObj);
    //  wip(1);
     //login webservice
     this.http.post<any>(this.data.WEBSERVICE+'/user/ResetPassword',jsonString,{headers: {
         'Content-Type': 'application/json'
       }})
    .subscribe(response=>{
      //  wip(0);
       var result=response;
       if(result.error.error_data!='0'){
         this.data.alert(result.error.error_msg,'danger');
       }else{
        /* $.confirm({
           title: 'Success',
           content: 'Password successfully reset',
           type: 'green',
           buttons: {
               ok: function () {
                  window.location='login.html';
           }
         }
       });*/

       this.data.alert('Password successfully reset','success');
       this.route.navigateByUrl('/login');

       }

     },function(reason){
      //  wip(0);
       this.data.alert('Internal Server Error','danger')
     });
   }else{
    //$('.submit_btn').attr('disabled','disabled');
     this.data.alert('Please provide valid email','warning');
   }
 }

}
