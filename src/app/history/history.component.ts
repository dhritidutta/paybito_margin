import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { BodyService } from '../body.service';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  constructor(public main:BodyService) { }

  noOfItemPerPage;
  timeSpan;
  collection;
  page=1;

  ngOnInit() {
    this.main.getDashBoardInfo();
    this.main.transactionHistory(1);
    this.collection = this.main.noOfItemPerPage;
    $('.historyTableBody').html(this.main.historyTableTr);


  }

  pager(pg){
    this.page = pg;
    this.main.transactionHistory(pg);
  }
  pagerNext(pg){
    pg++;
    this.page = pg;
    this.main.transactionHistory(pg);
  }
  pagerPre(pg){
    pg--;
    this.page = pg;
    this.main.transactionHistory(pg);
  }



  //function for get duration
  getDuration(duration){
      this.main.timeSpan=duration;
      this.main.transactionHistory('1');
      if(duration=='all'){
          $('.filter-button').removeClass('btn_active');
          $('.all_btn').addClass('btn_active');

      }
       if(duration=='last week'){
           $('.filter-button').removeClass('btn_active');
          $('.last_week_btn').addClass('btn_active');


      }
      if(duration=='last month'){
          $('.filter-button').removeClass('btn_active');
          $('.last_month_btn').addClass('btn_active');
      }


  }

}
