import {
  Component,
  OnInit
} from '@angular/core';
import {
  BodyService
} from '../body.service';
import {
  HttpClient
} from '@angular/common/http';
import {
  CoreDataService
} from '../core-data.service';
import * as $ from "jquery";
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-identity-verification',
  templateUrl: './identity-verification.component.html',
  styleUrls: ['./identity-verification.component.css']
})
export class IdentityVerificationComponent implements OnInit {

  constructor(
    public main: BodyService,
    private http: HttpClient,
    public data: CoreDataService,
    private modalService: NgbModal,
    private route: Router) { }
  public contries: any;
  currentDate:any=new Date().toISOString().slice(0,10);
  currentYear:any=new Date().getFullYear();
  currentMonth:any=(new Date().getMonth())+1;
  currentDay:any=new Date().getDate();

  ngOnInit() {
    this.main.getDashBoardInfo();
    this.getIdentityDetails();
    this.getLoc();
  }
  getLoc() {
    this.http.get<any>('./assets/data/country.json')
      .subscribe(data => {
        this.contries = data;
      })
  }
  panCardPic: any;
  aadharCardFront: any;
  aadharCardBack: any;

  getIdentityDetails() {
   this.data.alert('Loading...', 'dark');
    var identityObj = {};
    identityObj['user_id'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(identityObj);
    // wip(1);
    this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserDetails', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {

        // wip(0);
        var result = response;
        //   console.log(response);

        if (result.error.error_data) {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          $('.pan_card').val(result.userResult.ssn);
          if (result.userResult.profile_pic != '') {
            this.main.profilePic = this.data.WEBSERVICE + '/user/' + localStorage.getItem('user_id') + '/file/' + result.userResult.profile_pic + '?access_token=' + localStorage.getItem('access_token');
          } else {
            this.main.profilePic = './assets/img/default.png';
          }
          if (result.userResult.id_proof_doc != '') {
            this.panCardPic = this.data.WEBSERVICE + '/user/' + localStorage.getItem('user_id') + '/file/' + result.userResult.id_proof_doc + '?access_token=' + localStorage.getItem('access_token');
          } else {
            this.panCardPic = './assets/img/file-empty-icon.png';
          }
          if (result.userResult.address_proof_doc != '') {
            this.aadharCardFront = this.data.WEBSERVICE + '/user/' + localStorage.getItem('user_id') + '/file/' + result.userResult.address_proof_doc + '?access_token=' + localStorage.getItem('access_token');
          } else {
            this.aadharCardFront = './assets/img/file-empty-icon.png';
          }
          if (result.userResult.address_proof_doc_2 != '') {
            this.aadharCardBack = this.data.WEBSERVICE + '/user/' + localStorage.getItem('user_id') + '/file/' + result.userResult.address_proof_doc_2 + '?access_token=' + localStorage.getItem('access_token');
          } else {
            this.aadharCardBack = './assets/img/file-empty-icon.png';
          }


        }
      }, function (reason) {
        // wip(0);
        if (reason.data.error == 'invalid_token') {
          this.data.logout();
        } else {
          this.data.logout();
          this.data.alert('Could Not Connect To Server', 'danger');
        }
      }, () => {
        this.data.loader = false;
      });
  }

  upload(content) {
    if (
      $('.document_front_side')[0].files[0] != undefined || $('.document_back_side')[0].files[0] != undefined)
    // $('.aadhar_card_front_side')[0].files[0] != undefined || $('.aadhar_card_back_side')[0].files[0] != undefined
    {
      this.modalService.open(content);
    } else {
      this.data.alert('Please submit a detail to update', 'warning');
    }
  }
  docCountry:'';
  dateofbirth;
  doctype;
  uploadDocs() {

    this.data.alert('Loading...', 'dark');
    var fd = new FormData();
    fd.append('docCountry', this.docCountry);
  //  console.log(fd);
    fd.append('user_id', localStorage.getItem('user_id'));
    var ds = this.dateofbirth;
  var date = ds.day + '-' + ds.month + '-' + ds.year;
    fd.append('dob', date);
    
       // console.log(this.dateofbirth.day);
    fd.append('docType', this.doctype);
    //  fd.append('ssn', $('.pan_card').val());
    if ($('.document_front_side')[0].files[0] != undefined) {
      fd.append('scanData', $('.document_front_side')[0].files[0]);
    } else {
      fd.append('scanData','');
    }
   
    if ($('.document_back_side')[0].files[0] != undefined) {
      fd.append('backsideImageData', $('.document_back_side')[0].files[0]);
    } else {
      fd.append('backsideImageData', '');
    }

    this.http.post<any>(this.data.WEBSERVICE + '/user/UpdateUserDocs', fd, {
      headers: {
        "Authorization": "BEARER " + localStorage.getItem('access_token')
      }
    }).subscribe(result => {
      this.data.loader = false;

      if (result.error.error_data != '0') {
        this.data.alert(result.error.error_msg, 'dark');
      } else {
        this.data.alert('Identity Verification Documents Submitted', 'success');
        this.getIdentityDetails();
      }


    }, error => {
      this.data.alert(error.error.error_description, 'danger');
    });

  }

  getSize(content) {
    var sz = $('#' + content)[0].files[0];
    // console.log(sz);

    if (sz.type == "image/jpeg") {
      if (sz.size > 5000000) {
        this.data.alert('File size should be less than 2MB', 'warning');
        $('#' + content).val('');
      }
    }
    else {
      this.data.alert('File should be in JPG or JPEG. ' + sz.type.split('/')[1].toUpperCase() + ' is not allowed', 'warning');
      $('#' + content).val('');
    }
  }

}
