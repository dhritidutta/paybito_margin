import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { OrderBookComponent } from '../order-book/order-book.component';
import { TradesComponent } from '../trades/trades.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:any;
  password:any;
  userId:any;
  error:boolean;
  accessToken:any;
  refreshToken:any;
  expiresIn:any;
  logoutReason:any;
  loader: boolean;
  currentRoute;
  otpBlock: boolean = false;
  otp: any;

  constructor(
    private http:HttpClient,
    public data:CoreDataService,
    private cookie:CookieService,
    private route:Router,
    private nav:NavbarComponent,
    private orderBook:OrderBookComponent,
    private trade:TradesComponent) { }

  ngOnInit() {
    //caches.delete;
    /* $(function(){
      $('a#tab1').click(function(){
        location.href = 'https://www.paybito.com/dashboard/login';
      });
    }); */
    this.error = false;
    this.loader = false;
    this.logoutReason = this.nav.reason;
    this.route.events.subscribe(val =>{
      this.currentRoute = val;
      if(this.currentRoute.url!='/dashboard'&&this.orderBook.source12){
        this.orderBook.source12.close();
        this.orderBook.source2.close();
        //this.trade.getMarketTradeBuy.close();
      }
    });
  }

  basic(){
    //location.href = 'https://www.paybito.com/dashboard/login';
  }

  loginData(isValid:boolean){
    this.loader = true;
    if(isValid){
      this.logoutReason = '';
      var loginObj={};
      loginObj['email']=this.email;
      loginObj['password']=this.password;
      var jsonString=JSON.stringify(loginObj);
      //login webservice
      this.http.post<any>(this.data.WEBSERVICE+'/user/LoginWithUsernamePassword',jsonString,{ headers: {'content-Type':'application/json'}})
      .subscribe(data=>{
        // error
        if(data.error.error_data=='1'){
          this.error = true;
          this.loader = false;
        }
        // valid
        if(data.error.error_data=='0'){
          this.error = false;
          // console.log(data);

          if(data.userResult.two_factor_auth==0){
          this.userId = data.userResult.user_id;
          localStorage.setItem('user_id',this.userId);
          this.http.post<any>(this.data.WEBSERVICE+'/oauth/token?grant_type=password&username='+this.userId+'&password='+this.password,'',    {headers: { 'authorization': 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg=='}})
          .subscribe(dataAuth=>{
            this.accessToken = dataAuth.access_token;
            this.refreshToken = dataAuth.refresh_token;
            this.expiresIn = dataAuth.expires_in;

            localStorage.setItem('access_token',this.accessToken);
            localStorage.setItem('refresh_token',this.refreshToken);

            var expiresTime=this.expiresIn;
            expiresTime=expiresTime*300000;
            var start_time=$.now();
            var expiresIn=start_time+expiresTime;
            localStorage.setItem('expires_in',expiresIn);

            var userObj={};
            userObj['user_id']=this.userId;
            var userJsonString=JSON.stringify(userObj);

            this.http.post<any>(this.data.WEBSERVICE+'/user/GetUserDetails',userJsonString,{headers: {'Content-Type': 'application/json','authorization': 'BEARER '+this.accessToken}})
            .subscribe(dataRecheck=>{
              if(dataRecheck.error.error_data=='0'){
                localStorage.setItem('user_name',dataRecheck.userResult.full_name);
                localStorage.setItem('user_id',dataRecheck.userResult.user_id);
                localStorage.setItem('phone',dataRecheck.userResult.phone);
                localStorage.setItem('email',dataRecheck.userResult.email);
                localStorage.setItem('address',dataRecheck.userResult.address);
                localStorage.setItem('profile_pic',dataRecheck.userResult.profile_pic);
                localStorage.setItem('check_id_verification_status','true');
                localStorage.setItem('selected_currency','btc');
                localStorage.setItem('buying_crypto_asset','btc');
                localStorage.setItem('selling_crypto_asset','usd');
                this.cookie.set('access_token',localStorage.getItem('access_token'),60);
                this.route.navigateByUrl('/dashboard');
                this.data.alert('Login Successful!','success');
              }
            })
          })
        }
      else{
        this.loader = false;
        this.otpBlock = true;
        $('.otp_segment').show();
        $('.otp_btn').show();
        $('.login_btn').hide();
        $('#loginInputOTP').focus();
      }
    }
      },error=>{
        this.loader = true;
      })
    }
  }

  loginThroughOtp(){
    var otpObj={};
    otpObj['email']=this.email;
    otpObj['otp']=this.otp;
    var jsonString=JSON.stringify(otpObj);
    // wip(1);
    this.http.post<any>(this.data.WEBSERVICE+'/user/CheckTwoFactor',jsonString,{headers: {
            'Content-Type': 'application/json'
          }})
    .subscribe(response=>{
          // wip(0);
          var result= response;
            if(result.error.error_data!='0'){
              this.data.alert(result.error.error_msg,'danger');
            }else{
              this.userId=result.userResult.user_id;
              localStorage.setItem('user_id',this.userId);
              // wip(1);
              this.http.post<any>(this.data.WEBSERVICE+'/oauth/token?grant_type=password&username='+this.userId+'&password='+this.password,'',{  headers: {'authorization': 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg=='}})
           .subscribe(response=>{
                // wip(0);
                  var result= response;

                    localStorage.setItem('access_token',result.access_token);
                    localStorage.setItem('refresh_token',result.refresh_token);

                    var expiresTime=result.expires_in;
                    expiresTime=expiresTime*1000;
                    var start_time=$.now();
                    var expiresIn=start_time+expiresTime;
                    localStorage.setItem('expires_in',expiresIn);

                    //get user details
                    var userObj={};
                    userObj['user_id']=localStorage.getItem('user_id');
                    var userJsonString=JSON.stringify(userObj);
                    // wip(1);
                    var accessToken=localStorage.getItem('access_token');
                    this.http.post<any>(this.data.WEBSERVICE+'/user/GetUserDetails',userJsonString,{headers: {
                      'Content-Type': 'application/json',
                      'authorization': 'BEARER '+accessToken,
                    }})
                  .subscribe(response=>{
                      // wip(0);
                      var result=response;
                      if(result.error.error_data!='0'){
                        this.data.alert(result.error.error_msg,'error');
                      }else{
                        localStorage.setItem('user_name',result.userResult.full_name);
                        localStorage.setItem('user_id',result.userResult.user_id);
                        localStorage.setItem('phone',result.userResult.phone);
                        localStorage.setItem('email',result.userResult.email);
                        localStorage.setItem('address',result.userResult.address);
                        localStorage.setItem('profile_pic',result.userResult.profile_pic);
                        localStorage.setItem('buying_crypto_asset','btc');
                        localStorage.setItem('selected_currency','btc');
                        localStorage.setItem('selling_crypto_asset','usd');
                        this.cookie.set('access_token',localStorage.getItem('access_token'),60);
                        this.route.navigateByUrl('/dashboard');
                        this.data.alert('Login Successful!','success');
                      }
                    },reason=>{
                      // wip(0);
                     this.data.alert(reason,'danger');
                    });

              },reason=>{
                // wip(0);
               this.data.alert(reason,'danger')
              });
            }
          },
          reason=>{
            // wip(0);
           this.data.alert(reason,'danger')
          });
  }

}
