import {
  Component,
  OnInit
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  CoreDataService
} from '../core-data.service';
import {
  BodyService
} from '../body.service';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-my-wallet',
  templateUrl: './my-wallet.component.html',
  styleUrls: ['./my-wallet.component.css']
})
export class MyWalletComponent implements OnInit {
  flag: boolean;
  transactionType: any = 3;
  historyDetails: any;
  totalCount: any;
  historyTableTr: string;
  selectedCurrency: string;
  selectedCurrencyText: string;
  rcvCode: any;
  cryptoCurrency: string;
  rcv: any;
  lockOutgoingTransactionStatus: any;
  paybito_phone: any;
  paybito_amount: any;
  paybito_otp: any;
  other_address: string;
  other_amount: any;
  other_otp: any;
  sendDisclaimer: any;
  sendDisclaimer2: any;
  rateList: any;
  trigx: any;
  accept: boolean;
  mining_fees: number = 0;
  balance: number = 0;
  availableBalance:any = 0;
  limit: any = 0;
  amount: any;
  currency: any;
  btcLink: any = 'https://live.blockcypher.com/btc/tx/';
  ltcLink: any = 'https://live.blockcypher.com/ltc/tx/';
  bchLink: any = 'https://explorer.bitcoin.com/bch/search/';
  ethLink: any = 'https://etherscan.io/tx/';
  etclink: any =  'http://gastracker.io/tx/';
  bsvlink: any =  'https://blockchair.com/search?q=';
  triggerslink: any = 'https://xchain.io/tx/';

  loading: boolean;
  disclaim:boolean=false;


  ///////////
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  currencyName:any ="marginholding";

  assetList:any;
  arraylist:any;
  price:any;
  send_amount: any;
  model:any;
  modelabc:any;
  modeldate:any;
  currencyId:any;
  lst:any;
  getInput:any;
  arrayy:any ="assetcurrency";
  fundingdata:any;
  getdata:any;
  

  year: number;
  month: number;
  day: number;
  daytoday:number;

  mm: any;
  CurrencyName:any ="hello";
  arrayname:any ="marginholding";
  marginWalletBalance:any=0;
  getmarginasset:any;
  assetfromdate:any;
  assettodate:any;
  fdata:any;
  keyvalue:any;
  selectyear:any;
  sumfdt:any;
  sumtdt:any;
  usedBalance:any=0;
  mwb:any;
  ub:any;
  ab:any;
  margetpricegin:any;
  currentDate:any=new Date().toISOString().slice(0,10);
  currentYear:any=new Date().getFullYear();
  currentMonth:any=(new Date().getMonth())+1;
  currentDay:any=new Date().getDate();
   user = localStorage.getItem('user_id');
  public fiatbalance;

  constructor(private http: HttpClient, private data: CoreDataService, public main: BodyService, private modalService: NgbModal, private route: Router) {}
  bccBalance: any;
  btcBalance: any;
  customerId: any;
  diamBalance: any;
  ethBalance: any;
  fiatBalance: any;
  hcxBalance: any;
  iecBalance: any;
  ltcBalance: any;
  public temp:any;
  marginWalletBalance1:any;
  usedBalance1:any;
  availableBalance1:any;
  getmargindata:any = 0;
  balance_list:any;
  ngOnInit() {
    this.main.getUserTransaction();
    this.walletHistoryList('1');
    this.main.getDashBoardInfo();
   this.getAssets();
    //this.balancecheck();
    this.sendMax();
//     this.balance_list=this.main.balance_list;
//  console.log('this.balance_list++++++++++',this.balance_list);

  }

  walletHistoryList(pageNo) {
    debugger;
    $('.walletHistoryTableBody').html(`<tr>
    <td colspan="5" class="text-center py-3">
    <img src="./assets/svg-loaders/puff.svg" width="50" alt="">
    </td>
  </tr>`);
    var historyObj = {};
    historyObj['customerID'] = localStorage.getItem('user_id');
    historyObj['page_no'] = pageNo;
    historyObj['no_of_items_per_page'] = this.main.noOfItemPerPage;
    historyObj['time_span'] = this.main.timeSpan;
    historyObj['transaction_type'] = this.transactionType;

    var jsonString = JSON.stringify(historyObj);
    // wip(1);
    this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/GetUserAllTransaction', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(response => {
        // wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          if (result.error.error_msg == 'no result') {
            $('.walletHistoryTableBody').html(`<tr>
            <td colspan="5" class="text-center py-3">
              No Data Available
            </td>
          </tr>`);
          } else {
            this.historyDetails = result.userTransactionsResult;
            this.totalCount = result.totalCount;
            this.historyTableTr = '';
            if (this.historyDetails != null) {
              for (var i = 0; i < this.historyDetails.length; i++) {
                //if(this.historyDetails[i].action=='Send' || this.historyDetails[i].action=='Recieved'){
                var timestamp = this.historyDetails[i].transaction_timestamp;
                var timestampArr = timestamp.split('.');
                timestamp = this.data.readable_timestamp(timestampArr[0]);
                var action = this.historyDetails[i].action;
                this.selectedCurrency = localStorage.getItem('selected_currency');
                this.selectedCurrencyText = this.selectedCurrency.toUpperCase();
                var hrefForTxn = '';
                var toolTipDesc = '';
                var amount = '';
                if (action == 'Send' || this.transactionType == 3) {
                  if (this.historyDetails[i]['debit_btc_amount'] != '0') {
                    var amount = '<span class="text-white">BTC ' + this.historyDetails[i]['debit_btc_amount'] + ' Dr.';
                    if (this.historyDetails[i].btc_txn_id != null && this.historyDetails[i].btc_txn_id != 'null') {
                      if ((this.historyDetails[i].btc_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.btcLink + this.historyDetails[i].btc_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['debit_bch_amount'] != '0') {
                    var amount = '<span class="text-white">BCH ' + this.historyDetails[i]['debit_bch_amount'] + ' Dr.';
                    if (this.historyDetails[i].bch_txn_id != null && this.historyDetails[i].bch_txn_id != 'null') {
                      if ((this.historyDetails[i].bch_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.bchLink + this.historyDetails[i].bch_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['debit_triggers_amount'] != '0') {
                    var amount = '<span class="text-white">TRIGGERS ' + this.historyDetails[i]['debit_triggers_amount'] + ' Dr.';
                    if (this.historyDetails[i].triggers_txn_id != null && this.historyDetails[i].triggers_txn_id != 'null') {
                      if ((this.historyDetails[i].triggers_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.triggerslink + this.historyDetails[i].triggers_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['debit_eth_amount'] != '0') {
                    var amount = '<span class="text-white">ETH ' + this.historyDetails[i]['debit_eth_amount'] + ' Dr.';
                    // console.log(i + ' ' + amount);
                    if (this.historyDetails[i].eth_txn_id != null && this.historyDetails[i].eth_txn_id != 'null') {
                      if ((this.historyDetails[i].eth_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.ethLink + this.historyDetails[i].eth_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['debit_ltc_amount'] != '0') {
                    var amount = '<span class="text-white">LTC ' + this.historyDetails[i]['debit_ltc_amount'] + ' Dr.';
                    if (this.historyDetails[i].ltc_txn_id != null && this.historyDetails[i].ltc_txn_id != 'null') {
                      if ((this.historyDetails[i].ltc_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.ltcLink + this.historyDetails[i].ltc_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['debit_hcx_amount'] != '0') {
                    var amount = '<span class="text-white">HCX ' + this.historyDetails[i]['debit_hcx_amount'] + ' Dr.';
                  }
                  if (this.historyDetails[i]['debit_iec_amount'] != '0') {
                    var amount = '<span class="text-white">IEC ' + this.historyDetails[i]['debit_iec_amount'] + ' Dr.';
                  }
                  //new etc
                  if (this.historyDetails[i]['debit_etc_amount'] != '0') {
                    var amount = '<span class="text-white">ETC ' + this.historyDetails[i]['debit_etc_amount'] + ' Dr.';
                    if (this.historyDetails[i].etc_txn_id != null && this.historyDetails[i].etc_txn_id != 'null') {
                      if ((this.historyDetails[i].etc_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.etclink + this.historyDetails[i].etc_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
               // bsv
                  if (this.historyDetails[i]['debit_bsv_amount'] != '0') {
                    var amount = '<span class="text-white">BSV ' + this.historyDetails[i]['debit_bsv_amount'] + ' Dr.';
                    if (this.historyDetails[i].bsv_txn_id != null && this.historyDetails[i].bsv_txn_id != 'null') {
                      if ((this.historyDetails[i].bsv_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.bsvlink + this.historyDetails[i].bsv_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  //diam
                  if (this.historyDetails[i]['debit_diam_amount'] != '0') {
                    var amount = '<span class="text-white">DIAM ' + this.historyDetails[i]['debit_diam_amount'] + ' Dr.';
                    if (this.historyDetails[i].diam_txn_id != null && this.historyDetails[i].diam_txn_id != 'null') {
                      if ((this.historyDetails[i].diam_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.bsvlink + this.historyDetails[i].diam_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  /* if (this.historyDetails[i]['debit_triggers_amount'] != '0') {
                    var amount = '<span class="text-white">TRIGGERS ' + this.historyDetails[i]['debit_triggers_amount'] + ' Dr.';
                  } */

                }
                if (action == 'Received' || this.transactionType == 4) {
                 // console.log(amount);
                  if (this.historyDetails[i]['credit_btc_amount'] != '0') {
                    var amount = '<span class="text-white">BTC ' + this.historyDetails[i]['credit_btc_amount'] + ' Cr.';
                   // console.log(i + ' ' + amount);
                    if (this.historyDetails[i].btc_txn_id != null && this.historyDetails[i].btc_txn_id != 'null') {
                      if ((this.historyDetails[i].btc_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.btcLink + this.historyDetails[i].btc_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['credit_bch_amount'] != '0') {

                    var amount = '<span class="text-white">BCH ' + this.historyDetails[i]['credit_bch_amount'] + ' Cr.';
                    if (this.historyDetails[i].bch_txn_id != null && this.historyDetails[i].bch_txn_id != 'null') {
                      if ((this.historyDetails[i].bch_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.bchLink + this.historyDetails[i].bch_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['credit_eth_amount'] != '0') {
                    var amount = '<span class="text-white">ETH ' + this.historyDetails[i]['credit_eth_amount'] + ' Cr.';
                    // console.log(i + ' ' + amount);
                    if (this.historyDetails[i].eth_txn_id != null && this.historyDetails[i].eth_txn_id != 'null') {
                      if ((this.historyDetails[i].eth_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.ethLink + this.historyDetails[i].eth_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['credit_ltc_amount'] != '0') {

                    var amount = '<span class="text-white">LTC ' + this.historyDetails[i]['credit_ltc_amount'] + ' Cr.';
                    if (this.historyDetails[i].ltc_txn_id != null && this.historyDetails[i].ltc_txn_id != 'null') {
                      if ((this.historyDetails[i].ltc_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.ltcLink + this.historyDetails[i].ltc_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  if (this.historyDetails[i]['credit_hcx_amount'] != '0') {

                    var amount = '<span class="text-white">HCX ' + this.historyDetails[i]['credit_hcx_amount'] + ' Cr.';
                  }
                  if (this.historyDetails[i]['credit_iec_amount'] != '0') {

                    var amount = '<span class="text-white">IEC ' + this.historyDetails[i]['credit_iec_amount'] + ' Cr.';
                  }
                  //new

                  if (this.historyDetails[i]['credit_etc_amount'] != '0') {

                    var amount = '<span class="text-white">ETC ' + this.historyDetails[i]['credit_etc_amount'] + ' Cr.';
                    if (this.historyDetails[i].etc_txn_id != null && this.historyDetails[i].etc_txn_id != 'null') {
                      if ((this.historyDetails[i].etc_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.etclink + this.historyDetails[i].etc_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }

                  //bsv
                   if (this.historyDetails[i]['credit_bsv_amount'] != '0') {

                    var amount = '<span class="text-white">BSV ' + this.historyDetails[i]['credit_bsv_amount'] + ' Cr.';
                    if (this.historyDetails[i].bsv_txn_id != null && this.historyDetails[i].bsv_txn_id != 'null') {
                      if ((this.historyDetails[i].bsv_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.bsvlink + this.historyDetails[i].bsv_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  //diam
                  if (this.historyDetails[i]['credit_diam_amount'] != '0') {

                    var amount = '<span class="text-white">DIAM ' + this.historyDetails[i]['credit_diam_amount'] + ' Cr.';
                    if (this.historyDetails[i].diam_txn_id != null && this.historyDetails[i].diam_txn_id != 'null') {
                      if ((this.historyDetails[i].diam_txn_id).length >= 10) {
                        hrefForTxn = '<br><a target="_blank" href="' + this.bsvlink + this.historyDetails[i].diam_txn_id + '">(Check Transaction Block)</a>';
                      }
                    }
                  }
                  //
                  if (this.historyDetails[i]['credit_triggers_amount'] != '0') {

                    var amount = '<span class="text-white">TRIGGERS ' + this.historyDetails[i]['credit_triggers_amount'] + ' Cr.';
                  }
                  if (this.historyDetails[i]['credit_trigx_amount'] != '0') {

                    var amount = '<span class="text-white">TRIGX ' + this.historyDetails[i]['credit_trig_amount'] + ' Cr.';
                  }
                  action = 'Receive';
                }

                /*if(action=='Load'){

                    var amount=' <span class="text-green">'+this.CURRENCYICON+' '+this.historyDetails[i].credit_fiat_amount+' Cr.</span>';
                }*/
                /*if(action=='Withdraw'){

                    var amount=' <span class="text-green">'+this.CURRENCYICON+' '+this.historyDetails[i].debit_fiat_amount+' Dr.</span>';
                }*/
                if (this.historyDetails[i].status == 'confirm' || this.historyDetails[i].status == 'Confirm') {
                  //  console.log(i+' '+this.historyDetails[i].status+' confirmend');
                  var status = 'Confirmed';
                  var statusClass = 'text-green';
                }
                if (this.historyDetails[i].status == 'pending' || this.historyDetails[i].status == 'Pending') {
                  //  console.log(i+' '+this.historyDetails[i].status+' pending');
                  var status = 'Pending';
                  var statusClass = 'text-orange';
                }
                if (this.historyDetails[i].status == 'cancel' || this.historyDetails[i].status == 'Cancel') {
                  // console.log(i+' '+this.historyDetails[i].status+' cancelled');
                  var status = 'Cancelled';
                  var statusClass = 'text-red';
                }

                toolTipDesc = '<div class="position-absolute tool_tip_div tool_tip_' + this.historyDetails[i].transaction_id + ' mt-4 bg-pbgreen text-white p-1 rounded " style="display:none;">' + this.historyDetails[i].description + '  <span data-txn-id="' + this.historyDetails[i].transaction_id + '" onClick="angular.element(this).scope().hideToolTipDesc()"><i class="fa fa-times"></i></span></div>';
                var description = (this.historyDetails[i].description != null) ? this.historyDetails[i].description : 'Received Amount';
                if (description.length < 25) {
                  var descriptionTd = '<td width="30%" data-txn-id="' + this.historyDetails[i].transaction_id + '" > ' + description + ' ' + hrefForTxn + '</td>';;
                } else {
                  var descriptionTd = '<td width="30%" data-txn-id="' + this.historyDetails[i].transaction_id + '" title="' + description + '">' + toolTipDesc + ' ' + (description).substr(0, 25) + '...  ' + hrefForTxn + '</td>';
                }
                this.historyTableTr += '<tr>';
                this.historyTableTr += '<td width="30%">' + timestamp + '</td>';
                this.historyTableTr += descriptionTd;
                this.historyTableTr += '<td class="text-white" width="10%">' + action + '</td>';
                this.historyTableTr += '<td width="20%">' + amount + '</td>';
                this.historyTableTr += '<td class="' + statusClass + '" width="10%">' + status + ' </td>';
                this.historyTableTr += '</tr>';
                //}
              }
              this.main.pagination(this.totalCount, this.main.noOfItemPerPage, 'walletHistoryList');
            } else {
              this.historyTableTr += '<tr colspan="5" class="text-center">No Data Exist</tr>';
            }
            $('.walletHistoryTableBody').html(this.historyTableTr);
            // console.log('historyTableTr++++++++++++',this.historyTableTr);
            
            this.main.getUserTransaction();

          }
        }
      }, function (reason) {
        // wip(0);
        if (reason.data.error == 'invalid_token') {
          this.data.logout();
        } else {
          this.data.logout();
          this.data.alert('Could Not Connect To Server', 'warning');
        }
      });
  }

  filterType(type) {
    if (type == 'send') {
      this.transactionType = '3';
      this.walletHistoryList('1');
      $('.mywallet_filter_btn').removeClass('btn_active');
      $('.send_filter_btn_wallet').addClass('btn_active');

    } else {
      this.transactionType = '4';
      this.walletHistoryList('1');
      $('.mywallet_filter_btn').removeClass('btn_active');
      $('.recieved_filter_btn_wallet').addClass('btn_active');
    }
  }

  showToolTipDesc(elem) {
    var txnId = elem.getAttribute('data-txn-id');
    $('.tool_tip_div').hide();
    $('.tool_tip_' + txnId).show();
  }

  hideToolTipDesc() {
    setTimeout(function () {
     // console.log('hide function');
      $('.tool_tip_div').hide();
    }, 100);
  }

  copy(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

  getCurrencyForRecieve(elem, currency) {

    this.cryptoCurrency = currency;
    $('.receive_address_label, .receive_address, .recieve_qr_code').hide();
    $('.generate_address_btn').hide();
    $('#qr_code').html('');
    if (this.main.userDocVerificationStatus() == true) {
      this.generateAddress();
      this.modalService.open(elem, {
        centered: true
      });
    }

  }

  generateAddress() {
    this.rcv = null;
    var rcvObj = {};
    rcvObj['customerID'] = localStorage.getItem('user_id');
    rcvObj['crypto_currency'] = this.cryptoCurrency;
    var jsonString = JSON.stringify(rcvObj);
    // wip(1);
    if (this.cryptoCurrency != 'triggers') {
      this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/ReceiveBTC', jsonString, {
          headers: {
            'Content-Type': 'application/json',
            'authorization': 'BEARER ' + localStorage.getItem('access_token'),
          }
        })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            this.rcv = result.customerkeysResult.fromadd;
            //console.log(this.rcv);

          }

        });
    } else {
      this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/GetCounterPartyNewAddress', jsonString, {
          headers: {
            'Content-Type': 'application/json',
            'authorization': 'BEARER ' + localStorage.getItem('access_token'),
          }
        })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            this.rcv = result.customerkeysResult.fromadd;
          }

        });

    }

  }

  getCurrencyForSend(md, elem, bal) {

    this.cryptoCurrency = elem;
    this.selectedCurrency = elem;
    this.balance = bal;
    this.mining_fees = 0;
    if (this.main.userDocVerificationStatus() == true) {
      this.modalService.open(md, {
        centered: true
      });
      this.paybito_phone = this.paybito_amount = this.other_address = this.other_amount = null;
      var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
      this.lockOutgoingTransactionStatus = userAppSettingsObj.lock_outgoing_transactions;
      if (this.lockOutgoingTransactionStatus == 1) {
        $('.sendOtpSection').show();
        // $('.get_Otp_btn').show();
        $('.send_btn').show();

      } else {
        $('.sendOtpSection').hide();
        // $('.get_Otp_btn').hide();
        $('.send_btn').show();
      }
      var settings = JSON.parse(localStorage.getItem('environment_settings_list'));
      this.sendDisclaimer = settings.send_other_min_value[`${this.cryptoCurrency}_description`];
      this.sendDisclaimer2 = settings.send_other_m_charges[`${this.cryptoCurrency}_description`];
    }
    if(this.cryptoCurrency =='triggers'){
     this.flag = true;
   }
   else{
    //alert('2');
    this.flag = false;
   }
  }

  rate(content) {
    this.modalService.open(content, {
      centered: true
    });
    this.rateList = null;
    var feeObj = {};
    feeObj['currency'] = this.cryptoCurrency;
    var jsonString = JSON.stringify(feeObj);
    this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/GetFees', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token')
        }
      })
      .subscribe(data => {
        this.rateList = data.feesListResult;
      })
  }
  Disclaimerdtl(trigger){

    this.modalService.open(trigger, {
      centered: true
    });
  }
  transactionSendWithinPaybito() {

    //send webservice
    var sendToPaybitoObj = {};
    //{"customerID":"38","toadd":"61","btcAmount":"0.75"}
    sendToPaybitoObj['customerID'] = localStorage.getItem('user_id');
    sendToPaybitoObj['toadd'] = this.paybito_phone;
    sendToPaybitoObj['btcAmount'] = this.paybito_amount;
    sendToPaybitoObj['crypto_currency'] = this.cryptoCurrency;
    if (this.lockOutgoingTransactionStatus == 1) {
      sendToPaybitoObj['otp'] = this.paybito_otp;
    }
    var jsonString = JSON.stringify(sendToPaybitoObj);
    // wip(1);
    this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/SendBTCtoPayBitoWallet', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(response => {
        // wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.data.alert('Sent succesfully', 'success');
          $('#sendModal').modal('hide');
          this.paybito_phone = '';
          this.paybito_amount = '';
          this.other_address = '';
          this.other_amount = '';
          this.route.navigateByUrl('/my-wallet');
        }

      }, reason => {
        // wip(0);
        this.data.logout();
        // $('#sendModal').modal('hide');
        this.data.alert('Could Not Connect To Server', 'danger');
      });

  }

  transactionSendForOthers() {
    //alert($scope.cryptoCurrency);

    var sendToPaybitoObj = {};
    //{"customerID":"38","toadd":"61","btcAmount":"0.75"}
    sendToPaybitoObj['customerID'] = localStorage.getItem('user_id');
    sendToPaybitoObj['toadd'] = this.other_address;
    sendToPaybitoObj['btcAmount'] = this.other_amount;
    sendToPaybitoObj['crypto_currency'] = this.cryptoCurrency;
    if (this.lockOutgoingTransactionStatus == 1) {
      sendToPaybitoObj['otp'] = this.other_otp;
    }
    var jsonString = JSON.stringify(sendToPaybitoObj);
    if (this.cryptoCurrency != 'triggers'){
    this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/SendBTCtoOherWallet', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(response => {
        //  wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.data.alert('Balance transfered successfully', 'success');
          //  $('#sendModal').modal('hide');
          this.paybito_phone = '';
          this.paybito_amount = '';
          this.other_address = '';
          this.other_amount = '';
        }

      }, reason => {
        //  wip(0);
        //  $('#sendModal').modal('hide');
        this.data.alert(reason, 'danger');
      });
    }else{// Do changes Here for Triggers
      this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/SendTriggers', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(response => {
        //  wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.data.alert('Balance transfered successfully', 'success');
          //  $('#sendModal').modal('hide');
          this.paybito_phone = '';
          this.paybito_amount = '';
          this.other_address = '';
          this.other_amount = '';
        }

      }, reason => {
        //  wip(0);
        //  $('#sendModal').modal('hide');
        this.data.alert(reason, 'danger');
      });
    }
  }
  myFunction(){

    var checked = document.forms["uc-disclaimer-form"]["disclaim"].checked;

    if (checked == true) {

     // this.disclaim = true;
   // this.cryptoCurrency='submit';
     document.getElementById('sclaimer').style.display='block';
    } else {

      document.getElementById('sclaimer').style.display='none';
      //this.disclaim = false;
    }
  }
  swapModal(content, bal) {
    this.modalService.open(content, {
      centered: true,
      size: 'lg'
    });
    this.trigx = bal;
  }

  swapCurrency() {
    var usr = localStorage.getItem('user_id');
    this.data.alert('Loading...', 'dark');
    var obj = {};
    obj["customerID"] = usr;
    obj["btcAmount"] = this.trigx;

    this.http.post < any > (this.data.WEBSERVICE + '/userTransaction/SwapCurrency', obj, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
      .subscribe(data => {
        this.data.loader = false;
        this.accept = false;
        if (data.error.error_data == 1) {
          this.data.alert(data.error.error_msg, 'warning');
        } else {
          this.data.alert('Token swapping was successful', 'success');
          this.main.getUserTransaction();
        }

      })
  }

  getRate(event, cur) {
    this.mining_fees = 0;
    if (typeof event == 'object') {
      var am = event.target.value;
    } else {
      var am = event;
    }
    this.http.get('./assets/appdata/rateRange.json')
      .subscribe(data => {
        var datax: any = data;

        function curx(arr) {
          return arr.currency == cur;
        }
        var solid = datax.filter(curx);

        function ratex(arr) {
          return arr.fromFee <= parseInt(am) && arr.toFee >= parseInt(am);
        }
        var solid2 = solid.find(ratex);
        // console.log(solid2, am, cur, solid);
        if (solid2) {
          this.mining_fees = solid2.feeRate;
          this.limit = this.balance - this.mining_fees;
          // console.log(this.mining_fees, this.limit);
        }
      })
  }

  sendMax() {
    this.other_amount = 0;
    this.getRate(this.balance, this.cryptoCurrency);
    this.loading = true;

    setTimeout(() => {
      if (this.balance > 0 && this.balance > this.limit || this.balance < this.mining_fees) {
        this.other_amount = this.balance - this.mining_fees;
      } else {
        this.other_amount = 0;
      }
      this.loading = false;
    }, 500);
  }

  ///Added
  modalOpen(content,bal,cur){
    this.amount="";
    this.currency="";
    this.modalService.open(content);
    this.amount=bal;
    this.currency=cur.toLocaleLowerCase();
   //this.balancecheck();
   }
   
modalUp(content){
  if(this.arrayy&&this.modelabc&&this.modeldate){

  let totalToDate = this.modeldate.month + this.modeldate.year;
    let totalfromDate = this.modelabc.month + this.modelabc.year;
    //console.log(totalfromDate);

    if((this.modeldate.year > this.modelabc.year) || (totalToDate > totalfromDate) ||
    (totalToDate == totalfromDate && this.modeldate.day > this.modelabc.day)) {
      this.modalService.open(content);
    }
    else {
      this.modalService.open('To Date Must be greater than from date!!');
    }
  }else{
      this.data.alert("Please Fill-up all the filds", 'danger');
  }
}

//original modal faithup
  modalUpFiat(content){
 if(this.assetfromdate&&this.assettodate){
    let totalToDate = this.assettodate.month + this.assettodate.year;
    let totalfromDate = this.assetfromdate.month + this.assetfromdate.year;
    //console.log(totalfromDate);

    if((this.assettodate.year > this.assetfromdate.year) || (totalToDate > totalfromDate) ||
    (totalToDate == totalfromDate && this.assettodate.day > this.assetfromdate.day)) {
      this.modalService.open(content);
    }

    else {
      this.modalService.open('To Date Must be greater than from date!!');
    }
  }else{
  //  alert("noooooooooooooooo");
    this.data.alert("Please Fill-up from date and to date both", 'danger');

  }
    }

  getPrice(){
    this.http.get<any>(this.data.MARGINURL+'funding/getMarketRateByCurrency?currencyName='+this.currency)
    .subscribe(response=>{
      this.price = response;
  // console.log("unitprice:"+this.price.price);

    })
  }

  getAssets(){
    this.http.get<any>(this.data.MARGINURL+'funding/fundingHomeView')
    .subscribe(response=>{

        
      // this.arraylist = response.splice(0,7); //changed by sanu
      this.arraylist = response.splice(0,8);

    // console.log("funding home view response: "+this.arraylist[0].CurrencyName);
    })
  }

  findingViewForInputServise(){

     var ds = this.modelabc;
     var dp = this.modeldate;

   var objdatafund = {
       "customerId":this.user,
       "currencyId":parseInt(this.arrayy),
        "fromDate":`${ds.day}-${ds.month}-${ds.year}`,
         "toDate":`${dp.day}-${dp.month}-${dp.year}`,

      }

      this.http.post < any > (this.data.MARGINURL + 'funding/fundingViewByInputValue',JSON.stringify(objdatafund), {
        headers: {
          'Content-Type': 'application/json',
        }
      })
      .subscribe(data => {
        //  wip(0);
        this.fundingdata = data;
        // console.log('response',data);
      })
    }
//left faith funding
assetfundingmargin(){

  //alert("helloooooooooooooo");
  this.assetfromdate = this.assetfromdate;
  this.assettodate = this.assettodate;

var objdatafund = {
    "customerId":this.user,
    "currencyId":1,
     "fromDate":`${this.assetfromdate.day}-${this.assetfromdate.month}-${this.assetfromdate.year}`,
      "toDate":`${this.assettodate.day}-${this.assettodate.month}-${this.assettodate.year}`,
   }

   this.http.post < any > (this.data.MARGINURL + 'funding/fundingViewByInputValue',JSON.stringify(objdatafund), {
     headers: {
       'Content-Type': 'application/json',
     }
   })
   .subscribe(data => {
     //  wip(0);
     this.fdata = data;
   })

 }
//comment
// getfindingViewForInputServise(){
//   this.http.get<any>(this.data.MARGINURL+'funding/fundingViewByInputValue')
//   .subscribe(response=>{
//    this.getInput = response;
//    console.log(this.getInput);
//   })
// }

// getoutputByInputValue(){
//   this.http.get<any>(this.data.MARGINURL+'funding/fundingViewByInputValue')
//   .subscribe(response=>{
//    this.getdata = response;
//    console.log(response);
//    console.log(this.getdata);

//   })
// }

//lending
lendingPrice(){
  var usr = localStorage.getItem('user_id');
  var objdata = {
    "customerId":this.user,
    "amount":this.send_amount,
    "currencyName":this.currency,
    "baseCurrencyName":"usd",
    "unitPrice":0
  }
  this.http.post < any > (this.data.LENDINGURL + 'transferBalanceToMWB',JSON.stringify(objdata), {
    headers: {
      'Content-Type': 'application/json',
    }
  })
  .subscribe(data => {
    //  wip(0);
    if(data.returnId != '1'){
      this.data.alert("Margin not sucessfull", 'danger');
    }else{
      this.data.alert("Margin sucessfull", 'danger');
    }
    this.main.getUserTransaction();
   // this.balancecheck();
  })
}
somethingChanged()
{

this.flag=true;
  this.http.get<any>(this.data.LENDINGURL+'getMarginWalletByCurrency?customerId='+this.user+'&currencyName=' + this.currencyName)
  .subscribe(responsemargin=>{

    // alert(this.getmargindata);
     if(responsemargin != null){
      this.data.alert("Balance is Avilable", 'danger');

      this.marginWalletBalance1 = responsemargin.marginWalletBalance;
      this.usedBalance1 = responsemargin.usedBalance;
      this.availableBalance1 = responsemargin.availableBalance;

     }else{
      this.marginWalletBalance1 = 0;
      this.usedBalance1 = 0;
      this.availableBalance1 = 0;
      this.data.alert("No Balance is Available Here", 'danger');
     }

    //  console.log(this.getmargindata);
     this.main.getUserTransaction();

  })

}
reset() {
  this.send_amount = this.send_amount = this.send_amount = '';
  this.model = this.model = this.model = '';
  this.modeldate = this.modeldate = this.modeldate = '';
  this.modelabc = this.modelabc = this.modelabc = '';

  this.assetfromdate = this.assetfromdate = this.assetfromdate = '';
  this.assettodate = this.assettodate = this.assettodate = '';
  this.marginWalletBalance1="";
  this.usedBalance1="";
  this.availableBalance1="";
  $(function () {
    $('input.form-control').val('');
  })
}

//faith**
faitmargin(){
  this.http.get<any>(this.data.LENDINGURL+'getMarginWalletByCurrency?customerId='+this.user+'&currencyName=USD')
  .subscribe(faithmargin=>{
  // this.getmarginfaith = faithmargin;
  // console.log(this.getmarginfaith);
  // alert(this.getmarginfaith);
 // })

//^^^^^^^^^^^^^
if(faithmargin != null){
 // this.data.alert("Balance is Avilable", 'danger');

  this.marginWalletBalance = faithmargin.marginWalletBalance;
  this.usedBalance = faithmargin.usedBalance;
  this.availableBalance = faithmargin.availableBalance;

 }else{
  this.marginWalletBalance = 0;
  this.usedBalance = 0;
  this.availableBalance = 0;
 // this.data.alert("No Balance is Available Here", 'danger');
 }

//  console.log(this.getmargindata);
 this.main.getUserTransaction();

})

}
// balancecheck(){
//   this.http.get<any>(this.data.LENDINGURL+'getMainWalletBalance?customerId='+this.user)
//   .subscribe(data=>{
//  if(data != null){
// this.fiatbalance=data.fiatBalance;
// console.log('balence____________',data);
// //this.main.fiatBalance=data.fiatbalance;
// // bccBalance: 49500
// // btcBalance: 44302.9371
// // customerId: 196
// // diamBalance: 8000
// // ethBalance: 5000
// // fiatBalance: 37199.949
// // hcxBalance: 5000
// // iecBalance: 6000
// // ltcBalance: 5000
//  this.bccBalance=data.bccBalance;
//  this.btcBalance=data.btcBalance;
//  this.hcxBalance=data.hcxBalance;
//  this.ethBalance=data.ethBalance;
//  this.ltcBalance=data.ltcBalance;
//  this.diamBalance=data.diamBalance;
//  this.ethBalance=data.ethBalance;
//  this.iecBalance=data.iecBalance;
//  this.ltcBalance=data.ltcBalance;
 
 

// // this.temp.diamBalance=data.diamBalance;
//  this.fiatBalance=data.fiatBalance;
// // this.temp.hcxBalance=data.hcxBalance;
// // this.temp.iecBalance=data.iecBalance;
// // this.temp.ltcBalance=data.ltcBalance;

//  }else{
  
//  }

// });
// }
assetlendingPrice(){
  var objdata = {
    "customerId":this.user,
    "amount":this.send_amount,
    "currencyName":this.currency,
    "baseCurrencyName":"usd",
    "unitPrice":"0"
  }
  this.http.post < any > (this.data.LENDINGURL + 'transferBalanceToMWB',JSON.stringify(objdata), {
    headers: {
      'Content-Type': 'application/json',
    }
  })
  .subscribe(data => {
    if(data.returnId != 1){
      this.data.alert("Margin is not sucessfully. Please try again", 'danger');
        }else{
      this.data.alert("Margin sucessfull", 'danger');
        }
        this.main.getUserTransaction();
     //   this.balancecheck();
    //  wip(0);
  })
}
assetfundBuy(){
    var dt = this.model;
    var objdata =null;
     objdata = {"customerId":this.user,"amount":this.send_amount,"currencyName":this.currency,
    "baseCurrencyName":"usd","unitPrice":"0", "lendingDate":null, "returnDate":dt.day+'-'+dt.month+'-'+dt.year
    };
   // console.log("date is: "+dt.day+dt.month);
    this.http.post < any > (this.data.MARGINURL + 'funding/transferFund',JSON.stringify(objdata), {
      headers: {
        'Content-Type': 'application/json',
      }
    })
    .subscribe(data => {
      if(data.returnId != 1){
        this.data.alert("Funding is not sucessfully. Please try again", 'danger');
          }else{
        this.data.alert("Funding sucessfull", 'danger');
          }
          this.main.getUserTransaction();
        //  this.balancecheck();
      //  wip(0);
    })
}

getmarginval(){
  debugger;
  var name = this.currency.toUpperCase();
  var url = this.data.BUYURL  +name+'USD'+ '/' + 'USD'+name +'/' + 1;
//console.log(url);

    this.http.get<any>(url)
    .subscribe(response=>{
      this.margetpricegin = response;
    })

 }

 fundBuy(){

  var dt = this.model;
  var objdata =null;
   objdata = {"customerId":this.user,"amount":this.send_amount,"currencyName":this.currency,
  "baseCurrencyName":"usd","unitPrice":this.margetpricegin.price, "lendingDate":null, "returnDate":dt.day+'-'+dt.month+'-'+dt.year
  };
 // console.log("date is: "+dt.day+dt.month);
  this.http.post < any > (this.data.MARGINURL + 'funding/transferFund',JSON.stringify(objdata), {
    headers: {
      'Content-Type': 'application/json',
    }
  })
  .subscribe(data => {
    if(data.returnId != 1){
      this.data.alert("Funding is not sucessfully. Please try again", 'danger');
        }else{
      this.data.alert("Funding sucessfull", 'danger');
        }
        this.main.getUserTransaction();
    //  wip(0);
  })
}
}
