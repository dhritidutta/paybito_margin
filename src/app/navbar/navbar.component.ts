import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { OrderBookComponent } from '../order-book/order-book.component';
import * as $ from 'jquery';
import { CoreDataService } from '../core-data.service';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userName:any;
  profilePic:any;
  closeResult: string;
  token:any = localStorage.getItem('access_token');
  mode:any
  modeMessage:any;
  alertType:any;
  alertMsg:any;
  currentRoute:any;
  reason:any;
  time:number = 10;
  isNavbarCollapsed = true;
  sideMenu:any = [
    { name:'Dashboard', alias:'dashboard'},
    { name:'Profile Details', alias:'profile-details'},
    { name:'Identity Verification', alias:'identity-verification'},
    { name:'Bank Details', alias:'bank-details'},
    { name:'Promotions', alias:'promotion'},
    { name:'Settings', alias:'settings'},
    { name:'Support', alias:'support'},
  ];
  upperMenu:any = [
    {name:'My Wallet',alias:'my-wallet'},
    {name:'Deposit Funds',alias:'deposit-funds'},
    {name:'Withdraw Funds',alias:'withdraw-funds'},
    {name:'History',alias:'history'},
    {name:'Report',alias:'report'},
    {name:'Portfolio Details',alias:'Portfolio-details'},
  ]
  interval;
  userid: string;
  act: string;
  imageLink: string;
  constructor(private modalService:NgbModal,private route:Router,private orderBook: OrderBookComponent, public data:CoreDataService, private http:HttpClient){
    this.route.events.subscribe(val =>{
      this.currentRoute = val;
    });
   }

  ngOnInit() {
    if(this.route.url!='/dashboard'&&this.orderBook.source12){
      this.orderBook.source12.close();
      this.orderBook.source2.close();
    }
    if(this.token==null){
      this.route.navigateByUrl('/login');
    }
    this.userName=localStorage.getItem('user_name');
    this.userid=localStorage.getItem('user_id');
    this.act=localStorage.getItem('access_token');
    this.profilePic=localStorage.getItem('profile_pic');
    this.imageLink = (this.profilePic)?this.data.WEBSERVICE+'/user/'+this.userid+'/file/'+this.profilePic+'?access_token='+this.act:'./assets/img/default.png'

    this.data.idleLogout();

  }

  open(content) {
    this.modalService.open(content);
  }

}
