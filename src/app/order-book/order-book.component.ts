import { Component, OnInit, DoCheck } from "@angular/core";

import { TradesComponent } from "../trades/trades.component";

import * as $ from "jquery";

import { CoreDataService } from "../core-data.service";

import { Router } from "@angular/router";

import { DashboardComponent } from "../dashboard/dashboard.component";

import { HttpClient } from "@angular/common/http";

import { tokenKey } from "@angular/core/src/view";

import { ChartComponent } from "../../app/chart/chart.component";

@Component({
  selector: "app-order-book",

  templateUrl: "./order-book.component.html",

  styleUrls: ["./order-book.component.css"]
})
export class OrderBookComponent implements OnInit {
  buyingAssetIssuer: string;

  sellingAssetIssuer: string;

  bidBody: string;

  askBody: string;

  orderBookUrl: string;

  count: any;

  lowprice: any;

  highprice;
  any = 0;

  chartlist: any = 0;

  ctpdata: any = 0;

  // session:any = localStorage.getItem('access_token');

  constructor(
    public data: CoreDataService,
    private route: Router,
    public dash: DashboardComponent,
    private http: HttpClient
  ) {
    switch (this.data.environment) {
      case 'live':
        this.orderBookUrl = 'https://billbitcoin.com/'; //live
        break;

      case 'dev':
        this.orderBookUrl = 'http://ec2-52-9-81-243.us-west-1.compute.amazonaws.com:8000/'; //dev
        break;

      case 'uat':
        this.orderBookUrl = 'http://ec2-52-8-41-112.us-west-1.compute.amazonaws.com:8000/'; //uat
        break;

      case 'demo':
        this.orderBookUrl = 'http://13.52.204.253:8000/'; //uat http://13.52.204.253:8000/
        break;
    }
      $("#orderbookBidBody").html(this.bidBody);
  }

  urlBid: any;

  urlAsk: any;

  selectedBuyingCryptoCurrencyName: string;

  selectedSellingCryptoCurrencyName: string;

  sellingAssetType: string;

  buyingAssetType: string;

  source11: any;

  source12: any;

  source2: any;

  token: any;

  marketTradeBodyHtml: any;

  // issuerLink: any =
  //   "./assets/appdata/assetIssuer." + this.data.environment + ".json";

  ngOnInit() {

    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName;

    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName;

    this.http
      .get<any>(
        this.data.CHARTSERVISE +
          "trendsTradeGraphFor24Hours/" +
          this.selectedBuyingCryptoCurrencyName +
          "/" +
          this.selectedSellingCryptoCurrencyName
      )

      .subscribe(value => {
        if (value != "") {
          this.chartlist = value[0];

          // console.log("*******************", this.chartlist);

          // if (this.chartlist.length != null) {

          //   if (this.chartlist.length < 5) {

          //     var bidLength: any = this.chartlist.length;

          //   } else {

          //     var bidLength: any = 5;

          //   }

          //   var askLength = 5;

          var randomNoForFlashArr = [];

          var arraylength = 2;

          this.ctpdata = this.chartlist.CTP;

          this.lowprice = this.chartlist.LOW_PRICE;

          this.highprice = this.chartlist.HIGH_PRICE;

          var randomNo = this.randomNoForOrderBook(0, arraylength);

          randomNoForFlashArr.push(randomNo);

          // console.log("DDDDDDDDDDDDD"+randomNo);

          for (var i = 0; i < arraylength; i++) {
            if (!$.inArray(i, randomNoForFlashArr)) {
              if (i == 0) {
                document.getElementById("pricered").style.display =
                  "inline-block";

                document.getElementById("pricegreen").style.display = "none";
              } else {
                document.getElementById("pricered").style.display = "none";

                document.getElementById("pricegreen").style.display =
                  "inline-block";
              }
            }
          }
        }
      });
  }
  changemode() {
    // console.log('++++++++++++++',this.data.changescreencolor);
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".sp-highlow").css("background-color", "#d3dddd");
      $(".sp-highlow").css("color", "Black");
      $(".border-col").css("border-color", "#d3dddd");
      $("th").css({ "background-color": "#d3dddd", color: "#273338" });
      $(".text-left").css("color", "black");
      $(".text-right").css("color", "black");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".sp-highlow").css("background-color", "#273338");
      $(".sp-highlow").css("color", "yellow");
      $(".border-col").css("border-color", "#273338");
      $("th").css({ "background-color": "#273338", color: "#d3dddd" });
      $(".text-left")
        .css("color", "")
        .css("color", "white");
      $(".text-right").css("color", "white");
    }
  }
  ngDoCheck() {
    this.changemode();
    var randomNoForFlashArr = [];

    var arraylength = 2;

    var valcurrency = this.data.ctpdata;

    if (valcurrency == undefined) {
      this.ctpdata = this.chartlist.CTP;

      this.lowprice = this.chartlist.LOW_PRICE;

      this.highprice = this.chartlist.HIGH_PRICE;
      // console.log("data++++++++1", this.ctpdata, this.lowprice, this.highprice);
    } else {
      this.ctpdata = this.data.ctpdata;

      this.lowprice = this.data.lowprice;

      this.highprice = this.data.highprice;
      //console.log('data2++++++++',    this.ctpdata,this.lowprice, this.highprice)
    }

    var randomNo = this.randomNoForOrderBook(0, arraylength);

    randomNoForFlashArr.push(randomNo);

    for (var i = 0; i < arraylength; i++) {
      if (!$.inArray(i, randomNoForFlashArr)) {
        if (i == 0) {
          document.getElementById("pricered").style.display = "inline-block";

          document.getElementById("pricegreen").style.display = "none";
        } else {
          document.getElementById("pricered").style.display = "none";

          document.getElementById("pricegreen").style.display = "inline-block";
        }
      }
    }
  }

  //random function for flash class in server sent orderbook

  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);

    var maxVal1: number = parseInt(maxVal);

    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }

  // serverSentEventForOrderbookBid() {
  //   this.sellingAssetType = "credit_alphanum12";

  //   this.buyingAssetType = "credit_alphanum12";

  //   var result: any;

  //   var buy = this.data.selectedBuyingCryptoCurrencyName;

  //   var sell = this.data.selectedSellingCryptoCurrencyName;

  //   this.token = localStorage.getItem("access_token"); //Newly added by Arijit for Eventstream Management

  //   // console.log(1,this.source12);

  //   // console.log(2,this.source2);

  //   this.http
  //     .get<any>(this.issuerLink)

  //     .subscribe(data => {
  //       var arr = data.obj;

  //       //  particular

  //       function issuer(arr) {
  //         return arr.buy == buy && arr.sell == sell;
  //       }

  //       var obj = arr.find(issuer);

  //       this.buyingAssetIssuer = obj.buyingIssuer;

  //       this.sellingAssetIssuer = obj.sellingIssuer;

  //       var url =
  //         this.orderBookUrl +
  //         "order_book?selling_asset_type=" +
  //         this.sellingAssetType +
  //         "&buying_asset_type=" +
  //         this.buyingAssetType +
  //         "&selling_asset_code=" +
  //         this.data.selectedBuyingCryptoCurrencyName.toUpperCase() +
  //         "" +
  //         this.data.selectedSellingCryptoCurrencyName.toUpperCase() +
  //         "&selling_asset_issuer=" +
  //         this.buyingAssetIssuer +
  //         "&buying_asset_code=" +
  //         this.data.selectedSellingCryptoCurrencyName.toUpperCase() +
  //         "" +
  //         this.data.selectedBuyingCryptoCurrencyName.toUpperCase() +
  //         "&buying_asset_issuer=" +
  //         this.sellingAssetIssuer +
  //         "&order=desc";

  //       if (this.token.length > 0) {
  //         //Newly added by Arijit for Eventstream Management

  //         this.source12 = new EventSource(url);

  //         this.source12.addEventListener("message", event => {
  //           result = JSON.parse(event.data);

  //           var bidHtml = "";

  //           var askHtml = "";

  //           if (result.bids.length != null) {
  //             if (result.bids.length < 50) {
  //               var bidLength: any = result.bids.length;
  //             } else {
  //               var bidLength: any = 50;
  //             }

  //             var randomNoForFlashArr = [];

  //             // var randomNo = this.randomNoForOrderBook(0, result.bids.length);

  //             var randomNo = 0;

  //             randomNoForFlashArr.push(randomNo);

  //             for (var i = 0; i < bidLength; i++) {
  //               //  console.log(result.asks[i].amount);

  //               var className = "";

  //               if (!$.inArray(i, randomNoForFlashArr)) {
  //                 className = "bg-flash-green";

  //                 //console.log(i+' in array');
  //               }

  //               if (
  //                 (result.bids[i].amount / result.bids[i].price).toFixed(4) !=
  //                 "0.0000"
  //               ) {
  //                 bidHtml += '<tr class="' + className + '">';

  //                 bidHtml +=
  //                   '<td class=" text-left">' +
  //                   (
  //                     parseFloat(result.bids[i].amount) / result.bids[i].price
  //                   ).toFixed(4) +
  //                   "</td>";

  //                 if (this.data.selectedSellingCryptoCurrencyName == "usd") {
  //                   bidHtml +=
  //                     '<td class="text-green right">' +
  //                     parseFloat(result.bids[i].price).toFixed(4) +
  //                     "</td>";
  //                 } else {
  //                   bidHtml +=
  //                     '<td class="text-green right">' +
  //                     parseFloat(result.bids[i].price).toFixed(8) +
  //                     "</td>";
  //                 }

  //                 bidHtml +=
  //                   '<td class=" text-right">' +
  //                   (
  //                     (result.bids[i].amount / result.bids[i].price) *
  //                     result.bids[i].price
  //                   ).toFixed(4) +
  //                   "</td>";

  //                 bidHtml += "</tr>";
  //               } else {
  //               }
  //             }
  //           } else {
  //             bidHtml += "<tr>";

  //             bidHtml += '<td class=" text-right" colspan="2">No Data</td>';

  //             bidHtml += "</tr>";
  //           }

  //           // if(this.route.url=='/dashboard'){

  //           this.bidBody = bidHtml;

  //           $("#orderbookBidBody").html(this.bidBody);

  //           // }
  //         });

  //         //console.log(token);
  //       } else {
  //         //Newly added by Arijit for Eventstream Management

  //         this.source2.close();
  //       }
  //     });
  // }

  // serverSentEventForOrderbookAsk() {
  //   this.sellingAssetType = "credit_alphanum12";

  //   this.buyingAssetType = "credit_alphanum12";

  //   var buy = this.data.selectedBuyingCryptoCurrencyName;

  //   var sell = this.data.selectedSellingCryptoCurrencyName;

  //   this.token = localStorage.getItem("access_token");

  //   this.http
  //     .get<any>(this.issuerLink)

  //     .subscribe(data => {
  //       var arr = data.obj;

  //       //  particular

  //       function issuer(arr) {
  //         return arr.buy == buy && arr.sell == sell;
  //       }

  //       var obj = arr.find(issuer);

  //       this.buyingAssetIssuer = obj.buyingIssuer;

  //       this.sellingAssetIssuer = obj.sellingIssuer;

  //       var url =
  //         this.orderBookUrl +
  //         "order_book?selling_asset_type=" +
  //         this.sellingAssetType +
  //         "&buying_asset_type=" +
  //         this.buyingAssetType +
  //         "&selling_asset_code=" +
  //         this.data.selectedBuyingCryptoCurrencyName.toUpperCase() +
  //         "" +
  //         this.data.selectedSellingCryptoCurrencyName.toUpperCase() +
  //         "&selling_asset_issuer=" +
  //         this.buyingAssetIssuer +
  //         "&buying_asset_code=" +
  //         this.data.selectedSellingCryptoCurrencyName.toUpperCase() +
  //         "" +
  //         this.data.selectedBuyingCryptoCurrencyName.toUpperCase() +
  //         "&buying_asset_issuer=" +
  //         this.sellingAssetIssuer +
  //         "&order=desc";

  //       if (this.token.length > 0) {
  //         //Newly added by Arijit for Eventstream Management

  //         this.source2 = new EventSource(url);

  //         this.urlAsk = url;

  //         var result: any = new Object();

  //         this.source2.onmessage = (event: MessageEvent) => {
  //           result = JSON.parse(event.data);

  //           // console.log(result);

  //           var bidHtml = "";

  //           var askHtml = "";

  //           if (result.asks.length != null) {
  //             //alert("mmmmmmmmmmmmmm"+result.asks.length);

  //             if (result.asks.length < 50) {
  //               var askLength = result.asks.length;
  //             } else {
  //               var askLength: any = 50;
  //             }

  //             var randomNoForFlashArr = [];

  //             var randomNo = this.randomNoForOrderBook(0, result.asks.length);

  //             //  console.log("mmmmmmmmmmmmmm"+randomNo);

  //             // alert("mmmmmmmmmmmmmm"+randomNo);

  //             randomNoForFlashArr.push(randomNo);

  //             for (var i = 0; i < askLength; i++) {
  //               // console.log(result.asks[i].amount);

  //               var className = "";

  //               if (!$.inArray(i, randomNoForFlashArr)) {
  //                 var className = "bg-flash-red";

  //                 //console.log(i+' in array');
  //               }

  //               if (parseFloat(result.asks[i].amount).toFixed(4) != "0.0000") {
  //                 askHtml += '<tr class="' + className + '">';

  //                 askHtml +=
  //                   '<td class=" text-left">' +
  //                   parseFloat(result.asks[i].amount).toFixed(4) +
  //                   "</td>";

  //                 if (this.data.selectedSellingCryptoCurrencyName == "usd") {
  //                   askHtml +=
  //                     '<td class="text-red right">' +
  //                     parseFloat(result.asks[i].price).toFixed(4) +
  //                     "</td>";
  //                 } else {
  //                   askHtml +=
  //                     '<td class="text-red right">' +
  //                     parseFloat(result.asks[i].price).toFixed(8) +
  //                     "</td>";
  //                 }

  //                 askHtml +=
  //                   '<td class=" text-right">' +
  //                   (result.asks[i].amount * result.asks[i].price).toFixed(4) +
  //                   "</td>";

  //                 askHtml += "</tr>";
  //               } else {
  //               }
  //             }
  //           } else {
  //             askHtml += "<tr>";

  //             askHtml += '<td class=" text-right" colspan="2">No Data</td>';

  //             askHtml += "</tr>";
  //           }

  //           // if (this.route.url == '/dashboard') {

  //           this.askBody = askHtml;

  //           $("#orderbookAskBody").html(this.askBody); //Newly added by Arijit for Eventstream Management
  //         };
  //       } else {
  //         //Newly added by Arijit for Eventstream Management

  //         this.source2.close();
  //       }
  //     });
  // }

  //trade page setup
  serverSentEventForOrderbookBid() {
    this.sellingAssetType = 'credit_alphanum12';
    this.buyingAssetType = 'credit_alphanum12';
    // var buyingissuer_code=localStorage.getItem('buyingissuer_code');
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    // var sellingissuer_code=localStorage.getItem('sellingissuer_code');
    // var sellingissuer=localStorage.getItem('sellingissuer');
    var result: any;
    this.token = localStorage.getItem('access_token'); //Newly added by Arijit for Eventstream Management
    var url = this.orderBookUrl + 'order_book?selling_asset_type=' + this.sellingAssetType + '&buying_asset_type=' + this.buyingAssetType + '&selling_asset_code=' + this.data.selectedSellingCryptoCurrencyName + '&selling_asset_issuer=' + this.sellingAssetIssuer + '&buying_asset_code=' + this.data.selectedBuyingCryptoCurrencyName + '&buying_asset_issuer=' + this.buyingAssetIssuer + '&order=desc';
    // console.log(url);
    if (this.token.length > 0) {//Newly added by Arijit for Eventstream Management
      this.source12 = new EventSource(url);
      this.source12.addEventListener('message', event => {
        result = JSON.parse(event.data);
        var bidHtml = '';
        var askHtml = '';
        if (result.bids.length != null) {
          if (result.bids.length > 5) { //changed by sanu
            var bidLength: any = result.bids.length;
          } else {
            // var bidLength: any = 5;
            var bidLength:any = result.bids.length; //changed by sanu
          }
          var randomNoForFlashArr = [];
          var randomNo = 0;
          randomNoForFlashArr.push(randomNo);
          for (var i = 0; i < bidLength; i++) {
            var className = "";
            if (!$.inArray(i, randomNoForFlashArr)) {
              className = 'bg-flash-green';
            }
            if (((result.bids[i].amount) / (result.bids[i].price)).toFixed(4) != '0') {

              bidHtml += '<tr class="' + className + '">';

              bidHtml += '<td class="text-left">' + (parseFloat(result.bids[i].amount) / (result.bids[i].price)).toFixed(4) + '</td>';

              if (this.data.selectedSellingCryptoCurrencyName == 'usd') {

                bidHtml += '<td class="text-green text-right">' + (parseFloat(result.bids[i].price)).toFixed(4) + '</td>';

              } else {

                bidHtml += '<td class="text-green text-right">' + (parseFloat(result.bids[i].price)).toFixed(8) + '</td>';

              }

              bidHtml += '<td class=" text-right">' + ((result.bids[i].amount) / (result.bids[i].price) * (result.bids[i].price)).toFixed(4) + '</td>';

              bidHtml += '</tr>';

            }
            else { }
          }
        } else {
          bidHtml += '<tr>';
          bidHtml += '<td class="" colspan="2">No Data</td>';
          bidHtml += '</tr>';
        }
        // if(this.route.url=='/dashboard'){
        this.bidBody = bidHtml;
        $("#orderbookBidBody").html(this.bidBody);
        // }
      });
      //console.log(token);
    } else {
      //Newly added by Arijit for Eventstream Management
      this.source2.close();
    }
    // });

  }
  serverSentEventForOrderbookAsk() {
    this.sellingAssetType = 'credit_alphanum12';
    this.buyingAssetType = 'credit_alphanum12';
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    var url = this.orderBookUrl + 'order_book?selling_asset_type=' + this.sellingAssetType + '&buying_asset_type=' + this.buyingAssetType + '&selling_asset_code=' + this.data.selectedSellingCryptoCurrencyName + '&selling_asset_issuer=' + this.sellingAssetIssuer + '&buying_asset_code=' + this.data.selectedBuyingCryptoCurrencyName + '&buying_asset_issuer=' + this.buyingAssetIssuer + '&order=desc';
    if (this.token.length > 0) {
      this.source2 = new EventSource(url);
      this.urlAsk = url;
      var result: any = new Object;
      this.source2.onmessage = (event: MessageEvent) => {
        result = JSON.parse(event.data);
        var bidHtml = '';
        var askHtml = '';
        if (result.asks.length != null) {
          if (result.asks.length > 5) { //changed bu sanu
            var askLength = result.asks.length;
          } else {
            // var askLength: any = 5;
            var askLength = result.asks.length; //changed by sanu
          }
          var randomNoForFlashArr = [];
          // for(r=0;r<3;r++){
          var randomNo = this.randomNoForOrderBook(0, result.asks.length);
          randomNoForFlashArr.push(randomNo);
          //}
          //console.log(randomNoForFlashArr);

          for (var i = askLength-1; i>=0; i--) { //changed by sanu
            // console.log(result.asks[i].amount);
            var className = '';
            if (!$.inArray(i, randomNoForFlashArr)) {
              var className = 'bg-flash-red';
              //console.log(i+' in array');
            }
            if (((parseFloat(result.asks[i].amount))).toFixed(4) != '0.0000') {

              askHtml += '<tr class="' + className + '">';

              askHtml += '<td class="text-left">' + ((parseFloat(result.asks[i].amount))).toFixed(4) + '</td>';

              if (this.data.selectedSellingCryptoCurrencyName == 'usd') {

                askHtml += '<td class="text-red text-right">' + (parseFloat(result.asks[i].price)).toFixed(4) + '</td>';

              } else {

                askHtml += '<td class="text-red text-right">' + (parseFloat(result.asks[i].price)).toFixed(8) + '</td>';

              }

              askHtml += '<td class="text-right">' + ((result.asks[i].amount) * (result.asks[i].price)).toFixed(4) + '</td>';

              askHtml += '</tr>';

            }
            else { }
          }
        } else {
          askHtml += '<tr>';
          askHtml += '<td class="" colspan="2">No Data</td>';
          askHtml += '</tr>';
        }
        this.askBody = askHtml;
        $("#orderbookAskBody").html(this.askBody);
      }
    }
    else {
      this.source2.close();
      // console.log('Session Expired');
    }
  }
  tradePageSetup() {
    if (
      this.data.selectedBuyingCryptoCurrencyName &&
      this.data.selectedBuyingCryptoCurrencyName
    ) {
      this.serverSentEventForOrderbookBid();

      this.serverSentEventForOrderbookAsk();
    } else {
      // this.source12.close();
      // this.source2.close();
    }
  }
}
