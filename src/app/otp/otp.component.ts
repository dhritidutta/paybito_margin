import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {
  otp: any;

  constructor(
    private http:HttpClient,
    public data:CoreDataService,
    private route:Router
  ) { }

  ngOnInit() {
  }

  onKeyupSendOtp(){
    if((this.otp).length==5){
      this.otpData();
    }
  }

  otpData(){
    if(this.otp!=undefined){
      var userId=localStorage.getItem('signup_user_id');
      var userId=userId;
      var otp=this.otp;
      var otpObj={};
      otpObj['user_id']=userId;
      otpObj['otp']=otp;
      var jsonString=JSON.stringify(otpObj);
      this.http.post<any>(this.data.WEBSERVICE+'/user/CheckOTP',jsonString,{headers: {'Content-Type': 'application/json'}})
    .subscribe(response=>{
      var result=response;
      if(result.error.error_data=='2'){
        this.route.navigateByUrl('/login');
        this.data.alert(result.error.error_msg,'info');
      }
      if(result.error.error_data=='1'){
        this.data.alert(result.error.error_msg,'danger');
      }

      if(result.error.error_data=='0'){
        var userId=result.userResult.user_id;
        localStorage.setItem('signup_user_id',userId);
        this.data.alert('OTP Verified','success');
        this.route.navigateByUrl('/login');
      }
    },reason=>{
      this.data.alert(reason,'danger')

    });

    }else{
      this.data.alert('Please fill up all the fields properly','warning');
    }
  }

  resendOtp(){
    var otpObj={};
    otpObj['user_id']=localStorage.getItem('signup_user_id');
    var jsonString=JSON.stringify(otpObj);
    this.http.post<any>(this.data.WEBSERVICE+'/user/ResendOTP',jsonString,{   headers: {
          'Content-Type': 'application/json'
        }})
.subscribe(response=>{
          var result= response;
          if(result.error.error_data!='0'){
             this.data.alert(result.error.error_msg,'danger');
          }else{
            this.data.alert('OTP has been sent to your email','success');
          }
        },
        reason=>{
          this.data.alert('Could Not Connect To Server','warning')
        });
}

}
