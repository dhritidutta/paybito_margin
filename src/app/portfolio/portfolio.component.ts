import { Component, OnInit } from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  CoreDataService
} from '../core-data.service';
import {
  BodyService
} from '../body.service';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
// import { MarginService } from '../margin.service';
// import { FundingService } from '../funding.service';
import { StopLossComponent } from '../../app/stop-loss/stop-loss.component';
import { BrowserPlatformLocation } from '@angular/platform-browser/src/browser/location/browser_platform_location';
@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  p: number = 1;
  user = localStorage.getItem('user_id');
  header: any;
  headervalue = [];
  transactionheader: any;
  transactionvalue: any;
  holdingvalue = [];
  public flagSell:boolean;
  public flagBuy:boolean;
  constructor(
    private http: HttpClient,
    private data: CoreDataService,
    public main: BodyService,
    private modalService: NgbModal,
    private route: Router,
    private _StopLossComponent :StopLossComponent
    
  ) { }

  ngOnInit() {
   this.getPortfolioDetails();
    // this.transactionHistoryDetails();
  }

  //pagination

  //end pagenation

  //two api call
  getPortfolioDetails() {
    debugger;
    this.http.get<any>(this.data.LENDINGURL + 'portfolioDetails?customerId=' + this.user)
      .subscribe(response => {

        this.header = response.header;
        this.header[0] = "";
        this.header[6] = "";

        //console.log('+++++++++++++++++++++', this.header);

       this.headervalue = response.values;
      // console.log('+++++++++++++++++++++', this.headervalue);
       // var i;
       // for (i = 0; i < this.headervalue.length; i++) {
      
          // if (test == 'SHORT-SELL') {
          //   this.flagBuy=null;
          //   this.flagSell=null;
          //   this.flagBuy=false;
          //   this.flagSell=true;
          // }
          // if (test == 'LONG-BUY') {
          //   this.flagBuy=null;
          //   this.flagSell=null;
          //   this.flagBuy=true;
          //   this.flagSell=false;
            
          // }

       // }


      })
  }

  transactionHistoryDetails() {
    //  alert(this.user);
    this.http.get<any>(this.data.LENDINGURL + 'marginFundingTransactionHistory?customerId=' + this.user)
      .subscribe(response => {

        this.transactionheader = response.header;
      //  console.log(this.transactionheader);

        this.transactionvalue = response.values;
       // console.log(this.transactionvalue);

      })
  }
  PmarginBuy(sellingassetcode,buyassetcode,amount,price,ID) {
  this.data.alert('loading....','dark');
    var usr = localStorage.getItem('user_id');
    var objdata = {
      "customerId": usr,
      "buying_asset_code": sellingassetcode,
      "selling_asset_code": buyassetcode,
      // "buying_asset_code": buyassetcode,
      // "selling_asset_code": sellingassetcode,
      "quantity": amount,
      "price": price,
      "txnType": 1,
      "offerType": 'p',
      "portfolioId":ID

    }
    this._StopLossComponent.marginBuy(objdata);
    this.data.loader=false;
    this.getPortfolioDetails();
    //$('.tradeButton').click();
  }
  PmarginSell(sellingassetcode,buyassetcode,amount,price,ID){
    this.data.alert('loading....','dark');
    var usr = localStorage.getItem('user_id');
    var objdata = {
      "customerId": usr,
      // "buying_asset_code": sellingassetcode,
      // "selling_asset_code":buyassetcode,
      "buying_asset_code": buyassetcode,
      "selling_asset_code": sellingassetcode,
      "quantity": amount,
      "price": price,
      "txnType": 2,
      "offerType": 'P',
      "portfolioId":ID

    }
    this._StopLossComponent.marginSell(objdata);
    this.data.loader=false;
    this.getPortfolioDetails();
    //$('.tradeButton').click();
  }
  // callvalidateLimit() {
  //   this.stoploss.validateLimit();
  // }
}
