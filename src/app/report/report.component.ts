import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { BodyService } from '../body.service';
import { CoreDataService } from '../core-data.service';
import {
  HttpClient
} from '@angular/common/http';
import { d } from '@angular/core/src/render3';
import { first } from 'rxjs/operators';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  fiancialyear:any;
  reportlist:any;
  fromdate:any;
  usr:any;
  model:any;
  startdata:any;
  sldata:any;
  transtiondata:any;
  price:any;
  keys:any;
  sumfdt:any;
  sumtdt:any;
  keyval:any;
  results:any;
  headerdata:any;
  fromDate:any;
  reportid:any;
  datastore:any;
  reportarray:any="selectreport";
  selectyear:any="fyear"
  reportkeys:any;
  todate:any;
  toDate:any;
  fundingheader:any;
  year: number;
  month: number;
  newheader:any;
  datavalue:any;
  selectpdf:any="selectoption";
  downloadUrl:any='javascript:void(0)';
  // day: number;
  ID:any;
  item:any;
  public show:any;
  repu:any;
  public buttonName:any = 'Show';
 

  /*****date picker max and min date*******/
  currentDate:any=new Date().toISOString().slice(0,10);
  currentYear:any=new Date().getFullYear();
  currentMonth:any=(new Date().getMonth())+1;
  currentDay:any=new Date().getDate();

  /*********for showing export csv file button*******/
  showExportButton=0;

  /********pagination related variables**********/
  totalCount=0;
  pageNo=1;
  noOfItemsPerPage=10;
  showingRowValue=10;
  showPrevButton=0;
  showNextButton=0;
  pagiArr=[];
  pagiLength:any;

  constructor(
    private http: HttpClient,
    private data: CoreDataService,
    ) { }

  ngOnInit() {
    this.getreportdata();
    this.financialyear();
  }

  reset() {
    this.fromdate = this.fromdate = this.fromdate = '';

    this.reportarray = this.selectyear = '';

    this.todate = this.todate = this.todate = '';
    this.model = this.model = this.model = '';
    $(function () {
      $('input.form-control').val('');
    })
  }

  fundBuy(content){
    if(this.reportarray&&this.fromdate&&this.todate){
     open(content);
    }else{
      alert("Please Fillup all the given field")
    }
    }

  getreportdata(){
    this.http.get<any>(this.data.REPORTSERVISE+'viewAllMaster')
    .subscribe(response=>{
     this.reportkeys = Object.keys(response);
     //this.reportlist = Object.values(response);
     this.reportlist = (response);

   //  console.log("funding home view response: "+this.reportlist);
    })
  }

finddata(datastore,reportarray){
//if statement date formal

    var usr = localStorage.getItem('user_id');
    //alert(this.reportarray.values);
    this.repu = reportarray;

    var ds = this.fromdate;
    var dp = this.todate;

    if(ds==undefined && dp==undefined){

      this.sumfdt = '1' + '1' + this.selectyear;
      this.sumtdt = '31' + '12' + this.selectyear+1;
    }else{
      this.fromDate = ds.day + '-' + ds.month + '-' + ds.year;
      this.toDate = dp.day + '-' + dp.month  + '-' + dp.year;

      this.sumfdt = ds.month + ds.year;
     this.sumtdt = dp.month + dp.year;

    }
    if(this.repu==6){
      this.sumfdt='0';
      this.sumtdt='1';
    }
   // alert(this.sumtdt+'  '+this.sumfdt)
    if((this.sumtdt > this.sumfdt) || (this.sumfdt == this.sumtdt && dp.day>ds.day) || (dp.year > ds.year )){

      if(this.repu!= 6){
        this.http.get<any>(this.data.REPORTSERVISE+'viewRepotDetails/'+ usr + '/' + reportarray + '/' + this.fromDate+'/'+ this.toDate + '/'+2018+'/'+this.pageNo)
        .subscribe(response=>{
        //Table Header
        this.headerdata = Object.keys(response);

        this.keyval = Object.values(response[0]);
        this.datastore = this.keyval[0];

        this.fundingheader = this.keyval[1];
        //this.results = Object.keys(this.fundingheader[1]);

        this.results = Object.keys(this.fundingheader);
        this.showExportButton=1;
        this.totalCount=response[0]['Page count'][0];
        
          this.pagiArr=[];
          var limit=this.totalCount;
          
          for(var i=1;i<=limit;i++){
              if(i<=7){
                this.pagiArr.push(i);
              }
              
          }  
          //console.log(this.pagiArr);
          this.pagiLength=(this.pagiArr).length;
      });
    }
    else{
      this.fromDate = '0';
      this.toDate = '0';

        this.http.get<any>(this.data.REPORTSERVISE+'viewRepotDetails/'+ usr + '/' + reportarray + '/' + this.fromDate+'/'+ this.toDate + '/'+ this.selectyear+'/'+this.pageNo)
      .subscribe(response=>{
      //Table Header
      this.headerdata = Object.keys(response);
    //alert(this.headerdata);

      this.keyval = Object.values(response[0]);
      this.datastore = this.keyval[0];
    //console.log(datastore);

      this.fundingheader = this.keyval[1];
      this.results = Object.keys(this.fundingheader);
    this.showExportButton=1;
        this.totalCount=response[0]['Page count'][0];
        
          this.pagiArr=[];
          var limit=this.totalCount;
          
          for(var i=1;i<=limit;i++){
              if(i<=7){
                this.pagiArr.push(i);
              }
              
          }  
          //console.log(this.pagiArr);
          this.pagiLength=(this.pagiArr).length;
      });
    }
    if(this.repu!=6){
      this.downloadUrl=this.data.REPORTSERVISE+'viewRepotDetails/'+localStorage.getItem('user_id')+'/'+this.repu+'/'+this.fromDate+'/'+this.toDate+'/2018';
    }else{
      this.downloadUrl=this.data.REPORTSERVISE+'viewRepotDetails/'+localStorage.getItem('user_id')+'/'+this.repu+'/0/0/'+this.selectyear;
    }
    
  }
  else{
    alert("From Date is greater than To date");
    this.sumtdt='';
    this.sumfdt='';
  }

}

  OnChange(val) {
    // CHANGE THE NAME OF THE BUTTON.
  // console.log(val);
   if(val==6)
   this.show = 1;
    else{
      this.show=0;
    }
  }

  /********function defination for explode csv*******/
   exportTableToCSV(table, filename) {
    var $headers = $(table).find('tr:has(th)')
        ,$rows = $(table).find('tr:has(td)')
        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        ,tmpColDelim = String.fromCharCode(11) // vertical tab character
        ,tmpRowDelim = String.fromCharCode(0) // null character
        // actual delimiter characters for CSV format
        ,colDelim = '","'
        ,rowDelim = '"\r\n"';
        // Grab text from table into CSV formatted string
        var csv = '"';
        csv += formatRows($headers.map(grabRow));
        csv += rowDelim;
        csv += formatRows($rows.map(grabRow)) + '"';
        // Data URI
        var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
    $('#downloadCSVBtn')
        .attr({
        'download': filename
            ,'href': csvData
            //,'target' : '_blank' //if you want it to open in a new window
    });
    //------------------------------------------------------------
    // Helper Functions
    //------------------------------------------------------------
    // Format the output so it has the appropriate delimiters
    function formatRows(rows){
        return rows.get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim);
    }
    // Grab and format a row from the table
    function grabRow(i,row){

        var $row = $(row);
        //for some reason $cols = $row.find('td') || $row.find('th') won't work...
        var $cols = $row.find('td');
        if(!$cols.length) $cols = $row.find('th');
        return $cols.map(grabCol)
                    .get().join(tmpColDelim);
    }
    // Grab and format a column from the table
    function grabCol(j,col){
        var $col = $(col),
            $text = $col.text();
        return $text.replace('"', '""'); // escape double quotes
    }
}

/***********previous function defination for pagination ********/
showPreviousList(){
  this.pageNo=this.pageNo-1;
  this.finddata(this.datastore,this.reportarray);

}

/***********next function defination for pagination ********/
showNextList(){
  this.pageNo=this.pageNo+1;
  this.finddata(this.datastore,this.reportarray);
}
financialyear(){
  this.http.get<any>(this.data.REPORTSERVISE+'financialYearList')
  .subscribe(response=>{
  this.fiancialyear = response;
 // alert(this.fiancialyear);
  })
}
/****************get pagination number*******/
  pagi(num){
    this.pageNo=parseInt(num);
    this.finddata(this.datastore,this.reportarray);
  }

  /**************down load csv from backend***********/
  downloadCsvFromBackend(){
    //alert(this.repu);
    if(this.repu!=6){
      var downloadUrl=this.data.REPORTSERVISE+'viewRepotDetails/'+localStorage.getItem('user_id')+'/'+this.repu+'/'+this.fromDate+'/'+this.toDate+'/2018';
    }else{
      var downloadUrl=this.data.REPORTSERVISE+'viewRepotDetails/'+localStorage.getItem('user_id')+'/'+this.repu+'/0/0/'+this.selectyear;
    }
    this.http.get<any>(downloadUrl)
    .subscribe(response=>{
    
    })

  }
}


