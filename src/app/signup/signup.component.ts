import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from "jquery";
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  contries: any;
  captcha: string;
  loadmail: any;
  dataemail: any;
  dateofbirth;
  model;
  file1:any;
  file2:any;
 // @Output() messageEvent = new EventEmitter<string>();
  search: (text$: Observable<string>) => Observable<any>;


  title = 'identity';
  countries;
  man;
  reg: any = {};
  reg2: any = {};
  errormessage;

  constructor(private http: HttpClient, public data: CoreDataService, private route: Router, private activeRoute: ActivatedRoute) { }

  signupObj: any = {
    country: ''
  };
  signupObj1: any = {};
  ngOnInit() {
    /* $(function(){
      $('a#tab1').click(function(){
        location.href = 'https://www.paybito.com/dashboard/signup';
      });
    }); */

    this.getLoc();
    this.checkurlemail(this.loadmail);
  }

  checkurlemail(loadmail) {
    this.activeRoute.queryParams.subscribe(params => {
      //console.log(params);
      this.loadmail = params['email'];
    })
    if (this.loadmail != undefined && this.loadmail != '') {
      this.signupObj.email = this.loadmail;
      //alert(this.signupObj.email);
    } else {
      this.signupObj.email = ''
    }
    //alert(this.loadmail);
  }

  getLoc() {
    this.http.get<any>('./assets/data/country.json')
      .subscribe(data => {
        this.contries = data;
      })
  }

  resolved(captchaResponse: string) {
    this.captcha = captchaResponse;
  }
  IDMSERVICE: any = "https://sandbox.identitymind.com/im/account/consumer?graphScoreResponse=false";
  WEBSERVICE: any = "https://api.digitalterminal.net/webservice";
  merchantid: any = 'cornerstore';
  merchantpassword: any = '8215631cc6e8b0ef43a2aba1aa61a489ebe547f2';
  accessKey = btoa(("this.merchantid:this.merchantpassword"));
  // sendIdentity(isValid) {
  //   debugger;
  //   var fd = new FormData();
  //   if (isValid) {
  //       fd.append('g_recaptcha_response', this.captcha);
  //       // fd.append('g_recaptcha_response', "help");
  //       fd.append('bfn', this.signupObj.fast_name);
  //       fd.append('bmn', this.signupObj.middle_name);
  //       fd.append('bln', this.signupObj.last_name);
  //       fd.append('bgd', this.signupObj.gender);
  //       fd.append('bsn', this.signupObj.address);
  //       fd.append('bc', this.signupObj.city);
  //       fd.append('bco', this.signupObj.country);
  //       fd.append('bz', this.signupObj.zip);
  //       fd.append('tea', this.signupObj.email);
  //       fd.append('phn', this.signupObj.phone);
  //       fd.append('bs', this.signupObj.state);
  //       fd.append('docType',this.signupObj.signupInputdoctype);
  //       fd.append('soc', 'Google');
  //       var date = Date.now();
  //       fd.append('accountCreationTime', date.toString());
  //       fd.append('man', this.signupObj.fast_name + ' ' + this.signupObj.middle_name + ' ' + this.signupObj.last_name);
  //       fd.append('password', this.signupObj.password);
  //       var ds = this.signupObj.dateofbirth;
  //       var Dateofbirth =ds.year.toString()+ds.month.toString()+ds.day.toString();
  //       fd.append('dob', Dateofbirth);
  //      if ($('.document_front_side')[0].files[0] != undefined) {
  //       // this.signupObj1['scanData']=$('.document_front_side')[0].files[0];
  //       fd.append('scanData', $('.document_front_side')[0].files[0]);
  //       this.file1=($('.document_front_side')[0].files[0]);
  //         } else {
  //       //this.signupObj1['scanData'] = '';
  //       fd.append('scanData', '');
  //     }
  //     if ($('.document_back_side')[0].files[0] != undefined) {
        
  //       fd.append('backsideImageData', $('.document_back_side')[0].files[0]);
  //        } else {
  //      fd.append('backsideImageData', '')

  //     }
     
  //     // this.http.post < any > (this.IDMSERVICE, iamString, {jsonString,{headers: {'Content-Type': 'application/json'}})
  //     if (this.captcha != '') {
  //     this.http.post<any>(this.WEBSERVICE + '/user/AddIDMUserDetails', fd, { headers: { } })
  //         .subscribe(response => {
  //           // wip(0);
  //           var result = response;
  //           if (result.error.error_data != '0') {
  //             this.data.alert(result.error.error_msg, 'danger');
  //           } else {
  //             var userId = result.userResult.user_id;
  //             localStorage.setItem('signup_user_id', userId);
  //             this.data.alert('Registration Done,  Kindly check your email for verification token', 'success');
  //             this.route.navigateByUrl('/otp');
  //           }
  //         }, reason => {
  //           // wip(0);
  //           this.data.alert('Internal Server Error', 'danger')

  //         });
  //     }
  //     else {
  //       this.data.alert('Captcha Unverified', 'warning');
  //     }

  //   }
  //   else {
  //     this.data.alert('Please fill up all the fields properly', 'warning');
  //   }
  // }
  signupData(isValid){
    if(isValid){
      // console.log(this.signupObj);
      this.signupObj['g_recaptcha_response']=this.captcha;
      var jsonString=JSON.stringify(this.signupObj);
     // console.log(jsonString);
      //START
      if(this.captcha!=''){
        this.http.post<any>(this.data.WEBSERVICE+'/user/AddUserDetails',jsonString,{headers: {'Content-Type': 'application/json'}})
          .subscribe(response=>{
              // wip(0);
              var result=response;
              if(result.error.error_data!='0'){
              this.data.alert(result.error.error_msg,'danger');
              }else{
              var userId=result.userResult.user_id;
              localStorage.setItem('signup_user_id',userId);
              this.data.alert('Registration Done,  Kindly check your email for verification token','success');
              this.route.navigateByUrl('/otp');
              }
          },reason=>{
              // wip(0);
              this.data.alert('Internal Server Error','danger')
  
          });
   }else{
      this.data.alert('Captcha Unverified','warning');
   }
    }else{
      this.data.alert('Please fill up all the fields properly','warning');
    }
  }
  getSize(content) {
    var sz = $('#' + content)[0].files[0];
    // console.log(sz);

    if (sz.type == "image/jpeg") {
      if (sz.size > 5000000) {
        this.data.alert('File size should be less than 2MB', 'warning');
        $('#' + content).val('');
      }
    }
    else {
      this.data.alert('File should be in JPG or JPEG. ' + sz.type.split('/')[1].toUpperCase() + ' is not allowed', 'warning');
      $('#' + content).val('');
    }
  }


  // return this.http
  //       .get('https://ip-api.com/json/').subscribe(response =>{
  //         if(response!=""){
  //           console.log('ip',response);
  //           this.reg['ip']= response['query'];
  //           this.reg['clong'] = response['lon'];
  //           this.reg['clat'] = response['lat'];
  //           this.reg['man'] = this.reg['bfn'] + ' ' + this.reg['bln'];
  //           var iamString=JSON.stringify(this.reg);
  //           //console.log(this.accessKey);
  //           this.http.post < any > (this.IDMSERVICE, iamString, {
  //             headers: {
  //               'Content-Type': 'application/json',
  //               'Accept' : 'application/json',
  //               'Authorization': 'Basic' + '' + 'Y29ybmVyc3RvcmU6ODIxNTYzMWNjNmU4YjBlZjQzYTJhYmExYWE2MWE0ODllYmU1NDdmMg==',
  //             }
  //           })
  //           .subscribe(response =>{
  //             if(response){
  //               console.log('IDMSERVICE',response);
  //               var jsonString = JSON.stringify(this.reg);
  //               this.http.post<any>(this.WEBSERVICE+'/user/AddUserDetails',jsonString,{headers: {'Content-Type': 'application/json'}})
  //               .subscribe(response=>{
  //                   // wip(0);
  //                   var result=response;
  //                   if(result.error.error_data!='0'){
  //                   //this.data.alert(result.error.error_msg,'danger');
  //                   }else{
  //                   var userId=result.userResult.user_id;
  //                   localStorage.setItem('signup_user_id',userId);
  //                   /* this.data.alert('Registration Done,  Kindly check your email for verification token','success');
  //                   this.route.navigateByUrl('/otp'); */
  //                   }
  //               },reason=>{
  //                   // wip(0);
  //                   //this.data.alert('Internal Server Error','danger')

  //               });
  //             }
  //             else{
  //               alert("failed");
  //             }
  //           });  
  //           //console.log(jsonString); 
  //         }
  //         else{
  //           error =>  this.errormessage = <any>error;
  //         }
  //       });
  //}
  // signupData(isValid){
  //   if(isValid){
  //     this.signupObj['g_recaptcha_response']=this.captcha;
  //     var jsonString=JSON.stringify(this.signupObj);
  //     //START
  //     if(this.captcha!=''){
  //       this.http.post<any>(this.data.WEBSERVICE+'/user/AddUserDetails',jsonString,{headers: {'Content-Type': 'application/json'}})
  //         .subscribe(response=>{
  //             // wip(0);
  //             var result=response;
  //             if(result.error.error_data!='0'){
  //             this.data.alert(result.error.error_msg,'danger');
  //             }else{
  //             var userId=result.userResult.user_id;
  //             localStorage.setItem('signup_user_id',userId);
  //             this.data.alert('Registration Done,  Kindly check your email for verification token','success');
  //             this.route.navigateByUrl('/otp');
  //             }
  //         },reason=>{
  //             // wip(0);
  //             this.data.alert('Internal Server Error','danger')

  //         });
  //  }else{
  //     this.data.alert('Captcha Unverified','warning');
  //  }
  //   }else{
  //     this.data.alert('Please fill up all the fields properly','warning');
  //   }
  // }

  //confirm password
  confirmPassword(e) {
    if (
      (this.signupObj.password != '' && this.signupObj.password != undefined) &&
      (this.signupObj.repassword != '' && this.signupObj.repassword != undefined)
    ) {
      if (this.signupObj.password == this.signupObj.repassword) {
        $('.confirm_password_text').html('Password Matched');
        $('.confirm_password_text').css('color', 'lightgreen');
        $('#submit_btn').removeAttr('disabled');
      } else {
        $('.confirm_password_text').html('Password  Mismatched');
        $('.confirm_password_text').css('color', 'red');
        $('#submit_btn').attr('disabled', 'disabled');
      }
    } else {

    }

  }

  //check email
  checkEmail(loadmail) {
    //console.log('this.signupObj.email.value: '+this.signupObj.email);

    if (this.signupObj.email != undefined && this.signupObj.email != '') {
      //console.log(this.loadmail);
      // this.signupObj.email = this.loadmail;
      //  alert(this.signupObj.email);
      if (this.signupObj.email != '' && this.signupObj.email != undefined) {
        if (this.is_mail(this.signupObj.email) == true) {
          // wip(1);
          var emailValue = this.signupObj.email;
          var emailObj = {};
          emailObj['email'] = emailValue;
          var jsonString = JSON.stringify(emailObj);
          this.http.post<any>(this.data.WEBSERVICE + '/user/CheckEmail', jsonString, {
            headers: {

              'Content-Type': 'application/json'
            }
          })
            .subscribe(response => {
              // wip(0);
              var result = response;
              if (result.error.error_data != '0') {
                this.data.alert(result.error.error_msg, 'danger');
              } else {
                if (result.userResult.check_email_phone_flag == 1) {
                  this.data.alert('Email already registered , please try with another email address', 'warning');
                  this.signupObj.email = '';
                  //$('#signupInputEmail').focus();
                } else {

                }
              }
            }, reason => {
              // wip(0);
              this.data.alert('Internal Server Error', 'danger')
            });
        } else {
          // ui_alert('Please Provide Valid Email','error')
        }
      }
    } else {
      this.data.alert('Please Provide Email Id', 'warning');
    }

  }
  //check phone
  checkPhone() {
    if (this.signupObj.phone != '' && this.signupObj.phone != undefined) {
      // wip(1);
      var phoneValue = this.signupObj.phone;
      var phoneObj = {};
      phoneObj['phone'] = phoneValue;
      var jsonString = JSON.stringify(phoneObj);
      this.http.post<any>(this.data.WEBSERVICE + '/user/CheckPhone', jsonString, {
        headers: {

          'Content-Type': 'application/json'
        }
      })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            if (result.userResult.check_email_phone_flag == 1) {
              this.data.alert('Phone No. already registered , please try with another phone no.', 'warning');
              this.signupObj.phone.value = '';
              //$('#signupInputPhone').focus();
            } else {

            }
          }
        }, reason => {
          // wip(0);
          this.data.alert('Internal Server Error', 'danger')

        });
    } else {
      this.data.alert('Please Provide Phone No.', 'warning')
    }

  }
  //check password
  checkPassword() {
    var password = this.signupObj.password;
    if (password != '' && password != undefined && password.length >= 8) {
      var passwordStatus = this.checkAlphaNumeric(password);
      if (passwordStatus == false) {
        this.data.alert('Password should have atleast one upper case letter, one lowercase letter , one special case and one number', 'warning');
      } else {

      }
    }
    else {
      this.data.alert('Password should be minimum 8 Charecter', 'warning');
    }
  }

  checkAlphaNumeric(string) {
    if (string.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)) {
      return true;
    } else {
      return false;
    }
  }

  is_mail(email) {
    var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return regex.test(email);
  }

}