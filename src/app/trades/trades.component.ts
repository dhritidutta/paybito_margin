import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, interval } from 'rxjs';
import { NavbarComponent } from '../navbar/navbar.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { OrderBookComponent } from '../order-book/order-book.component';

@Component({
  selector: 'app-trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TradesComponent implements OnInit {
  url: any;
  currency: any;
  selectedSellingAssetText: any;
  selectedBuyingAssetText: any;
  marketTradeBodyHtml: any;
  transactionType: any;
  myTradeTableDisplayStatus: any;
  CreditCurrencyAmount: any;
  CreditBaseAmount: any;
  selectedCurrency: any;
  indentificationStatus: any;
  bankDetailStatus: any;
  historyTableTr: any;
  myOfferRecordsHtml: Observable<any>;
  closeResult: string;
  modifyAmount: any;
  modifyPrice: any;
  modifyVolume: any;
  manageOfferId: any;
  modifyType: any;
  delOfferId: any;
  delTxnType: any;
  delPrice: any;
  loader: boolean;
  dateString: string;
  dateStringArr: any;
  stopLossTableDisplayStatus: number;
  selldata: any = 0;
  flag: string;
  modifySlAmount: number;
  modifySlPrice: number;
  modifySlTrigger: number;
  marketOrderPrice: number;
  stopLossSellingAsset: any;
  stopLossBuyingAsset: any;
  stopLossOfferId: any;
  stopLossQty: any;
  modifySlOffer: any;
  buydata: any = 0;

  tradeInterval: any;
  buyingAssetIssuer: any;
  sellingAssetIssuer: any;
  //malini
  marketTradeRecords: any;
  itemcount: any;
  //
  constructor(
    public data: CoreDataService,
    private http: HttpClient,
    private modalService: NgbModal,
    private nav: NavbarComponent,
    public dash: DashboardComponent,
    private orderBook: OrderBookComponent) {
  }

  ngOnInit() {
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    this.myTradeTableDisplayStatus = 0;
    this.getMarketTradeBuy();
    this.getDashBoardInfo();
    this.getOfferLists();
    this.getMyTrade();
    // this.getOfferLists();
    this.loader = false;

    // console.log('trade');

    this.reload();

    this.tradeInterval = setInterval(() => {
      var us = localStorage.getItem('user_id');
      if (us != null) {
        this.getOfferLists();
        this.getStopLossOfferForBuy();
        this.getStopLossOfferForSell();
      }
    }, 5000);

  }
  ngDoCheck() {
    this.changemode();
  }
  changemode() {
    //console.log('++++++++++++++');
    if (this.data.changescreencolor == true) {
      $(".bg_new_class").removeClass("bg-dark").css("background-color", "#fefefe");
      $(".bg-black").css("background-color", "transparent");
      $(".btn").css("color", "#000");
      $(".text-blue").css("color", "black");
    }
    else {
      $(".bg_new_class").removeClass("bg-dark").css("background-color", "#16181a");
      $(".bg-black").css("background-color", "transparent");
      $(".btn").css("color", "#fff");
      $(".text-blue").css("color", "white");

    }
  }
  //malini
  //random function for flash class in server sent orderbook
  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);
    var maxVal1: number = parseInt(maxVal);
    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }
  reload() {
    this.getMyTrade();
    this.getOfferLists();
    this.getStopLossOfferForBuy();
    this.getStopLossOfferForSell();
  }
  // getMarketTradeBuy1() {

  //   $('#marketTradeBody').html('<tr><td colspan="3" class="text-center"><img src="./assets/svg-loaders/three-dots.svg" alt="" width="50"></td></tr>');
  //   var sellingAssetType = 'credit_alphanum12';
  //   var buyingAssetType = 'credit_alphanum12';
  //   this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
  //   this.selectedSellingAssetText = this.data.selectedSellingAssetText;
  //   this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
  //   // console.log('this.buyingAssetIssuer+++++++', this.buyingAssetIssuer);
  //   //'GDIVHXQWMZ2U2TEQ6DVIMVDUL7JTBI2KTIZQKE6GQSRAW6D5NMS37GCY';
  //   this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;//'GBDWSH2SAZM3U5FR6LPO4E2OQZNVZXUAZVH5TWEJD64MGYJJ7NWF4PBX';
  //   // this.url = this.orderBook.orderBookUrl+'trades?base_asset_type='+buyingAssetType+'&base_asset_code='+(this.selectedBuyingAssetText).toUpperCase()+(this.selectedSellingAssetText).toUpperCase()+'&base_asset_issuer='+this.buyingAssetIssuer+'&counter_asset_type='+buyingAssetType+'&counter_asset_code='+(this.selectedSellingAssetText).toUpperCase()+(this.selectedBuyingAssetText).toUpperCase()+'&counter_asset_issuer='+this.sellingAssetIssuer+'&order=desc';
  //   //var url = this.orderBookUrl + 'order_book?selling_asset_type=' + this.sellingAssetType + '&buying_asset_type=' + this.buyingAssetType + '&selling_asset_code=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase()+ this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&selling_asset_issuer=' + this.sellingAssetIssuer + '&buying_asset_code=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase()+ this.data.selectedSellingCryptoCurrencyName.toUpperCase() +'&buying_asset_issuer=' + this.buyingAssetIssuer + '&order=desc';
  //   var url = this.orderBook.orderBookUrl + 'trades?base_asset_type=' + buyingAssetType + '&base_asset_code=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase() + '&base_asset_issuer=' + this.sellingAssetIssuer + '&counter_asset_type=' + sellingAssetType + '&counter_asset_code=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&counter_asset_issuer=' + this.buyingAssetIssuer + '&order=desc' + '&limit=' + 50;
  //   this.http.get<any>(url)
  //     .subscribe(response => {
  //       //alert('jkhkjh');
  //       var result = response;
  //       this.marketTradeRecords = result._embedded.records;
  //       // console.log('this.marketTradeRecords+++++++', response);

  //       this.itemcount = this.marketTradeRecords.length;
  //       var arraylength = this.marketTradeRecords.length;
  //       // var marketTradeRecords=result._embedded.records;
  //       this.marketTradeBodyHtml = '';
  //       for (var i = 0; i < this.marketTradeRecords.length; i++) {

  //         var timeStampString = this.marketTradeRecords[i].ledger_close_time;
  //         var timeStampStringArr = timeStampString.split('T');
  //         var date = timeStampStringArr[0];
  //         var time = (timeStampStringArr[1]).slice(0, -1);
  //         var amount: any = parseFloat(this.marketTradeRecords[i].base_amount);
  //         var price: any = parseFloat(this.marketTradeRecords[i].counter_amount) / parseFloat(this.marketTradeRecords[i].base_amount);
  //         this.marketTradeBodyHtml += '<tr class="filter">';
  //         // this.marketTradeBodyHtml += '<td>' + date + ' ' + time + '</td>';
  //         this.marketTradeBodyHtml += '<td class="text-left">' + time + '</td>';
  //         // this.marketTradeBodyHtml += '<td>' + marketTradeRecords[i].base_asset_code + '</td>';

  //         if (this.selectedSellingAssetText == 'usd') {
  //           // this.marketTradeBodyHtml += '<td class="text-white">' + parseFloat(amount) + '</td>';
  //           // this.marketTradeBodyHtml += '<td class="text-white">' + (parseFloat(price)).toFixed(4) + '</td>';
  //           this.marketTradeBodyHtml += '<td class="text-white text-right"">' + parseFloat(amount) + '</td>';
  //           this.marketTradeBodyHtml += '<td class="text-white text-right">' + (parseFloat(price)).toFixed(4) + '</td>';
  //         } else {
  //           // this.marketTradeBodyHtml += '<td class="text-white">' + (parseFloat(amount)).toFixed(8) + '</td>';
  //           // this.marketTradeBodyHtml += '<td class="text-white">' + (parseFloat(price)).toFixed(8) + '</td>';
  //           this.marketTradeBodyHtml += '<td class="text-white text-right">' + (parseFloat(amount)).toFixed(8) + '</td>';
  //           this.marketTradeBodyHtml += '<td class="text-white text-right">' + (parseFloat(price)).toFixed(8) + '</td>';
  //         }
  //         this.marketTradeBodyHtml += '</tr>';
  //       }
  //       $('#marketTradeBody').html(this.marketTradeBodyHtml);
  //       if (this.marketTradeRecords.length == 0)
  //         $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
  //     }, error => {
  //       $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
  //     })
  //   // })
  // }

  getMarketTradeBuy() {
    $('#marketTradeBody').html('<tr><td colspan="3" class="text-center"><img src="./assets/svg-loaders/three-dots.svg" alt="" width="50"></td></tr>');
    var sellingAssetType = 'credit_alphanum12';
    var buyingAssetType = 'credit_alphanum12';
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText.toLowerCase();
    this.selectedSellingAssetText = this.data.selectedSellingAssetText.toLowerCase();
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;//'GBDWSH2SAZM3U5FR6LPO4E2OQZNVZXUAZVH5TWEJD64MGYJJ7NWF4PBX';
    // var buy = this.selectedBuyingAssetText;
    // var sell = this.selectedSellingAssetText;
    // var url = this.orderBook.orderBookUrl + 'trades?base_asset_type=' + buyingAssetType + '&base_asset_code=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase() + '&base_asset_issuer=' + this.sellingAssetIssuer + '&counter_asset_type=' + sellingAssetType + '&counter_asset_code=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&counter_asset_issuer=' + this.buyingAssetIssuer + '&order=desc' + '&limit=' + 50;
    // this.http.get<any>(url)
    //   .subscribe(data => {
    //     var arr = data.obj;
    //var result = response;
    //       this.marketTradeRecords = result._embedded.records;
    //  particular
    // function issuer(arr) {
    //   return arr.buy == buy && arr.sell == sell;
    // }
    // var obj = arr.find(issuer);
    // this.buyingAssetIssuer = obj.buyingIssuer;
    // this.sellingAssetIssuer = obj.sellingIssuer;

    var url = this.orderBook.orderBookUrl + 'trades?base_asset_type=' + buyingAssetType + '&base_asset_code=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase() + '&base_asset_issuer=' + this.sellingAssetIssuer + '&counter_asset_type=' + sellingAssetType + '&counter_asset_code=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&counter_asset_issuer=' + this.buyingAssetIssuer + '&order=desc' + '&limit=' + 50;

    this.http.get<any>(url)
      .subscribe(response => {
        var result = response;
       // console.log('this.marketTradeRecords+++++++', response);
        this.marketTradeRecords = result._embedded.records;
        this.itemcount = this.marketTradeRecords.length;
        var arraylength = this.marketTradeRecords.length;
        if (arraylength != null) {
          this.marketTradeBodyHtml = '';
          var randomNoForFlashArr = [];
          var randomNo = this.randomNoForOrderBook(0, 10);
          randomNoForFlashArr.push(randomNo);
          for (var i = 0; i < this.marketTradeRecords.length; i++) {
            var className = "text-green";
            if (!$.inArray(i, randomNoForFlashArr)) {
              if (randomNo % 2 == 0) {
                var className = 'text-red';
              } else if (randomNo % 2 != 0) {
                var className = 'text-red';
              }
            }
            var timeStampString = this.marketTradeRecords[i].ledger_close_time;
            var timeStampStringArr = timeStampString.split('T');
            var date = timeStampStringArr[0];
            var time = (timeStampStringArr[1]).slice(0, -1);

            var amount: any = parseFloat(this.marketTradeRecords[i].base_amount);
            var price: any = parseFloat(this.marketTradeRecords[i].counter_amount) / parseFloat(this.marketTradeRecords[i].base_amount);
            this.marketTradeBodyHtml += '<tr class="text-ash">';
            this.marketTradeBodyHtml += '<td class="text-left">' + time + '</td>';
            if (this.selectedSellingAssetText == 'eur') {
              this.marketTradeBodyHtml += '<td class="text-right">' + parseFloat(amount) + '</td>';
              this.marketTradeBodyHtml += '<td class="' + className + ' right">' + (parseFloat(price)).toFixed(4) + '</td>';
            } else {
              this.marketTradeBodyHtml += '<td class="text-right">' + (parseFloat(amount)).toFixed(8) + '</td>';
              this.marketTradeBodyHtml += '<td class="' + className + ' right">' + (parseFloat(price)).toFixed(8) + '</td>';
            }
            this.marketTradeBodyHtml += '</tr>';
          }
          // this.getMarketTradeBuy();
        }
        $('#marketTradeBody').html(this.marketTradeBodyHtml);
        if (this.marketTradeRecords.length == 0)
          $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
      }, error => {
        $('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
      })
    // })
  }

  getDashBoardInfo() {
    var infoObj = {};
    infoObj['user_id'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(infoObj);
    this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, { headers: { 'Content-Type': 'application/json', 'authorization': 'BEARER ' + localStorage.getItem('access_token') } })
      .subscribe(response => {
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          var storeDashboardInfo = JSON.stringify(result);
          var environmentSettingsListObj: any = {};
          localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
          for (var i = 0; i < result.settingsList.length; i++) {
            environmentSettingsListObj['' + result.settingsList[i].name + ''] = result.settingsList[i];
          }
          environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
          localStorage.setItem('environment_settings_list', environmentSettingsListObj);
          this.data.environment_settings_list = environmentSettingsListObj;
          //showing disclaimer for sell
          var environmentSettingListObj: any = JSON.parse(localStorage.getItem('environment_settings_list'));
          var buyDisclaimer = environmentSettingListObj['buy_max_value'][this.selectedCurrency + '_description'];
          var buyTxnDisclaimer = environmentSettingListObj['buy_txn_charges'][this.selectedCurrency + '_description'];
          var sellDisclaimer = environmentSettingListObj['sell_max_value'][this.selectedCurrency + '_description'];
          var sellTxnDisclaimer = environmentSettingListObj['sell_txn_charges'][this.selectedCurrency + '_description'];
          var sendDisclaimer = environmentSettingListObj['send_other_max_value'][this.selectedCurrency + '_description'];
          var sendMiningDisclaimer = environmentSettingListObj['send_other_m_charges'][this.selectedCurrency + '_description'];

          if (result.userAppSettingsResult.user_docs_status == '') {
            this.indentificationStatus = 'Identity verification documents not submitted';
          }
          if (result.userAppSettingsResult.user_docs_status == '1') {
            this.indentificationStatus = 'Identity verification documents verified';
          }
          if (result.userAppSettingsResult.user_docs_status == '0') {
            this.indentificationStatus = ' Identity verification documents submitted for Verification';
          }
          if (result.userAppSettingsResult.user_docs_status == '2') {
            this.indentificationStatus = ' Identity verification documents declined, please submit again';
          }
          if (result.userAppSettingsResult.bank_details_status == '') {
            this.bankDetailStatus = 'Bank details not submitted';
          }
          if (result.userAppSettingsResult.bank_details_status == '0') {
            this.bankDetailStatus = 'Bank details  submitted for Verification';
          }
          if (result.userAppSettingsResult.bank_details_status == '2') {
            this.bankDetailStatus = ' Bank details verified';
          }
          if (result.userAppSettingsResult.bank_details_status == '3') {
            this.bankDetailStatus = ' Bank documents declined, please submit again';
          }
          if (
            localStorage.getItem('check_id_verification_status') == 'true' &&
            result.userAppSettingsResult.user_docs_status == ''
          ) {
            this.data.alert('Please submit Identity verification documents to access all Digital Terminal features', 'warning');
          }
          localStorage.setItem('check_id_verification_status', 'false');
        }
      }, function (reason) {
        //   wip(0);
        if (reason.data.error == 'invalid_token') { this.data.logout(); } else { this.data.alert('Could Not Connect To Server', 'danger'); }
      });
  }

  getMyTrade() {
    // $('#myTradeBody').html('<tr><td colspan="6" class="text-center">Loading...</td></tr>');

    var tradeObj = {};
    tradeObj['customerID'] = localStorage.getItem('user_id');
    tradeObj['page_no'] = '1';
    tradeObj['no_of_items_per_page'] = '20';
    tradeObj['time_span'] = 'all';
    if (this.transactionType == undefined) {

    } else {
      tradeObj['transaction_type'] = this.transactionType;
    }

    var jsonString = JSON.stringify(tradeObj);
    if (localStorage.getItem('user_id') != null)
      this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/GetUserAllTransaction', jsonString, { headers: { 'Content-Type': 'application/json', 'authorization': 'BEARER ' + localStorage.getItem('access_token') } })
        .subscribe(response => {
          var result = response;
          var totalCount = result.totalCount;
         // console.log('totalCount', result);

          if (result.error.error_data != '0') {
          } else {
            var tradeDetails = result.userTransactionsResult;
            this.historyTableTr = '';
            if (tradeDetails)
              if (tradeDetails.length > 0) {
                for (var i = 0; i < tradeDetails.length; i++) {
                  var action = tradeDetails[i].action;
                  var trnid = tradeDetails[i].transaction_id;
                  if (action == 'Buy' || action == 'Sell') {
                    var timestamp = tradeDetails[i].transaction_timestamp;
                    var timestampArr = timestamp.split('.');
                    timestamp = this.data.readable_timestamp(timestampArr[0]);
                    if (localStorage.getItem('selected_currency') != undefined) {
                      this.selectedCurrency = localStorage.getItem('selected_currency');
                    } else {
                      this.selectedCurrency = localStorage.setItem('selected_currency', 'btc');
                      this.selectedCurrency = localStorage.getItem('selected_currency');
                    }

                    this.CreditCurrencyAmount = tradeDetails[i]['credit_' + tradeDetails[i].currency + '_amount'];
                    if (tradeDetails[i].base_currency != 'usd') {
                      this.CreditBaseAmount = tradeDetails[i]['credit_' + tradeDetails[i].base_currency + '_amount'];
                    } else {
                      this.CreditBaseAmount = tradeDetails[i]['credit_fiat_amount'];
                    }
                    // console.log(tradeDetails[i].base_currency);

                    if (tradeDetails[i].status == 'confirm') {
                      var statusClass = "text-green";
                      var status = 'Confirmed';
                    }
                    if (tradeDetails[i].status == 'pending') {
                      var statusClass = "text-orange";
                      var status = 'Pending';
                    }
                    if (tradeDetails[i].status == 'Cancel') {
                      var statusClass = "text-red";
                      var status = 'Cancel';
                    }
                    var orderId = tradeDetails[i].orderid;
                    this.historyTableTr += '<tr class="filter ' + action.toLowerCase() + '">';
                    this.historyTableTr += '<td>' + timestamp + '</td>';
                    this.historyTableTr += '<td class="">' + tradeDetails[i].currency.toUpperCase() + '</td>';
                    this.historyTableTr += '<td class="">' + tradeDetails[i].base_currency.toUpperCase() + '</td>';
                    if (action == 'Buy') {
                      this.historyTableTr += '<td class="">' + (parseFloat(this.CreditCurrencyAmount)) + '&nbsp;' + tradeDetails[i].currency.toUpperCase() + '</td>';
                    }
                    if (action == 'Sell') {
                      this.historyTableTr += '<td class="">' + (parseFloat(this.CreditBaseAmount)) + '&nbsp;' + tradeDetails[i].base_currency.toUpperCase() + '</td>';
                    }
                    this.historyTableTr += '<td class="">' + (parseFloat(tradeDetails[i].price)).toFixed(6) + '</td>';
                    /*if(localStorage.getItem('selling_crypto_asset')=='eur'){
                      // console.log('price lower');
                       this.historyTableTr+='<td class="text-white">'+(parseFloat(tradeDetails[i].offer_price)).toFixed(4)+'</td>';
                    }else{
                      this.historyTableTr+='<td class="text-white">'+(parseFloat(tradeDetails[i].offer_price)).toFixed(6)+'</td>';
                    }*/
                    var cls = action == 'Buy' ? 'success' : 'danger';
                    this.historyTableTr += '<td class="text-' + cls + '">' + action + '</td>';
                    this.historyTableTr += '</tr>';
                  }
                }
              } else {
                this.historyTableTr += '<tr><td colspan="5" class="text-center">No Data Available</td></tr>';
              }
            $('#myTradeBody').html(this.historyTableTr);
            if (this.myTradeTableDisplayStatus == 0) {
              $('#myTradeBody').children('.filter').show();
            }
            if (this.myTradeTableDisplayStatus == 1) {
              $('#myTradeBody').children('.filter').show();
              $('#myTradeBody').children('tr.sell').hide();
            }
            if (this.myTradeTableDisplayStatus == 2) {
              $('#myTradeBody').children('.filter').show();
              $('#myTradeBody').children('tr.buy').hide();
            }

          }

        }, reason => {
          if (reason.error.error == 'invalid_token') {
            this.data.logout();
            this.data.alert('Session Timeout. Login Again', 'warning');
          } else this.data.alert('Could Not Connect To Server', 'danger');
        })

  }

  myTradeDisplay(value) {
    this.myTradeTableDisplayStatus = value;
    this.getMyTrade()
  }

  getOfferLists() {

    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText
    var inputObj = {};
    var mnObj;
    //inputObj['account_pub_key']=localStorage.getItem('trade_public_key');
    inputObj['user_id'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(inputObj);
    if (localStorage.getItem('user_id') != null)
      this.http.post<any>(this.data.WEBSERVICE + '/userTrade/GetOfferByAccountID', jsonString, { headers: { 'Content-Type': 'application/json' } })
        .subscribe(response => {
          var result = response;
          // console.log('+++++++++++++++++myOfferRecordsHtml',result);
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            var result = response;
            var apiResponse = JSON.parse(response.api_response);
            var myOfferRecords = result.tradeListResult;
            mnObj = [];
            if (myOfferRecords)
              for (var i = 0; i < myOfferRecords.length; i++) {
                if (
                  (myOfferRecords[i].buying_asset_code == this.selectedBuyingAssetText && myOfferRecords[i].selling_asset_code == this.selectedSellingAssetText) ||
                  (myOfferRecords[i].buying_asset_code == this.selectedSellingAssetText && myOfferRecords[i].selling_asset_code == this.selectedBuyingAssetText)) {
                  if (myOfferRecords[i].txn_type == 1) {
                    var myOfferType = 'Buy';
                  }
                  if (myOfferRecords[i].txn_type == 2) {
                    var myOfferType = 'Sell';
                  }
                  var rsObj = {};
                  rsObj['time'] = this.data.readable_timestamp(myOfferRecords[i].timestamp);
                  rsObj['offerId'] = myOfferRecords[i].offer_id;
                  rsObj['amount'] = (parseFloat(myOfferRecords[i].amount));
                  rsObj['buy'] = myOfferRecords[i].buying_asset_code;
                  if (this.selectedSellingAssetText == 'USD') {
                    rsObj['price'] = (parseFloat(myOfferRecords[i].price));
                    rsObj['volume'] = (parseFloat(myOfferRecords[i].price) * parseFloat(myOfferRecords[i].amount));
                  }
                  else {
                    rsObj['price'] = (parseFloat(myOfferRecords[i].price));
                    rsObj['volume'] = (parseFloat(myOfferRecords[i].price) * parseFloat(myOfferRecords[i].amount));
                  }
                  rsObj['type'] = myOfferType;
                  rsObj['txn_typ'] = myOfferRecords[i].txn_type;
                  rsObj['offer_type'] = myOfferRecords[i].offer_type;
                  mnObj.push(rsObj);

                }

              }
            this.myOfferRecordsHtml = mnObj;
            // console.log('*************', this.myOfferRecordsHtml)

          }
          // console.log('data:');

        }
          , reason => {
            if (reason.error.error == 'invalid_token') {
              this.data.logout();
              this.data.alert('Session Timeout. Login Again', 'warning');
            } else {
              this.data.logout();
              this.data.alert('Could Not Connect To Server', 'danger');
            }
          });
  }

  getOfferDetails(content, offerId) {
    // console.log('content++++',content);
    var inputObj = {};
    this.data.alert('Loading...', 'dark');
    inputObj['offer_id'] = offerId;
    inputObj['user_id'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(inputObj);
    this.http.post<any>(this.data.WEBSERVICE + '/userTrade/GetOfferByID', jsonString, { headers: { 'Content-Type': 'application/json' } })
      .subscribe(response => {
        this.data.loader = false;
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.manageOfferId = offerId;
          this.modifyAmount = parseFloat(result.tradeResult.amount);
          this.modifyPrice = parseFloat(result.tradeResult.price);
          this.modifyVolume = parseFloat(this.modifyAmount) * parseFloat(this.modifyPrice);
          this.modifyType = result.tradeResult.txn_type;
          this.modalService.open(content, { centered: true });
        }
      }, reason => {
        if (reason.error.error == 'invalid_token') {
          this.data.logout();
          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }

  manageOffer() {
    this.data.alert('Loading...', 'dark');
    var inputObj = {};
    inputObj['offer_id'] = this.manageOfferId;
    inputObj['user_id'] = localStorage.getItem('user_id');
    if (this.modifyType == 1) {
      inputObj['selling_asset_code'] = this.data.selectedSellingAssetText.toUpperCase();
      inputObj['buying_asset_code'] = this.data.selectedBuyingAssetText.toUpperCase();
    }
    else {
      inputObj['buying_asset_code'] = this.data.selectedSellingAssetText.toUpperCase();
      inputObj['selling_asset_code'] = this.data.selectedBuyingAssetText.toUpperCase();
    }
    inputObj['amount'] = this.modifyAmount;
    inputObj['price'] = this.modifyPrice;
    inputObj['txn_type'] = this.modifyType;

    var jsonString = JSON.stringify(inputObj);
    this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeManageOffer', jsonString, { headers: { 'Content-Type': 'application/json' } })
      .subscribe(response => {
        var result = response;
        if (result.error.error_data != '0') {
          this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeManageOffer', jsonString, { headers: { 'Content-Type': 'application/json' } })
            .subscribe(subresponse => {
              this.data.loader = false;
              var result = subresponse;
              if (result.error.error_data != '0') {
                this.data.alert(result.error.error_msg, 'danger');
              } else {
                this.data.alert(result.error.error_msg, 'success');
                this.getMyTrade();
              }
            });
        }
        else {
          this.data.alert(result.error.error_msg, 'success');
          this.getMyTrade();
        }
      }, reason => {
        if (reason.error.error == 'invalid_token') {
          this.data.logout();
          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }

  manageOffer_margin() {

    this.data.alert('Loading...', 'dark');
    var obj = {};
    obj["customerId"] = localStorage.getItem('user_id');
    obj['offerId'] = this.manageOfferId;
    if (this.modifyType == 1) {
      obj['selling_asset_code'] = this.data.selectedSellingAssetText.toUpperCase();
      obj['buying_asset_code'] = this.data.selectedBuyingAssetText.toUpperCase();
    }
    else {
      obj['buying_asset_code'] = this.data.selectedSellingAssetText.toUpperCase();
      obj['selling_asset_code'] = this.data.selectedBuyingAssetText.toUpperCase();
    }

    obj["quantity"] = this.modifyAmount;
    obj["price"] = this.modifyPrice;
    obj["txnType"] = this.modifyType;
    obj["offerType"] = "U";
    var jsonString = JSON.stringify(obj);

    this.http.post<any>(this.data.LENDINGURL + 'manageOffer', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    }).subscribe(response => {
      // wip(0);
      var result = response.message;
      // console.log("result");
      // alert(result);

      // if(result != null){
      this.data.alert(result, 'danger');
      //  }
      //  else{
      //  alert("BIG PROBLEM");

      //  }
      // this.data.alert(response, 'danger');

    });
  }

  deleteTrade(content, offerId, txnType, price) {
    this.delOfferId = offerId;
    this.delTxnType = txnType;
    this.delPrice = price;
     this.modalService.open(content, { centered: true });
  }
  delOffer() {
    this.data.alert('Loading...', 'dark');
    $('.load').fadeIn();
    var inputObj = {};
    inputObj['offer_id'] = this.delOfferId;
    inputObj['user_id'] = localStorage.getItem('user_id');
    if (this.delTxnType == 2) {
      inputObj['selling_asset_code'] = this.selectedBuyingAssetText.toUpperCase();
      inputObj['buying_asset_code'] = this.selectedSellingAssetText.toUpperCase();
    }
    if (this.delTxnType == 1) {
      inputObj['selling_asset_code'] = this.selectedSellingAssetText.toUpperCase();
      inputObj['buying_asset_code'] = this.selectedBuyingAssetText.toUpperCase();
    }
    inputObj['amount'] = '0';
    inputObj['txn_type'] = this.delTxnType;
    inputObj['price'] = this.delPrice;
    var jsonString = JSON.stringify(inputObj);
    this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeManageOffer', jsonString, { headers: { 'Content-Type': 'application/json' } })
      .subscribe(response => {
        this.data.loader = false;
        $('.load').fadeOut();
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'warning');
        } else {
          this.getMyTrade();
          this.data.alert(result.error.error_msg, 'success');
          $('#trade').click();
        }
      }, reason => {
        if (reason.error.error == 'invalid_token') {
          this.data.logout();
          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }
  delOffer_margin() {
    this.data.alert('Loading...', 'dark');
    $('.load').fadeIn();
    var inputObj = {};
    inputObj['offerId'] = this.delOfferId;
    inputObj["customerId"] = localStorage.getItem('user_id');
    if (this.delTxnType == 2) {
      inputObj['selling_asset_code'] = this.selectedBuyingAssetText.toUpperCase();
      inputObj['buying_asset_code'] = this.selectedSellingAssetText.toUpperCase();
    }
    if (this.delTxnType == 1) {
      inputObj['selling_asset_code'] = this.selectedSellingAssetText.toUpperCase();
      inputObj['buying_asset_code'] = this.selectedBuyingAssetText.toUpperCase();
    }
 
    inputObj['quantity'] = '0';
    inputObj['txntype'] = this.delTxnType;
    inputObj['price'] = this.delPrice;
    inputObj["offerType"] = "D";
    var jsonString = JSON.stringify(inputObj);
    this.http.post<any>(this.data.LENDINGURL + 'manageOffer', jsonString, { headers: { 'Content-Type': 'application/json' } })
      // this.http.post<any>(this.data.WEBSERVICE + '/userTrade/TradeManageOffer', jsonString, { headers: { 'Content-Type': 'application/json' } })
      .subscribe(response => {
        var result=response.message;
        this.data.alert(result,'danger');
      //   this.data.loader = false;
      //   $('.load').fadeOut();
      //   var result = response;
      //   if (result.error.error_data != '0') {
      //     this.data.alert(result.error.error_msg, 'warning');
      //   } else {
      //     this.getMyTrade();
      //     this.data.alert(result.error.error_msg, 'success');
      //     $('#trade').click();
      //   }
      // }, reason => {
      //   if (reason.error.error == 'invalid_token') {
      //     this.data.logout();
      //     this.data.alert('Session Timeout. Login Again', 'warning');
      //   } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }
  getStopLossOfferForSell() {
    var inputObj = {};
    inputObj['user_id'] = localStorage.getItem('user_id');
    inputObj['selling_asset_code'] = (localStorage.getItem('buying_crypto_asset') ? localStorage.getItem('buying_crypto_asset') : 'btc').toUpperCase();
    inputObj['buying_asset_code'] = (localStorage.getItem('selling_crypto_asset') ? localStorage.getItem('selling_crypto_asset') : 'usd').toUpperCase();
    inputObj['txn_type'] = '2';
    var jsonString = JSON.stringify(inputObj);
    this.http.post<any>(this.data.WEBSERVICE + '/userTrade/GetStopLossBuySell', jsonString, { headers: { 'Content-Type': 'application/json' } })
      .subscribe(response => {

        var result = response;
        var apiResponse: any = JSON.parse(result.api_response);
        this.selldata = (apiResponse) ? apiResponse.response : '';
      }, function (reason) {
        this.data.alert(reason, 'warning');
      });
  }

  getStopLossOfferForBuy() {
    var inputObj = {};
    inputObj['user_id'] = localStorage.getItem('user_id');
    inputObj['buying_asset_code'] = (localStorage.getItem('buying_crypto_asset') ? localStorage.getItem('buying_crypto_asset') : 'btc').toUpperCase();
    inputObj['selling_asset_code'] = (localStorage.getItem('selling_crypto_asset') ? localStorage.getItem('selling_crypto_asset') : 'usd').toUpperCase();
    inputObj['txn_type'] = '1';
    var jsonString = JSON.stringify(inputObj);
    var us = localStorage.getItem('user_id');
    if (us != null)
      this.http.post<any>(this.data.WEBSERVICE + '/userTrade/GetStopLossBuySell', jsonString, {
        headers: {
          'Content-Type': 'application/json',
        }
      })
        .subscribe(response => {
          var result = response;
          var apiResponse = JSON.parse(result.api_response);
          this.buydata = (apiResponse) ? apiResponse.response : '';
        }, function (reason) {
          this.data.alert(reason, 'warning');
        });
  }

  modifyStoploss(content, id, q, p, t, flag) {
    this.modalService.open(content, { centered: true });
    this.modifySlAmount = q;
    this.modifySlPrice = p;
    this.modifySlTrigger = t;
    this.modifySlOffer = id;
    this.flag = flag;
  }

  manageStoploss() {
    this.data.alert('Loading...', 'dark');
    if (this.flag == 'Buy') {

      var marketOrderUrl = this.data.TRADESERVICE + '/getAmountBuy/' + localStorage.getItem('selling_crypto_asset').toUpperCase() + localStorage.getItem('buying_crypto_asset').toUpperCase() + '/' + localStorage.getItem('buying_crypto_asset').toUpperCase() + localStorage.getItem('selling_crypto_asset').toUpperCase() + '/' + this.modifySlAmount;
      //  console.log(this.flag);
      // console.log(marketOrderUrl);

    } else {
      var marketOrderUrl = this.data.TRADESERVICE + '/getAmountSell/' + localStorage.getItem('buying_crypto_asset').toUpperCase() + localStorage.getItem('selling_crypto_asset').toUpperCase() + '/' + localStorage.getItem('selling_crypto_asset').toUpperCase() + localStorage.getItem('buying_crypto_asset').toUpperCase() + '/' + this.modifySlAmount;
    }

    this.http.get<any>(marketOrderUrl)
      .subscribe(data => {

        // wip(0);
        var result = data;
        if (result.code == '0') {
          this.marketOrderPrice = parseFloat(result.price);
          if (this.flag == 'Buy') {
            // console.log(this.marketOrderPrice,this.modifySlTrigger, this.modifySlPrice,this.flag);
            if (
              this.marketOrderPrice < this.modifySlTrigger &&
              this.marketOrderPrice < this.modifySlPrice &&
              this.modifySlTrigger < this.modifySlPrice

            ) {
              var inputObj = {};
              inputObj['selling_asset_code'] = localStorage.getItem('selling_crypto_asset').toUpperCase();
              inputObj['buying_asset_code'] = localStorage.getItem('buying_crypto_asset').toUpperCase();

              // inputObj['selling_asset_code']=localStorage.getItem('buying_crypto_asset').toUpperCase();
              // inputObj['buying_asset_code']=localStorage.getItem('selling_crypto_asset').toUpperCase();

              inputObj['user_id'] = localStorage.getItem('user_id');
              inputObj['modify_type'] = 'edit';
              inputObj['stoploss_id'] = this.modifySlOffer;
              inputObj['stop_loss_price'] = this.modifySlPrice;
              inputObj['trigger_price'] = this.modifySlTrigger;
              inputObj['quantity'] = this.modifySlAmount;
              inputObj['txn_type'] = '1';
              //"sellassetcode":"BTC", "buyassetcode":"ETH" ,"cust_Id":"9001", "modifyType":"edit", "stoploss_Id":"2100", "stopprice":"190.01", "triggerPrice":"191.01", "quantity":"28.29"}
              var jsonString = JSON.stringify(inputObj);
              // wip(1);
              this.http.post<any>(this.data.WEBSERVICE + '/userTrade/ModifyStopLossBuySell', jsonString, { headers: { "Content-Type": "application/json" } })
                .subscribe(data => {
                  // wip(0);
                  this.data.loader = false;
                  var result = data;
                  if (result.error.error_data != '0') {
                    this.data.alert(result.error.error_msg, 'danger');
                  } else {
                    this.data.alert(result.error.error_msg, 'success');
                    this.getStopLossOfferForBuy();
                    // $('#trade').click();
                    // wip(1);
                    // location.reload();
                  }

                });
            } else {
              this.data.alert('*Market order price should be greater than trigger price & trigger price should be greater than stop loss price', 'warning');
            }
          } else {
            //   console.log(this.marketOrderPrice,this.modifySlTrigger, this.modifySlPrice);

            if (
              this.marketOrderPrice > this.modifySlTrigger &&
              this.marketOrderPrice > this.modifySlPrice &&
              this.modifySlTrigger > this.modifySlPrice
            ) {
              var inputObj = {};
              inputObj['selling_asset_code'] = localStorage.getItem('buying_crypto_asset').toUpperCase();
              inputObj['buying_asset_code'] = localStorage.getItem('selling_crypto_asset').toUpperCase();
              inputObj['user_id'] = localStorage.getItem('user_id');
              inputObj['modify_type'] = 'edit';
              inputObj['stoploss_id'] = this.modifySlOffer;
              inputObj['stop_loss_price'] = this.modifySlPrice;
              inputObj['trigger_price'] = this.modifySlTrigger;
              inputObj['quantity'] = this.modifySlAmount;
              inputObj['txn_type'] = '2';
              //"sellassetcode":"BTC", "buyassetcode":"ETH" ,"cust_Id":"9001", "modifyType":"edit", "stoploss_Id":"2100", "stopprice":"190.01", "triggerPrice":"191.01", "quantity":"28.29"}
              var jsonString = JSON.stringify(inputObj);
              // wip(1);
              this.http.post<any>(this.data.WEBSERVICE + '/userTrade/ModifyStopLossBuySell', jsonString, { headers: { "Content-Type": "application/json" } })
                .subscribe(data => {
                  // wip(0);
                  this.data.loader = false;
                  var result = data;
                  if (result.error.error_data != '0') {
                    this.data.alert(result.error.error_msg, 'danger');
                  } else {
                    this.data.alert(result.error.error_msg, 'success');
                    $('#trade').click();
                    // wip(1);
                    // location.reload();
                  }
                });
            } else {
              this.data.alert('*Market order price should be greater than trigger price & trigger price should be greater than stop loss price', 'warning');
            }
          }

        } else {

          this.data.alert('*Orderbook depth reached, price not found', 'warning');
        }

      });
  }

  delStoploss(content, id, q, p, t, flag) {
    this.modalService.open(content, { centered: true });
    this.modifySlAmount = q;
    this.modifySlPrice = p;
    this.modifySlTrigger = t;
    this.modifySlOffer = id;
    this.flag = flag;
  }

  removeStoploss() {
    this.data.alert('Loading...', 'dark');
    this.stopLossBuyingAsset = localStorage.getItem('buying_crypto_asset');
    this.stopLossSellingAsset = localStorage.getItem('selling_crypto_asset');
    // console.log( this.stopLossBuyingAsset+' '+ this.stopLossSellingAsset);
    var inputObj = {};
    inputObj['selling_asset_code'] = (this.stopLossSellingAsset).toUpperCase();
    inputObj['buying_asset_code'] = (this.stopLossBuyingAsset).toUpperCase();
    inputObj['user_id'] = localStorage.getItem('user_id');
    inputObj['modify_type'] = 'delete';
    inputObj['stoploss_id'] = this.modifySlOffer;
    inputObj['stop_loss_price'] = this.modifySlPrice;
    inputObj['trigger_price'] = this.modifySlTrigger;
    inputObj['quantity'] = this.modifySlAmount;
    if (this.flag == 'buy') {
      inputObj['txn_type'] = '1';
    } else {
      inputObj['txn_type'] = '2';
    }

    var jsonString = JSON.stringify(inputObj);
    this.http.post<any>(this.data.WEBSERVICE + '/userTrade/ModifyStopLossBuySell', jsonString, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .subscribe(data => {
        this.data.loader = false;
        var result = data;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.data.alert(result.error.error_msg, 'success');
          $('#trade').click();
          // wip(1);
          // location.reload();
        }

      });
  }

}
