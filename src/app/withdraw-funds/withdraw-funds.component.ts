import { Component, OnInit } from '@angular/core';
import { BodyService } from '../body.service';
import { HttpClient } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import * as $ from 'jquery';


@Component({
  selector: 'app-withdraw-funds',
  templateUrl: './withdraw-funds.component.html',
  styleUrls: ['./withdraw-funds.component.css']
})
export class WithdrawFundsComponent implements OnInit {

  constructor(public main:BodyService, private http:HttpClient, public data:CoreDataService) { }

  environmentSettingListObj;
  withdrawMaxValue;
  withdrawMinValue;
  withdrawDisclaimer;
  withdrawTxnChargeDisclaimer;

  ngOnInit() {
    this.main.getUserTransaction();
    this.getBankDetails();
    this.main.getDashBoardInfo();

    this.environmentSettingListObj=JSON.parse(localStorage.getItem('environment_settings_list'));
    this.withdrawMaxValue=this.environmentSettingListObj['withdrawal_max_value']['value'];
    this.withdrawMinValue=this.environmentSettingListObj['withdrawal_min_value']['value'];
    this.withdrawDisclaimer=this.environmentSettingListObj['withdrawal_min_value']['description'];
    this.withdrawTxnChargeDisclaimer=this.environmentSettingListObj['withdrawal_txn_charges']['description'];
  }

  accountNo;
  beneficiaryName;
  bankName;
  routingNo;
  bankAddress;
  lockOutgoingTransactionStatus;
  withdrawOtp;


  getBankDetails(){
    var bankDetailsObj={};
    bankDetailsObj['user_id']=localStorage.getItem('user_id');
    var jsonString=JSON.stringify(bankDetailsObj);
    // wip(1);
    this.http.post<any>(this.data.WEBSERVICE+'/user/GetUserBankDetails',jsonString,{        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER '+localStorage.getItem('access_token'),
        }})
   .subscribe(response=>{
        // wip(0);
        var result=response;
        if(result.error.error_data!='0'){
             this.data.alert(result.error.error_msg,'danger');
        }else{
            this.accountNo=result.bankDetails.account_no
            this.beneficiaryName=result.bankDetails.benificiary_name;
            this.bankName=result.bankDetails.bank_name
            this.routingNo=result.bankDetails.routing_no;
            this.bankAddress=result.bankDetails.address;
        }
    },function(reason){
        // wip(0);
      if(reason.data.error=='invalid_token'){ this.data.logout();}else{ this.data.logout();this.data.alert('Could Not Connect To Server','danger');}
    });
}
withdraw_fund_amount;
withdrawFund(){
//   console.log(this.withdraw_fund_amount);
  if(this.main.userDocVerificationStatus() && this.main.userBankVerificationStatus()){
      if(this.withdraw_fund_amount!=undefined){
          if(parseFloat(this.withdraw_fund_amount)>=parseFloat(this.withdrawMinValue) && parseFloat(this.withdraw_fund_amount)<=parseFloat(this.withdrawMaxValue)){
              var withdawObj={};
              withdawObj['customer_id']=localStorage.getItem('user_id');
              withdawObj['amount']=this.withdraw_fund_amount;
              withdawObj['bank_name']=this.bankName;
              withdawObj['ifsc_code']=this.routingNo;
              withdawObj['beneficiary_name']=this.beneficiaryName;
              if(this.lockOutgoingTransactionStatus==1){
                  withdawObj['otp']=this.withdrawOtp;
              }
              var jsonString=JSON.stringify(withdawObj);
              // wip(1);
              this.http.post<any>(this.data.WEBSERVICE+'/userTransaction/FundWithdrawal',jsonString,{headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'BEARER '+localStorage.getItem('access_token'),
                  }})
              .subscribe(response=>{
                  // wip(0);
                  var result=response;
                  if(result.error.error_data!='0'){
                       this.data.alert(result.error.error_msg,'dark');
                  }else{
                      this.withdraw_fund_amount='';
                      this.main.getUserTransaction();
                      this.data.alert('Withdrawal Done Successfully','success');
                  }
              },function(reason){
                  // wip(0);
                if(reason.data.error=='invalid_token'){ this.logout();}else{ alert('Could Not Connect To Server');}
              });

          }else{
              this.data.alert('Please Provide Proper Amount','danger');
          }
      }else{
          this.data.alert('Please Provide Proper Amount','warning');
      }
  }
  else{
    $('#wfn').attr('disabled',true);
      this.data.alert('Your Bank Details are being verified','warning');
  }

}

determineLockStatusForWithdraw(){
  var userAppSettingsObj=JSON.parse(localStorage.getItem('user_app_settings_list'));
  this.lockOutgoingTransactionStatus=userAppSettingsObj.lock_outgoing_transactions;
  if(this.lockOutgoingTransactionStatus==1){
    //   $('#otpModal').modal('show');
    this.data.alert('You are not eligble to withdraw','danger');
  }else{
      this.withdrawFund();
  }
}

resendOtpForOutgoing(){
    var otpObj={};
    otpObj['email']=localStorage.getItem('email');
    var jsonString=JSON.stringify(otpObj);
    this.http.post<any>(this.data.WEBSERVICE+'/user/ResendOTP',jsonString,{headers: {
          'Content-Type': 'application/json'
        }})
    .subscribe(response=>{
          var result= response;
          if(result.error.error_data!='0'){
             this.data.alert(result.error.error_msg,'danger');
          }else{
            $('.send_btn').show();
            $('.get_Otp_btn').hide();
          }


        },
        reason=>{
            this.data.logout();
        //   wip(0);
          this.data.alert('Could Not Connect To Server','danger');
        });
}

}
